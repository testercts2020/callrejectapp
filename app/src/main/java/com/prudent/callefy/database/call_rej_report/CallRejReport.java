package com.prudent.callefy.database.call_rej_report;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class CallRejReport implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "call_rejected_case")
    private String call_rejected_case;

    @ColumnInfo(name = "call_rejected_case_status")
    private String call_rejected_case_status;

    @ColumnInfo(name = "call_rejected_group_name")
    private String call_rejected_group_name;

    @ColumnInfo(name = "call_rejected_response_sms")
    private String call_rejected_response_sms;

    @ColumnInfo(name = "call_rejected_number")
    private String call_rejected_number;

    @ColumnInfo(name = "call_rejected_date_time")
    private String call_rejected_date_time;

    @ColumnInfo(name = "is_working_successfully")
    private boolean is_working_successfully;

    @ColumnInfo(name = "uploadStatus")
    private int uploadStatus;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCall_rejected_case() {
        return call_rejected_case;
    }

    public void setCall_rejected_case(String call_rejected_case) {
        this.call_rejected_case = call_rejected_case;
    }

    public String getCall_rejected_case_status() {
        return call_rejected_case_status;
    }

    public void setCall_rejected_case_status(String call_rejected_case_status) {
        this.call_rejected_case_status = call_rejected_case_status;
    }

    public String getCall_rejected_group_name() {
        return call_rejected_group_name;
    }

    public void setCall_rejected_group_name(String call_rejected_group_name) {
        this.call_rejected_group_name = call_rejected_group_name;
    }

    public String getCall_rejected_response_sms() {
        return call_rejected_response_sms;
    }

    public void setCall_rejected_response_sms(String call_rejected_response_sms) {
        this.call_rejected_response_sms = call_rejected_response_sms;
    }

    public String getCall_rejected_number() {
        return call_rejected_number;
    }

    public void setCall_rejected_number(String call_rejected_number) {
        this.call_rejected_number = call_rejected_number;
    }

    public String getCall_rejected_date_time() {
        return call_rejected_date_time;
    }

    public void setCall_rejected_date_time(String call_rejected_date_time) {
        this.call_rejected_date_time = call_rejected_date_time;
    }

    public boolean isIs_working_successfully() {
        return is_working_successfully;
    }

    public void setIs_working_successfully(boolean is_working_successfully) {
        this.is_working_successfully = is_working_successfully;
    }

    public int isUploadStatus() {
        return uploadStatus;
    }

    public void setUploadStatus(int uploadStatus) {
        this.uploadStatus = uploadStatus;
    }

    public int getUploadStatus() {
        return uploadStatus;
    }
}
