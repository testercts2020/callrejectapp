package com.prudent.callefy.database.calender_event;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;


import com.prudent.callefy.database.groups.group_item.ContGroupItem;

import java.util.List;

@Dao
public interface CalenderEventDao {

    @Query("SELECT * FROM CalenderEvent ORDER BY id ASC")
    List<CalenderEvent> getAll();

    @Query("SELECT * FROM CalenderEvent WHERE event_id =:event_id ORDER BY id DESC")
    List<CalenderEvent> getEventByIdAndDate(String event_id  );

    @Query("SELECT * FROM CalenderEvent WHERE is_dnt_active = :isActive ORDER BY id DESC")
    List<CalenderEvent> getEventDNDActivatedList(boolean isActive);

    @Insert
    void insert(CalenderEvent event);

    @Update
    void update(CalenderEvent event);
}
