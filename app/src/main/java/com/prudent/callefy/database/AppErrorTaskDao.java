package com.prudent.callefy.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface  AppErrorTaskDao {

    @Query("SELECT * FROM AppErrorTask")
    List<AppErrorTask> getAll();

    @Query("SELECT * FROM AppErrorTask WHERE upload_status =0")
    List<AppErrorTask> getLiveOrderById();

    @Insert
    void insert(AppErrorTask task);

    @Delete
    void delete(AppErrorTask task);

    @Update
    void update(AppErrorTask task);

    @Query("UPDATE AppErrorTask SET upload_status=1 WHERE upload_status = 0")
    void updateAfterSync();
}
