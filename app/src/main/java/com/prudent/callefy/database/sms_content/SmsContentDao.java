package com.prudent.callefy.database.sms_content;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;


import com.prudent.callefy.database.AppErrorTask;

import java.util.List;

@Dao
public interface SmsContentDao {

    @Query("SELECT * FROM SmsContent ORDER BY id DESC")
    List<SmsContent> getAll();

    @Insert
    void insert(SmsContent task);

    @Delete
    void delete(SmsContent task);

    @Query("DELETE FROM SmsContent WHERE id = :smsId")
    void deleteById(int smsId);

    @Query("DELETE FROM SmsContent WHERE sms_content = :sms")
    void deleteByContent(String sms);

    @Update
    void update(SmsContent task);

    @Query("UPDATE SmsContent SET sms_content =:sms WHERE id = :smsId")
    void updateById(int smsId , String sms);


    @Query("UPDATE SmsContent SET sms_content =:sms WHERE sms_content = :oldSms")
    void updateByMessage( String sms, String oldSms);
}
