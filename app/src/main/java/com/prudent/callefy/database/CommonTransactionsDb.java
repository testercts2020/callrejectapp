package com.prudent.callefy.database;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.prudent.callefy.database.calender_event.CalenderEvent;
import com.prudent.callefy.database.call_rej_report.CallRejReport;
import com.prudent.callefy.database.groups.ContactGroup;
import com.prudent.callefy.database.groups.group_item.ContGroupItem;
import com.prudent.callefy.database.missed_calls.MissedCalls;
import com.prudent.callefy.database.sms_content.SmsContent;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.ui.groups.group_item.GroupItemActivity;
import com.prudent.callefy.utils.CommonUtils;

import java.util.List;

public class CommonTransactionsDb {
    Context mContext;
    CommonTransAppErrorListDbCallBack mTransAppErrorCallBack;
    CommonTransNotifLogListDbCallBack mTransNotifLogListCallBack;
    CommonTransSmsContentDbCallBack mSmsContentDbCallBack;
    CommonTransGroupDbCallBack mTransGroupDbCallBack;
    CommonTransGroupItemDbCallBack mTransGroupItemDbCallBack;
    CommonTransCalenderEventDbCallBack mTransCalenderEventDbCallBack;
    CommonTransMissedCallsDbCallsBack mTransMissedCallsDbCallsBack;
    CommonTransCallRejReportDbCallsBack mTransCallRejReportDbCallsBack;
    CommonTransForegroundServiceDbCallsBack mCommonTransForegroundServiceDbCallsBack;

    public CommonTransactionsDb(Context mContext) {
        this.mContext = mContext;
    }

    public CommonTransactionsDb(Context mContext, CommonTransForegroundServiceDbCallsBack mCommonTransForegroundServiceDbCallsBack) {
        this.mContext = mContext;
        this.mCommonTransForegroundServiceDbCallsBack = mCommonTransForegroundServiceDbCallsBack;
    }

    public CommonTransactionsDb(Context mContext, CommonTransAppErrorListDbCallBack mTransAppErrorCallBack) {
        this.mContext = mContext;
        this.mTransAppErrorCallBack = mTransAppErrorCallBack;
    }

    public CommonTransactionsDb(Context mContext, CommonTransNotifLogListDbCallBack mTransNotifLogListCallBack) {
        this.mContext = mContext;
        this.mTransNotifLogListCallBack = mTransNotifLogListCallBack;
    }

    public CommonTransactionsDb(Context mContext, CommonTransSmsContentDbCallBack mSmsContentDbCallBack) {
        this.mContext = mContext;
        this.mSmsContentDbCallBack = mSmsContentDbCallBack;
    }

    public CommonTransactionsDb(Context mContext, CommonTransGroupDbCallBack mTransGroupDbCallBack) {
        this.mContext = mContext;
        this.mTransGroupDbCallBack = mTransGroupDbCallBack;
    }

//    public CommonTransactionsDb(Context mContext, CommonTransGroupItemDbCallBack mTransGroupItemDbCallBack, CommonTransCalenderEventDbCallBack mTransCalenderEventDbCallBack) {
//        this.mContext = mContext;
//        this.mTransGroupItemDbCallBack = mTransGroupItemDbCallBack;
//        this.mTransCalenderEventDbCallBack = mTransCalenderEventDbCallBack;
//    }

    public CommonTransactionsDb(Context mContext, CommonTransGroupItemDbCallBack mTransGroupItemDbCallBack, CommonTransCalenderEventDbCallBack mTransCalenderEventDbCallBack, CommonTransMissedCallsDbCallsBack mTransMissedCallsDbCallsBack) {
        this.mContext = mContext;
        this.mTransGroupItemDbCallBack = mTransGroupItemDbCallBack;
        this.mTransCalenderEventDbCallBack = mTransCalenderEventDbCallBack;
        this.mTransMissedCallsDbCallsBack = mTransMissedCallsDbCallsBack;
    }

//    public CommonTransactionsDb(Context mContext, CommonTransGroupItemDbCallBack mTransGroupItemDbCallBack) {
//        this.mContext = mContext;
//        this.mTransGroupItemDbCallBack = mTransGroupItemDbCallBack;
//    }

    public CommonTransactionsDb(Context mContext, CommonTransSmsContentDbCallBack mSmsContentDbCallBack,
                                CommonTransGroupItemDbCallBack mTransGroupItemDbCallBack,
                                CommonTransGroupDbCallBack mTransGroupDbCallBack) {
        this.mContext = mContext;
        this.mSmsContentDbCallBack = mSmsContentDbCallBack;
        this.mTransGroupItemDbCallBack = mTransGroupItemDbCallBack;
        this.mTransGroupDbCallBack = mTransGroupDbCallBack;
    }

    public CommonTransactionsDb(Context mContext, CommonTransCalenderEventDbCallBack mTransCalenderEventDbCallBack) {
        this.mContext = mContext;
        this.mTransCalenderEventDbCallBack = mTransCalenderEventDbCallBack;
    }

    public CommonTransactionsDb(Context mContext, CommonTransMissedCallsDbCallsBack mTransMissedCallsDbCallsBack) {
        this.mContext = mContext;
        this.mTransMissedCallsDbCallsBack = mTransMissedCallsDbCallsBack;
    }

    public CommonTransactionsDb(Context mContext, CommonTransCallRejReportDbCallsBack mTransCallRejReportDbCallsBack) {
        this.mContext = mContext;
        this.mTransCallRejReportDbCallsBack = mTransCallRejReportDbCallsBack;
    }

    ///////////  Error task  start ///////////////

    public void saveErrorSituation(String task, String desc, String isSuccess) {
        String sTask = task;
        String sDesc = desc;

        class SaveTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                //creating a task
                AppErrorTask task = new AppErrorTask();
                task.setTask(sTask);
                task.setDesc(sDesc);
                task.setErrorDate(CommonUtils.getTodayDate());
                task.setUploadStatus("0");
                task.setIsWorkingSuccessFul(isSuccess);
                task.setUserId(SharedPrefs.getUserId());

                //adding to database
                DatabaseClient.getInstance(mContext).getAppDatabase()
                        .taskDao()
                        .insert(task);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
//                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveTask st = new SaveTask();
        st.execute();


    }

    public void getTasks() {

        class GetTasks extends AsyncTask<Void, Void, List<AppErrorTask>> {

            @Override
            protected List<AppErrorTask> doInBackground(Void... voids) {
                List<AppErrorTask> taskList = DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .taskDao()
                        .getLiveOrderById();
                return taskList;
            }

            @Override
            protected void onPostExecute(List<AppErrorTask> tasks) {
                super.onPostExecute(tasks);

                if (tasks == null || tasks.size() < 1) {
                    Log.d("TAG", "onPostExecute: NO Data");
                } else {
                    if (mTransAppErrorCallBack != null) {
                        mTransAppErrorCallBack.getAppErrorTaskList(tasks);
                    }
                    for (AppErrorTask ts : tasks) {
                        Log.d("TAG", "onPostExecute: " + ts.getTask());

                    }
                }

            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
    }

    public void updateStatusAfterSync() {

        class UpdateTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .taskDao()
                        .updateAfterSync();

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

//                Toast.makeText(mContext, "Updated", Toast.LENGTH_LONG).show();
            }
        }

        UpdateTask st = new UpdateTask();
        st.execute();

    }


    ///////////  Error task  end ///////////////

    ///////////  Notification Call logs  start ///////////////

    public void getNotificationCallLogs() {

        class GetNotificationCallLogs extends AsyncTask<Void, Void, List<NotificationCallLog>>{

            @Override
            protected List<NotificationCallLog> doInBackground(Void... voids) {

                List<NotificationCallLog> logList= DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .notificationCallLogDao()
                        .getAllDateWise();

                return logList;
            }

            @Override
            protected void onPostExecute(List<NotificationCallLog> notificationCallLogs) {
                super.onPostExecute(notificationCallLogs);
                mTransNotifLogListCallBack.getNotifCallLogList(notificationCallLogs);

            }
        }

        GetNotificationCallLogs gt = new GetNotificationCallLogs();
        gt.execute();

    }

    public void saveNotificationCallLogs(NotificationCallLog log) {

        class SaveTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                DatabaseClient.getInstance(mContext).getAppDatabase()
                        .notificationCallLogDao()
                        .insert(log);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
//                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveTask st = new SaveTask();
        st.execute();


    }

    public void deleteAllNotificationCallLog() {

        class SaveTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {


                DatabaseClient.getInstance(mContext).getAppDatabase()
                        .notificationCallLogDao()
                        .deleteAll();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
//                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveTask st = new SaveTask();
        st.execute();


    }


    ///////////  Notification Call logs  end ///////////////


    ///////////  Sms content  start ///////////////

    public void getSmsContent() {

        class GetSmsContent extends AsyncTask<Void, Void, List<SmsContent>>{

            @Override
            protected List<SmsContent> doInBackground(Void... voids) {

                List<SmsContent> smsContentList= DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .smsContentDao()
                        .getAll();

                return smsContentList;
            }

            @Override
            protected void onPostExecute(List<SmsContent> smsContentList) {
                super.onPostExecute(smsContentList);
                mSmsContentDbCallBack.getSmsContentList(smsContentList);

            }
        }

        GetSmsContent gt = new GetSmsContent();
        gt.execute();

    }

    public void saveSmsContent(SmsContent smsContent) {

        class SaveSmsContent extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                try{
                    DatabaseClient.getInstance(mContext).getAppDatabase()
                            .smsContentDao()
                            .insert(smsContent);
                }catch (Exception e){

                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
//                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveSmsContent st = new SaveSmsContent();
        st.execute();


    }

    public void deleteSmsContent(int id, String message) {

        class SmsContent extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                try{
                    if (id == 0){
                        DatabaseClient.getInstance(mContext).getAppDatabase()
                                .smsContentDao()
                                .deleteByContent(message);
                    }else {
                        DatabaseClient.getInstance(mContext).getAppDatabase()
                                .smsContentDao()
                                .deleteById(id);
                    }



                }catch (Exception e){

                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
//                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SmsContent st = new SmsContent();
        st.execute();


    }


    public void updateSmsContent(int id, String message, String oldMessage) {

        class SmsContent extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                try{
                    if (id == 0){
                        DatabaseClient.getInstance(mContext).getAppDatabase()
                                .smsContentDao()
                                .updateByMessage( message, oldMessage);
                    }else {
                        DatabaseClient.getInstance(mContext).getAppDatabase()
                                .smsContentDao()
                                .updateById(id, message);
                    }

                }catch (Exception e){

                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
//                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SmsContent st = new SmsContent();
        st.execute();


    }


    ///////////  Sms content end ///////////////



    ///////////  Contact Group  start ///////////////

    public void getContactGroups() {

        class GetContactGroups extends AsyncTask<Void, Void, List<ContactGroup>>{

            @Override
            protected List<ContactGroup> doInBackground(Void... voids) {

                List<ContactGroup> smsContentList= DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .contactGroupDao()
                        .getAllContactPermanent(true);

                return smsContentList;
            }

            @Override
            protected void onPostExecute(List<ContactGroup> items) {
                super.onPostExecute(items);
                mTransGroupDbCallBack.getGroupDbList(items);

            }
        }

        GetContactGroups gt = new GetContactGroups();
        gt.execute();

    }

    public void getLastAddedContactGroup() {

        class GetContactGroups extends AsyncTask<Void, Void, List<ContactGroup>>{

            @Override
            protected List<ContactGroup> doInBackground(Void... voids) {

                List<ContactGroup> smsContentList= DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .contactGroupDao()
                        .getLastContactGp();

                return smsContentList;
            }

            @Override
            protected void onPostExecute(List<ContactGroup> items) {
                super.onPostExecute(items);
                mTransGroupDbCallBack.onGetGroupDbLastItem(items);

            }
        }

        GetContactGroups gt = new GetContactGroups();
        gt.execute();

    }

    public void getContactGroupById(int group_id) {

        class GetContactGroups extends AsyncTask<Void, Void, List<ContactGroup>>{

            @Override
            protected List<ContactGroup> doInBackground(Void... voids) {

                List<ContactGroup> smsContentList= DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .contactGroupDao()
                        .getContactGpById(group_id);

                return smsContentList;
            }

            @Override
            protected void onPostExecute(List<ContactGroup> items) {
                super.onPostExecute(items);
                mTransGroupDbCallBack.onGetGroupDbItem(items);

            }
        }

        GetContactGroups gt = new GetContactGroups();
        gt.execute();

    }

    public void saveContactGroup(ContactGroup contactGroup) {

        class SaveContactGroup extends AsyncTask<Void, Void, Boolean> {

            @Override
            protected Boolean doInBackground(Void... voids) {

                try{
                    DatabaseClient.getInstance(mContext).getAppDatabase()
                            .contactGroupDao()
                            .insert(contactGroup);
                }catch (Exception e){

                    Log.d("TAG", "doInBackground: "+e.toString());
                    return false;
                }

                return true;
            }

            @Override
            protected void onPostExecute(Boolean saved) {
                super.onPostExecute(saved);
                mTransGroupDbCallBack.onSavedGroupDb(saved);
//                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveContactGroup st = new SaveContactGroup();
        st.execute();
    }

    public void updateContactGroup(ContactGroup contactGroup) {

        class SaveContactGroup extends AsyncTask<Void, Void, Boolean> {

            @Override
            protected Boolean doInBackground(Void... voids) {

                try{
                    DatabaseClient.getInstance(mContext).getAppDatabase()
                            .contactGroupDao()
                            .update(contactGroup);
                }catch (Exception e){

                    Log.d("TAG", "doInBackground: "+e.toString());
                    return false;
                }

                return true;
            }

            @Override
            protected void onPostExecute(Boolean saved) {
                super.onPostExecute(saved);
                mTransGroupDbCallBack.onUpdateGroupDb(saved);
//                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveContactGroup st = new SaveContactGroup();
        st.execute();
    }

    public void deleteContactGroup(ContactGroup contactGroup) {

        class SaveContactGroup extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                try{
                    DatabaseClient.getInstance(mContext).getAppDatabase()
                            .contactGroupDao()
                            .delete(contactGroup);
                }catch (Exception e){

                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
//                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveContactGroup st = new SaveContactGroup();
        st.execute();


    }

    public void deleteContactGroupContentList(int group_id) {

        class SaveContactGroup extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                try{
                    DatabaseClient.getInstance(mContext).getAppDatabase()
                            .contactGroupDao()
                            .deleteGroupContacts(group_id);
                }catch (Exception e){

                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
//                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveContactGroup st = new SaveContactGroup();
        st.execute();


    }

    public void deleteContactGroupsNotPermanent() {

        class GetContactGroups extends AsyncTask<Void, Void, Boolean>{
            @Override
            protected Boolean doInBackground(Void... voids) {

                DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .contactGroupDao()
                        .deleteGroupContactsNotPermanent(false);
                return null;
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
            }
        }

        GetContactGroups gt = new GetContactGroups();
        gt.execute();

    }


    ///////////  Contact Group  end ///////////////

    ///////////  Contact Group Item  start ///////////////



    public void getContactGroupsItemList(int group_id) {

        class GetContactGroupsItem extends AsyncTask<Void, Void, List<ContGroupItem>>{

            @Override
            protected List<ContGroupItem> doInBackground(Void... voids) {

                List<ContGroupItem> contGroupItemList= DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .contGroupItemDao()
                        .getAllById(group_id);

                return contGroupItemList;
            }

            @Override
            protected void onPostExecute(List<ContGroupItem> items) {
                super.onPostExecute(items);
                mTransGroupItemDbCallBack.getGroupItemDbList(items);

            }
        }

        GetContactGroupsItem gt = new GetContactGroupsItem();
        gt.execute();

    }

//    public void getGroupsDetailsForContacts(int group_id) {
//
//        class GetContactGroupsItem extends AsyncTask<Void, Void, List<ContGroupItem>>{
//
//            @Override
//            protected List<ContGroupItem> doInBackground(Void... voids) {
//
//                List<ContGroupItem> contGroupItemList= DatabaseClient
//                        .getInstance(mContext)
//                        .getAppDatabase()
//                        .contGroupItemDao()
//                        .getAllById(group_id);
//
//                return contGroupItemList;
//            }
//
//            @Override
//            protected void onPostExecute(List<ContGroupItem> items) {
//                super.onPostExecute(items);
//                mTransGroupItemDbCallBack.getGroupItemDbList(items);
//
//            }
//        }
//
//        GetContactGroupsItem gt = new GetContactGroupsItem();
//        gt.execute();
//
//    }

    public void getGroupsDetailsForContacts(int group_id) {

        class GetContactGroups extends AsyncTask<Void, Void, List<ContactGroup>>{

            @Override
            protected List<ContactGroup> doInBackground(Void... voids) {

                List<ContactGroup> smsContentList= DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .contactGroupDao()
                        .getContactGpById(group_id);

                return smsContentList;
            }

            @Override
            protected void onPostExecute(List<ContactGroup> items) {
                super.onPostExecute(items);
                mTransGroupItemDbCallBack.onGetGroupDbItem(items);

            }
        }

        GetContactGroups gt = new GetContactGroups();
        gt.execute();

    }

    public void getLatAddedContactGroupsItemList() {

        class GetContactGroupsItem extends AsyncTask<Void, Void, List<ContGroupItem>>{

            @Override
            protected List<ContGroupItem> doInBackground(Void... voids) {

                List<ContGroupItem> contGroupItemList= DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .contGroupItemDao()
                        .getLastAddedItem();

                return contGroupItemList;
            }

            @Override
            protected void onPostExecute(List<ContGroupItem> items) {
                super.onPostExecute(items);
                mTransGroupItemDbCallBack.getLastAddedGroupItemDbList(items);

            }
        }

        GetContactGroupsItem gt = new GetContactGroupsItem();
        gt.execute();

    }

    public void getContactGroupsItem(int group_id) {

        class GetContactGroupsItem extends AsyncTask<Void, Void, List<ContGroupItem>>{

            @Override
            protected List<ContGroupItem> doInBackground(Void... voids) {

                List<ContGroupItem> contGroupItemList= DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .contGroupItemDao()
                        .getAllById(group_id);

                return contGroupItemList;
            }

            @Override
            protected void onPostExecute(List<ContGroupItem> items) {
                super.onPostExecute(items);
                mTransGroupItemDbCallBack.getGroupItemDbList(items);

            }
        }

        GetContactGroupsItem gt = new GetContactGroupsItem();
        gt.execute();

    }

    public void getContactListForCheck(int group_id, int actionId) { //  todo it may be delete if not use

        class SaveContactGroup extends AsyncTask<Void, Void, Boolean> {

            @Override
            protected Boolean doInBackground(Void... voids) {

                try{
                    DatabaseClient.getInstance(mContext).getAppDatabase()
                            .contGroupItemDao()
                            .updateCallAction(group_id, actionId);
                }catch (Exception e){

                    Log.d("TAG", "doInBackground: "+e.toString());
                    return false;
                }

                return true;
            }

            @Override
            protected void onPostExecute(Boolean aVoid) {
                super.onPostExecute(aVoid);
                mTransGroupItemDbCallBack.onSaveGroupItemDbList(aVoid);
//                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveContactGroup st = new SaveContactGroup();
        st.execute();


    }

    public void getContactGroupsNoByInComing(String incomNo) {

        class GetContactGroupsItem extends AsyncTask<Void, Void, List<ContGroupItem>>{

            @Override
            protected List<ContGroupItem> doInBackground(Void... voids) {

                List<ContGroupItem> contGroupItemList= DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .contGroupItemDao()
                        .getAllByNo(incomNo);

                return contGroupItemList;
            }

            @Override
            protected void onPostExecute(List<ContGroupItem> items) {
                super.onPostExecute(items);
                mTransGroupItemDbCallBack.getGroupItemDbList(items);

            }
        }

        GetContactGroupsItem gt = new GetContactGroupsItem();
        gt.execute();

    }

    public void saveContactGroupItem(ContGroupItem contactGroup) {

        class SaveContactGroup extends AsyncTask<Void, Void, Boolean> {

            @Override
            protected Boolean doInBackground(Void... voids) {

                try{
                    DatabaseClient.getInstance(mContext).getAppDatabase()
                            .contGroupItemDao()
                            .insert(contactGroup);
                }catch (Exception e){

                    Log.d("TAG", "doInBackground: "+e.toString());
                    return false;
                }

                return true;
            }

            @Override
            protected void onPostExecute(Boolean aVoid) {
                super.onPostExecute(aVoid);
                mTransGroupItemDbCallBack.onSaveGroupItemDbList(aVoid);
//                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveContactGroup st = new SaveContactGroup();
        st.execute();
    }

    public void updateGroupPermanent(int groupId) {

        class SaveContactGroup extends AsyncTask<Void, Void, Boolean> {

            @Override
            protected Boolean doInBackground(Void... voids) {

                try{
                      DatabaseClient.getInstance(mContext).getAppDatabase()
                            .contactGroupDao()
                            .updateGroupPermanent(groupId, true);
                }catch (Exception e){

                    Log.d("TAG", "doInBackground: "+e.toString());
                    return false;
                }

                return true;
            }

            @Override
            protected void onPostExecute(Boolean aVoid) {
                super.onPostExecute(aVoid);
//                mTransGroupItemDbCallBack.onSaveGroupItemDbList(aVoid);
//                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveContactGroup st = new SaveContactGroup();
        st.execute();
    }

    public void deleteContactGroupItem(ContGroupItem contactGroup) {

        class SaveContactGroup extends AsyncTask<Void, Void, Boolean> {

            @Override
            protected Boolean doInBackground(Void... voids) {

                try{
                    DatabaseClient.getInstance(mContext).getAppDatabase()
                            .contGroupItemDao()
                            .delete(contactGroup);
                }catch (Exception e){

                    Log.d("TAG", "doInBackground: "+e.toString());
                    return false;
                }

                return true;
            }

            @Override
            protected void onPostExecute(Boolean aVoid) {
                super.onPostExecute(aVoid);
                mTransGroupItemDbCallBack.onDeleteGroupItemDbList(aVoid);
//                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveContactGroup st = new SaveContactGroup();
        st.execute();


    }

    public void updateContactGroupItemCallAction(int group_id, int actionId) {

        class SaveContactGroup extends AsyncTask<Void, Void, Boolean> {

            @Override
            protected Boolean doInBackground(Void... voids) {

                try{
                    DatabaseClient.getInstance(mContext).getAppDatabase()
                            .contGroupItemDao()
                            .updateCallAction(group_id, actionId);
                }catch (Exception e){

                    Log.d("TAG", "doInBackground: "+e.toString());
                    return false;
                }

                return true;
            }

            @Override
            protected void onPostExecute(Boolean aVoid) {
                super.onPostExecute(aVoid);
                mTransGroupItemDbCallBack.onSaveGroupItemDbList(aVoid);
//                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveContactGroup st = new SaveContactGroup();
        st.execute();


    }

    public void updateContactGroupItemCallAction(int group_id,  String message) {

        class SaveContactGroup extends AsyncTask<Void, Void, Boolean> {

            @Override
            protected Boolean doInBackground(Void... voids) {

                try{
                    DatabaseClient.getInstance(mContext).getAppDatabase()
                            .contGroupItemDao()
                            .updateCallMessage(group_id, message);
                }catch (Exception e){

                    Log.d("TAG", "doInBackground: "+e.toString());
                    return false;
                }

                return true;
            }

            @Override
            protected void onPostExecute(Boolean aVoid) {
                super.onPostExecute(aVoid);
                mTransGroupItemDbCallBack.onSaveGroupItemDbList(aVoid);
//                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveContactGroup st = new SaveContactGroup();
        st.execute();


    }

    public void updateContactGroupItemDoNotDisturb(int group_id,  boolean dnb) {

        class SaveContactGroup extends AsyncTask<Void, Void, Boolean> {

            @Override
            protected Boolean doInBackground(Void... voids) {

                try{
                    DatabaseClient.getInstance(mContext).getAppDatabase()
                            .contGroupItemDao()
                            .updateDoNotDisturb(group_id, dnb);
                }catch (Exception e){

                    Log.d("TAG", "doInBackground: "+e.toString());
                    return false;
                }

                return true;
            }

            @Override
            protected void onPostExecute(Boolean aVoid) {
                super.onPostExecute(aVoid);
                mTransGroupItemDbCallBack.onSaveGroupItemDbList(aVoid);
//                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveContactGroup st = new SaveContactGroup();
        st.execute();


    }

//    public void checkContactGroupItemDoNotDisturbActive() {
//
//        class SaveContactGroup extends AsyncTask<Void, Void, Boolean> {
//
//            @Override
//            protected Boolean doInBackground(Void... voids) {
//
//                try{
//                    DatabaseClient.getInstance(mContext).getAppDatabase()
//                            .contGroupItemDao()
//                            .checkDonotDistActive();
//                }catch (Exception e){
//
//                    Log.d("TAG", "doInBackground: "+e.toString());
//                    return false;
//                }
//
//                return true;
//            }
//
//            @Override
//            protected void onPostExecute(Boolean isActive) {
//                super.onPostExecute(isActive);
//                mTransGroupItemDbCallBack.onCheckDoNotDistActiveDb(isActive);
////                Toast.makeText(mContext, "Saved", Toast.LENGTH_LONG).show();
//            }
//        }
//
//        SaveContactGroup st = new SaveContactGroup();
//        st.execute();
//
//
//    }

    public void checkContactGroupItemDoNotDisturbActive() {

        class GetContactGroupsItem extends AsyncTask<Void, Void, List<ContGroupItem>>{

            @Override
            protected List<ContGroupItem> doInBackground(Void... voids) {

                List<ContGroupItem> contGroupItemList= DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .contGroupItemDao()
                        .checkDonotDistActive(true);

                return contGroupItemList;
            }

            @Override
            protected void onPostExecute(List<ContGroupItem> items) {
                super.onPostExecute(items);
                Log.d("TAG", "onPostExecute: "+items.size());
                if (items.size()<1)
                mTransGroupItemDbCallBack.onCheckDoNotDistActiveDb(false);
                else {
                    mTransGroupItemDbCallBack.onCheckDoNotDistActiveDb(true);

                }
//                mTransGroupItemDbCallBack.getGroupItemDbList(items);

            }
        }

        GetContactGroupsItem gt = new GetContactGroupsItem();
        gt.execute();

    }


    ///////////  Contact Group Item end ///////////////

    ///////////  Calender event Item start ///////////////

    public void getAllCalenderEventList2(int group_id) {

//        class GetContactGroupsItem extends AsyncTask<Void, Void, List<ContGroupItem>>{
        class CalenderEventItem extends AsyncTask<Void, Void, List<ContGroupItem>>{

            @Override
            protected List<ContGroupItem> doInBackground(Void... voids) {

                List<ContGroupItem> contGroupItemList= DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .contGroupItemDao()
                        .getAllById(group_id);

                return contGroupItemList;
            }

            @Override
            protected void onPostExecute(List<ContGroupItem> items) {
                super.onPostExecute(items);
                mTransGroupItemDbCallBack.getGroupItemDbList(items);

            }
        }

        CalenderEventItem ce = new CalenderEventItem();
        ce.execute();

    }

    public void getAllCalenderEventList() {

        class CalenderEventItem extends AsyncTask<Void, Void, List<CalenderEvent>>{
            @Override
            protected List<CalenderEvent> doInBackground(Void... voids) {
                List<CalenderEvent> list = DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .calenderEventDao()
                        .getAll();
                return list;
            }

            @Override
            protected void onPostExecute(List<CalenderEvent> calenderEvents) {
                super.onPostExecute(calenderEvents);
                mTransCalenderEventDbCallBack.getCalenderEventDbList(calenderEvents);
            }
        }

        CalenderEventItem ce = new CalenderEventItem();
        ce.execute();

    }

    public void getAllActivatedCalenderEventList() {

        class CalenderEventItem extends AsyncTask<Void, Void, List<CalenderEvent>>{
            @Override
            protected List<CalenderEvent> doInBackground(Void... voids) {
                List<CalenderEvent> list = DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .calenderEventDao()
                        .getEventDNDActivatedList(true);
                return list;
            }

            @Override
            protected void onPostExecute(List<CalenderEvent> calenderEvents) {
                super.onPostExecute(calenderEvents);
                mTransCalenderEventDbCallBack.getCalenderEventDbList(calenderEvents);
            }
        }

        CalenderEventItem ce = new CalenderEventItem();
        ce.execute();

    }

    public void saveEventList(CalenderEvent event) {

        class CalenderEventItem extends AsyncTask<Void, Void, List<CalenderEvent>>{
            @Override
            protected List<CalenderEvent> doInBackground(Void... voids) {
                List<CalenderEvent> list = DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .calenderEventDao()
                        .getAll();
                return list;
            }

            @Override
            protected void onPostExecute(List<CalenderEvent> calenderEvents) {
                super.onPostExecute(calenderEvents);
                mTransCalenderEventDbCallBack.getCalenderEventDbList(calenderEvents);
            }
        }

        CalenderEventItem ce = new CalenderEventItem();
        ce.execute();

    }

    public void addOrUpdateEventItem(CalenderEvent event, boolean needResponse) {  // needResponse we have only need last item update

        class CalenderEventItem extends AsyncTask<Void, Void, Boolean>{
            @Override
            protected Boolean doInBackground(Void... voids) {
                List<CalenderEvent> list = DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .calenderEventDao()
                        .getEventByIdAndDate(event.getEvent_id() );

                if (list == null || list.size() <1){
                    try {
                    DatabaseClient
                            .getInstance(mContext)
                            .getAppDatabase()
                            .calenderEventDao()
                            .insert(event);
                        return true;
                    }catch (Exception e){
                        Log.d("TAG6666", "doInBackground: "+e.getMessage());
                        return false;
                    }
                }else {
                    try {
                        event.setIs_dnt_active(list.get(0).isIs_dnt_active());
                        event.setId(list.get(0).getId());
                        DatabaseClient
                                .getInstance(mContext)
                                .getAppDatabase()
                                .calenderEventDao()
                                .update(event);
                        return true;
                    }catch (Exception e){
                        Log.d("TAG6666", "doInBackground: "+e.getMessage());
                        return false;
                    }

                }




            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if (needResponse)
                mTransCalenderEventDbCallBack.onIsAddOrUpdateDbEvent(aBoolean);
            }
        }

        CalenderEventItem ce = new CalenderEventItem();
        ce.execute();

    }

    public void updateEventItem(CalenderEvent event) {  // needResponse we have only need last item update

        class CalenderEventItem extends AsyncTask<Void, Void, Boolean>{
            @Override
            protected Boolean doInBackground(Void... voids) {
                try {

                    DatabaseClient
                            .getInstance(mContext)
                            .getAppDatabase()
                            .calenderEventDao()
                            .update(event);
                    return true;
                }catch (Exception e){
                    Log.d("TAG6666", "doInBackground: "+e.getMessage());
                    return false;
                }


            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                    mTransCalenderEventDbCallBack.onUpdateDbEvent(aBoolean);
            }
        }

        CalenderEventItem ce = new CalenderEventItem();
        ce.execute();

    }

    ///////////  Contact Group Item end ///////////////

    ///////////  Missed calls start ///////////////

    public void getAllMissedCalls() {

        class MissedCallsItem extends AsyncTask<Void, Void, List<MissedCalls>>{
            @Override
            protected List<MissedCalls> doInBackground(Void... voids) {
                List<MissedCalls> list= DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .missedCallsDao()
                        .getAll();
                return list;
            }

            @Override
            protected void onPostExecute(List<MissedCalls> missedCalls) {
                super.onPostExecute(missedCalls);
                mTransMissedCallsDbCallsBack.onGetAllMissedCalls(missedCalls);
            }
        }

        MissedCallsItem mc = new MissedCallsItem();
        mc.execute();
    }

    public void getAllMissedCallsForSaveToServer() {

        class MissedCallsItem extends AsyncTask<Void, Void, List<MissedCalls>>{
            @Override
            protected List<MissedCalls> doInBackground(Void... voids) {
                List<MissedCalls> list= DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .missedCallsDao()
                        .getAllMissedCallReport();
                return list;
            }

            @Override
            protected void onPostExecute(List<MissedCalls> missedCalls) {
                super.onPostExecute(missedCalls);
                mTransMissedCallsDbCallsBack.onGetAllMissedCallsNotUploaded(missedCalls);
            }
        }

        MissedCallsItem mc = new MissedCallsItem();
        mc.execute();
    }

    public void getAllMissedCallsForSaveToServerForeground() {

        class MissedCallsItem extends AsyncTask<Void, Void, List<MissedCalls>>{
            @Override
            protected List<MissedCalls> doInBackground(Void... voids) {
                List<MissedCalls> list= DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .missedCallsDao()
                        .getAllMissedCallReport();
                return list;
            }

            @Override
            protected void onPostExecute(List<MissedCalls> missedCalls) {
                super.onPostExecute(missedCalls);
                mCommonTransForegroundServiceDbCallsBack.onGetAllCallRejReports(missedCalls);
            }
        }

        MissedCallsItem mc = new MissedCallsItem();
        mc.execute();
    }

    public void addMissedCallsItem(MissedCalls missedCalls) {

        class MissedCallsItem extends AsyncTask<Void, Void, Boolean>{
            @Override
            protected Boolean doInBackground(Void... voids) {

                try {
                    DatabaseClient
                            .getInstance(mContext)
                            .getAppDatabase()
                            .missedCallsDao()
                            .insert(missedCalls);
                    return true;
                }catch (Exception e){
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                mTransMissedCallsDbCallsBack.onInsertMissedCalls(aBoolean);
            }
        }

        MissedCallsItem mc = new MissedCallsItem();
        mc.execute();
    }


    public void updateMissedCallsItemAfterUploadServer() {

        class MissedCallsItem extends AsyncTask<Void, Void, Boolean>{
            @Override
            protected Boolean doInBackground(Void... voids) {

                try {
                    DatabaseClient
                            .getInstance(mContext)
                            .getAppDatabase()
                            .missedCallsDao()
                            .updateAfterUploadReport();
                    return true;
                }catch (Exception e){
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);

            }
        }

        MissedCallsItem mc = new MissedCallsItem();
        mc.execute();
    }



    ///////////  Missed calls end ///////////////

    ///////////  Call reject report start ///////////////

    public void getCallRejectedReportForUpload() {

        class CallRejReportItem extends AsyncTask<Void, Void, List<CallRejReport>>{
            @Override
            protected List<CallRejReport> doInBackground(Void... voids) {
                List<CallRejReport> list = DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .callRejReportDao()
                        .getAll();

                return list;
            }

            @Override
            protected void onPostExecute(List<CallRejReport> callRejReports) {
                super.onPostExecute(callRejReports);
                mTransCallRejReportDbCallsBack.onGetAllCallRejReports(callRejReports);
            }

        }

        CallRejReportItem cr = new CallRejReportItem();
        cr.execute();
    }

    public void saveCallRejectedReportForUpload() {

        class CallRejReportItem extends AsyncTask<Void, Void, List<CallRejReport>>{
            @Override
            protected List<CallRejReport> doInBackground(Void... voids) {
                List<CallRejReport> list = DatabaseClient
                        .getInstance(mContext)
                        .getAppDatabase()
                        .callRejReportDao()
                        .getAll();

                return list;
            }

            @Override
            protected void onPostExecute(List<CallRejReport> callRejReports) {
                super.onPostExecute(callRejReports);
                mTransCallRejReportDbCallsBack.onGetAllCallRejReports(callRejReports);
            }

        }

        CallRejReportItem cr = new CallRejReportItem();
        cr.execute();
    }



    ///////////  Call reject report end ///////////////


    public interface CommonTransAppErrorListDbCallBack {

        void getAppErrorTaskList(List<AppErrorTask> tasks);
    }

    public interface CommonTransNotifLogListDbCallBack {

        void getNotifCallLogList(List<NotificationCallLog> logs);
    }

    public interface CommonTransSmsContentDbCallBack {

        void getSmsContentList(List<SmsContent> logs);
    }

    public interface CommonTransGroupDbCallBack {

        void getGroupDbList(List<ContactGroup> item);
        void onGetGroupDbItem(List<ContactGroup> item);
        void onGetGroupDbLastItem(List<ContactGroup> item);
        void onSavedGroupDb(boolean isSaved);
        void onUpdateGroupDb(boolean isUpdated);
    }

    public interface CommonTransGroupItemDbCallBack {

        void getGroupItemDbList(List<ContGroupItem> item);
        void getLastAddedGroupItemDbList(List<ContGroupItem> item);
        void onGetGroupDbItem(List<ContactGroup> item);
        void onSaveGroupItemDbList(boolean isSaved);
        void onDeleteGroupItemDbList(boolean isSaved);
        void onCheckDoNotDistActiveDb(boolean isActive);
    }

    public interface CommonTransCalenderEventDbCallBack {
        void getCalenderEventDbList(List<CalenderEvent> item);
        void onIsAddOrUpdateDbEvent(boolean isUpdate);
        void onUpdateDbEvent(boolean isUpdate);

    }

    public interface CommonTransMissedCallsDbCallsBack{
        void onGetAllMissedCalls(List<MissedCalls> list);
        void onGetAllMissedCallsNotUploaded(List<MissedCalls> list);
        void onInsertMissedCalls(boolean isAdded);
    }

    public interface CommonTransCallRejReportDbCallsBack{
        void onGetAllCallRejReports(List<CallRejReport> list);
    }

    public interface CommonTransForegroundServiceDbCallsBack{
        void onGetAllCallRejReports(List<MissedCalls> list);
    }

}
