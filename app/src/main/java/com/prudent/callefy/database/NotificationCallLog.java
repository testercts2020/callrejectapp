package com.prudent.callefy.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class NotificationCallLog implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "to_no")
    private String toNo;

    @ColumnInfo(name = "from_no")
    private String fromNo;

    @ColumnInfo(name = "date_time")
    private String dateTime;

    @ColumnInfo(name = "forwarding_no")
    private String forwardingNo;

    @ColumnInfo(name = "no_of_calls_same_user")
    private String noOfCallsSameUser;

    @ColumnInfo(name = "call_type")
    private String callType;

    @ColumnInfo(name = "caller_name")
    private String callerName;

    @ColumnInfo(name = "is_header")
    private Boolean isHeader;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getToNo() {
        return toNo;
    }

    public void setToNo(String toNo) {
        this.toNo = toNo;
    }

    public String getFromNo() {
        return fromNo;
    }

    public void setFromNo(String fromNo) {
        this.fromNo = fromNo;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getForwardingNo() {
        return forwardingNo;
    }

    public void setForwardingNo(String forwardingNo) {
        this.forwardingNo = forwardingNo;
    }

    public String getNoOfCallsSameUser() {
        return noOfCallsSameUser;
    }

    public void setNoOfCallsSameUser(String noOfCallsSameUser) {
        this.noOfCallsSameUser = noOfCallsSameUser;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getCallerName() {
        return callerName;
    }

    public void setCallerName(String callerName) {
        this.callerName = callerName;
    }

    public Boolean getHeader() {
        return isHeader;
    }

    public void setHeader(Boolean header) {
        isHeader = header;
    }
}
