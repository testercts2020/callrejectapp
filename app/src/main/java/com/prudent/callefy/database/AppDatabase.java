package com.prudent.callefy.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.prudent.callefy.database.calender_event.CalenderEvent;
import com.prudent.callefy.database.calender_event.CalenderEventDao;
import com.prudent.callefy.database.call_rej_report.CallRejReport;
import com.prudent.callefy.database.call_rej_report.CallRejReportDao;
import com.prudent.callefy.database.groups.ContactGroup;
import com.prudent.callefy.database.groups.ContactGroupDao;
import com.prudent.callefy.database.groups.group_item.ContGroupItem;
import com.prudent.callefy.database.groups.group_item.ContGroupItemDao;
import com.prudent.callefy.database.missed_calls.MissedCalls;
import com.prudent.callefy.database.missed_calls.MissedCallsDao;
import com.prudent.callefy.database.sms_content.SmsContent;
import com.prudent.callefy.database.sms_content.SmsContentDao;

@Database(entities = {AppErrorTask.class,
        NotificationCallLog.class,
        SmsContent.class,
        ContactGroup.class,
        ContGroupItem.class,
        CalenderEvent.class,
        MissedCalls.class,
        CallRejReport.class
}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract AppErrorTaskDao taskDao();
    public abstract NotificationCallLogDao notificationCallLogDao();
    public abstract SmsContentDao smsContentDao();
    public abstract ContactGroupDao contactGroupDao();
    public abstract ContGroupItemDao contGroupItemDao();
    public abstract CalenderEventDao calenderEventDao();
    public abstract MissedCallsDao missedCallsDao();
    public abstract CallRejReportDao callRejReportDao();
}
