package com.prudent.callefy.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface NotificationCallLogDao {

    @Query("SELECT * FROM NotificationCallLog")
    List<NotificationCallLog> getAll();

    @Query("SELECT * FROM NotificationCallLog ORDER BY date_time DESC")
    List<NotificationCallLog> getAllDateWise();

    @Insert
    void insert(NotificationCallLog task);

    @Insert
    void insertList(List<NotificationCallLog> taskList);

    @Query("DELETE FROM NotificationCallLog")
    void deleteAll();


}
