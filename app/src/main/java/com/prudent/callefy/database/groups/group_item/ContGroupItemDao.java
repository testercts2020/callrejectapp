package com.prudent.callefy.database.groups.group_item;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.prudent.callefy.database.groups.ContactGroup;

import java.util.List;

@Dao
public interface ContGroupItemDao {

    @Query("SELECT * FROM ContGroupItem ORDER BY id DESC")
    List<ContGroupItem> getAll();

    @Query("SELECT * FROM ContGroupItem WHERE group_id =:id ORDER BY id DESC")
    List<ContGroupItem> getAllByIdDistingt(int id);

    @Query("SELECT * FROM ContGroupItem WHERE group_id =:id ORDER BY id DESC")
    List<ContGroupItem> getAllById(int id);

    @Query("SELECT * FROM ContGroupItem WHERE contact_no =:mobNo ORDER BY id DESC")
    List<ContGroupItem> getAllByNo(String mobNo);

    @Query("SELECT * FROM ContGroupItem WHERE is_donot_disturb_active =:pnb ORDER BY id DESC")
    List<ContGroupItem> checkDonotDistActive(boolean pnb);

    @Query("SELECT * FROM ContGroupItem WHERE id =(SELECT MAX(id) FROM ContGroupItem) ORDER BY id DESC")
    List<ContGroupItem> getLastAddedItem();

    @Insert
    void insert(ContGroupItem item);

    @Delete
    void delete(ContGroupItem item);

    @Update
    void update(ContGroupItem item);

    @Query("UPDATE ContGroupItem SET call_action_id=:actionId WHERE group_id =:group_id ")
    void updateCallAction(int group_id, int actionId);

    @Query("UPDATE ContGroupItem SET sms_content=:message WHERE group_id =:group_id ")
    void updateCallMessage(int group_id, String message);

    @Query("UPDATE ContGroupItem SET is_donot_disturb_active=:dnb WHERE group_id =:group_id ")
    void updateDoNotDisturb(int group_id, boolean dnb);
}
