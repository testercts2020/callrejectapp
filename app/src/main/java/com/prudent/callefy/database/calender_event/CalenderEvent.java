package com.prudent.callefy.database.calender_event;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class CalenderEvent implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "event_id")
    private String event_id;

    @ColumnInfo(name = "event_name")
    private String event_name;

    @ColumnInfo(name = "event_desc")
    private String event_desc;

    @ColumnInfo(name = "start_date")
    private String start_date;

    @ColumnInfo(name = "end_date")
    private String end_date;

    @ColumnInfo(name = "start_date_before_5min")
    private String start_date_before_5min;

    @ColumnInfo(name = "is_dnt_active")
    private boolean is_dnt_active;

    @ColumnInfo(name = "is_current_event")
    private boolean is_current_event;

    @ColumnInfo(name = "last_event_update_date")
    private String last_event_update_date;

    public CalenderEvent(){

    }

    public CalenderEvent(String event_id,String event_name, String event_desc, String start_date, String end_date, String start_date_before_5min,String last_event_update_date, boolean is_dnt_active, boolean is_current_event) {
        this.event_id = event_id;
        this.event_name = event_name;
        this.event_desc = event_desc;
        this.start_date = start_date;
        this.end_date = end_date;
        this.start_date_before_5min = start_date_before_5min;
        this.is_dnt_active = is_dnt_active;
        this.is_current_event = is_current_event;
        this.last_event_update_date = last_event_update_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEvent_name() {
        return event_name;
    }

    public void setEvent_name(String event_name) {
        this.event_name = event_name;
    }

    public String getEvent_desc() {
        return event_desc;
    }

    public void setEvent_desc(String event_desc) {
        this.event_desc = event_desc;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getStart_date_before_5min() {
        return start_date_before_5min;
    }

    public void setStart_date_before_5min(String start_date_before_5min) {
        this.start_date_before_5min = start_date_before_5min;
    }

    public boolean isIs_dnt_active() {
        return is_dnt_active;
    }

    public void setIs_dnt_active(boolean is_dnt_active) {
        this.is_dnt_active = is_dnt_active;
    }

    public boolean isIs_current_event() {
        return is_current_event;
    }

    public void setIs_current_event(boolean is_current_event) {
        this.is_current_event = is_current_event;
    }

    public String getLast_event_update_date() {
        return last_event_update_date;
    }

    public void setLast_event_update_date(String last_event_update_date) {
        this.last_event_update_date = last_event_update_date;
    }

    public String getEvent_id() {
        return event_id;
    }

    public void setEvent_id(String event_id) {
        this.event_id = event_id;
    }
}
