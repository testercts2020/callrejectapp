package com.prudent.callefy.database.call_rej_report;

import androidx.room.Dao;
import androidx.room.Query;

import com.prudent.callefy.database.calender_event.CalenderEvent;

import java.util.List;

@Dao
public interface CallRejReportDao {

    @Query("SELECT * FROM CallRejReport ORDER BY id ASC")
    List<CallRejReport> getAll();

}
