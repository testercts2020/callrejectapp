package com.prudent.callefy.database.groups.group_item;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(indices = {@Index(value = {"contact_no"}, unique = true)})
public class ContGroupItem implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "group_id")
    private int group_id;

    @ColumnInfo(name = "group_name")
    private String group_name;

    @ColumnInfo(name = "group_stage")
    private int group_stage; // its working Group and group contacts are save one table
    // if "group_stage == 1" ==>act group and "group_stage == 2" ==> add contact

    @ColumnInfo(name = "contact_name")
    private String contact_name;

    @ColumnInfo(name = "contact_no")
    private String contact_no;

    @ColumnInfo(name = "sms_content")
    private String sms_content;

    @ColumnInfo(name = "sms_content_id")
    private int sms_content_id;

    @ColumnInfo(name = "call_action")
    private String call_action;

    @ColumnInfo(name = "call_action_id")
    private int call_action_id;

    @ColumnInfo(name = "is_donot_disturb_active")
    private boolean is_donot_disturb_active;

    public ContGroupItem(int group_id, String group_name, String contact_name, String contact_no,
                         String sms_content, int sms_content_id, String call_action,
                         int call_action_id, int group_stage) {

        if (group_stage == 1){
            this.group_name = group_name;
            this.group_stage = group_stage;
        }else {
            this.group_id = group_id;
            this.group_name = group_name;
            this.contact_name = contact_name;
            this.contact_no = contact_no;
            this.sms_content = sms_content;
            this.sms_content_id = sms_content_id;
            this.call_action = call_action;
            this.call_action_id = call_action_id;
            this.group_stage = group_stage;
        }

    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getSms_content() {
        return sms_content;
    }

    public void setSms_content(String sms_content) {
        this.sms_content = sms_content;
    }

    public int getSms_content_id() {
        return sms_content_id;
    }

    public void setSms_content_id(int sms_content_id) {
        this.sms_content_id = sms_content_id;
    }

    public String getCall_action() {
        return call_action;
    }

    public void setCall_action(String call_action) {
        this.call_action = call_action;
    }

    public int getCall_action_id() {
        return call_action_id;
    }

    public void setCall_action_id(int call_action_id) {
        this.call_action_id = call_action_id;
    }


    public int getGroup_stage() {
        return group_stage;
    }

    public void setGroup_stage(int group_stage) {
        this.group_stage = group_stage;
    }

    public boolean isIs_donot_disturb_active() {
        return is_donot_disturb_active;
    }

    public void setIs_donot_disturb_active(boolean is_donot_disturb_active) {
        this.is_donot_disturb_active = is_donot_disturb_active;
    }

    @Override
    public String toString() {
        return "ContGroupItem{" +
                "id=" + id +
                ", group_id=" + group_id +
                ", group_name='" + group_name + '\'' +
                ", contact_name='" + contact_name + '\'' +
                ", contact_no='" + contact_no + '\'' +
                ", sms_content='" + sms_content + '\'' +
                ", sms_content_id='" + sms_content_id + '\'' +
                ", call_action='" + call_action + '\'' +
                ", call_action_id='" + call_action_id + '\'' +
                '}';
    }
}
