package com.prudent.callefy.database.missed_calls;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class MissedCalls implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "date_time")
    private String date_time;

    @ColumnInfo(name = "ph_no")
    private String ph_no;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "icon_text")
    private String icon_text;

    @ColumnInfo(name = "is_sms_sended")
    private boolean is_sms_sended;

    @ColumnInfo(name = "sms_content")
    private String sms_content;

    @ColumnInfo(name = "call_rejected_case")
    private String call_rejected_case; // this number is set group or default

    @ColumnInfo(name = "call_rejected_case_status")
    private String call_rejected_case_status; // call_reject / call_reject_with_sms

    @ColumnInfo(name = "call_rejected_group_name")
    private String call_rejected_group_name;

//    @ColumnInfo(name = "call_rejected_response_sms")
//    private String call_rejected_response_sms;

    @ColumnInfo(name = "is_working_successfully")
    private boolean is_working_successfully;

    @ColumnInfo(name = "uploadStatus")
    private int uploadStatus;


//    public MissedCalls(String date_time, String ph_no, String name, String icon_text, boolean is_sms_send, String sms_content) {
//        this.date_time = date_time;
//        this.ph_no = ph_no;
//        this.name = name;
//        this.icon_text = icon_text;
//        this.is_sms_sended = is_sms_send;
//        this.sms_content = sms_content;
//    }

//    public MissedCalls(String date_time, String ph_no, String name, String icon_text,
//                       boolean is_sms_send, String sms_content, String call_rejected_case,
//                       String call_rejected_case_status, String call_rejected_group_name,
//                       boolean is_working_successfully, int uploadStatus) {
//        this.date_time = date_time;
//        this.ph_no = ph_no;
//        this.name = name;
//        this.icon_text = icon_text;
//        this.is_sms_sended = is_sms_send;
//        this.sms_content = sms_content;
//        this.call_rejected_case = call_rejected_case;
//        this.call_rejected_case_status = call_rejected_case_status;
//        this.call_rejected_group_name = call_rejected_group_name;
//        this.is_working_successfully = is_working_successfully;
//        this.uploadStatus = uploadStatus;
//
//    }


    public MissedCalls(  String date_time, String ph_no, String name, String icon_text, boolean is_sms_sended, String sms_content, String call_rejected_case, String call_rejected_case_status, String call_rejected_group_name, boolean is_working_successfully, int uploadStatus) {

        this.date_time = date_time;
        this.ph_no = ph_no;
        this.name = name;
        this.icon_text = icon_text;
        this.is_sms_sended = is_sms_sended;
        this.sms_content = sms_content;
        this.call_rejected_case = call_rejected_case;
        this.call_rejected_case_status = call_rejected_case_status;
        this.call_rejected_group_name = call_rejected_group_name;
        this.is_working_successfully = is_working_successfully;
        this.uploadStatus = uploadStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getPh_no() {
        return ph_no;
    }

    public void setPh_no(String ph_no) {
        this.ph_no = ph_no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon_text() {
        return icon_text;
    }

    public void setIcon_text(String icon_text) {
        this.icon_text = icon_text;
    }

//    public boolean getIs_sms_send() {
//        return is_sms_send;
//    }
//
//    public void setIs_sms_send(boolean is_sms_send) {
//        this.is_sms_send = is_sms_send;
//    }


    public boolean isIs_sms_sended() {
        return is_sms_sended;
    }

    public void setIs_sms_sended(boolean is_sms_sended) {
        this.is_sms_sended = is_sms_sended;
    }

    public String getSms_content() {
        return sms_content;
    }

    public void setSms_content(String sms_content) {
        this.sms_content = sms_content;
    }

//    public boolean isIs_sms_send() {
//        return is_sms_send;
//    }

    public String getCall_rejected_case() {
        return call_rejected_case;
    }

    public void setCall_rejected_case(String call_rejected_case) {
        this.call_rejected_case = call_rejected_case;
    }

    public String getCall_rejected_case_status() {
        return call_rejected_case_status;
    }

    public void setCall_rejected_case_status(String call_rejected_case_status) {
        this.call_rejected_case_status = call_rejected_case_status;
    }

    public String getCall_rejected_group_name() {
        return call_rejected_group_name;
    }

    public void setCall_rejected_group_name(String call_rejected_group_name) {
        this.call_rejected_group_name = call_rejected_group_name;
    }

//    public String getCall_rejected_response_sms() {
//        return call_rejected_response_sms;
//    }
//
//    public void setCall_rejected_response_sms(String call_rejected_response_sms) {
//        this.call_rejected_response_sms = call_rejected_response_sms;
//    }

    public boolean isIs_working_successfully() {
        return is_working_successfully;
    }

    public void setIs_working_successfully(boolean is_working_successfully) {
        this.is_working_successfully = is_working_successfully;
    }

    public int getUploadStatus() {
        return uploadStatus;
    }

    public void setUploadStatus(int uploadStatus) {
        this.uploadStatus = uploadStatus;
    }



}
