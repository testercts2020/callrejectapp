package com.prudent.callefy.database.groups;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(indices = {@Index(value = {"group_name"}, unique = true)})
public class ContactGroup implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "group_id")
    private String group_id;

    @ColumnInfo(name = "group_name")
    private String group_name;

    @ColumnInfo(name = "call_status_id")
    private int call_status_id;

    @ColumnInfo(name = "call_status")
    private String call_status;

    @ColumnInfo(name = "group_sms_content")
    private String group_sms_content;

    @ColumnInfo(name = "is_group_permanent")
    private boolean is_group_permanent;

//    public ContactGroup(String group_name, String call_status) {
//        this.group_name = group_name;
//        this.call_status = call_status;
//    }

    public ContactGroup(String group_name, int call_status_id, String call_status, String group_sms_content) {
        this.group_name = group_name;
        this.call_status_id = call_status_id;
        this.call_status = call_status;
        this.group_sms_content = group_sms_content;
        this.is_group_permanent = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getCall_status() {
        return call_status;
    }

    public void setCall_status(String call_status) {
        this.call_status = call_status;
    }

    public int getCall_status_id() {
        return call_status_id;
    }

    public void setCall_status_id(int call_status_id) {
        this.call_status_id = call_status_id;
    }

    public String getGroup_sms_content() {
        return group_sms_content;
    }

    public void setGroup_sms_content(String group_sms_content) {
        this.group_sms_content = group_sms_content;
    }

    public boolean isIs_group_permanent() {
        return is_group_permanent;
    }

    public void setIs_group_permanent(boolean is_group_permanent) {
        this.is_group_permanent = is_group_permanent;
    }
}
