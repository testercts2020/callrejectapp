package com.prudent.callefy.database.groups;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.prudent.callefy.database.sms_content.SmsContent;

import java.util.List;

@Dao
public interface ContactGroupDao {

    @Query("SELECT * FROM ContactGroup ORDER BY id ASC")
    List<ContactGroup> getAll();

    @Query("SELECT * FROM ContactGroup WHERE is_group_permanent = :isPermt ORDER BY id ASC")
    List<ContactGroup> getAllContactPermanent(boolean isPermt);

    @Query("SELECT * FROM ContactGroup WHERE id =(SELECT MAX(id) FROM ContactGroup) ORDER BY id ASC")
    List<ContactGroup> getLastContactGp();

    @Query("SELECT * FROM ContactGroup WHERE id = :id  ORDER BY id ASC")
    List<ContactGroup> getContactGpById(int id);

    @Insert
    void insert(ContactGroup task);

    @Delete
    void delete(ContactGroup task);

    @Query("DELETE FROM ContGroupItem WHERE group_id = :id ")
    void deleteGroupContacts(int id);

    @Query("DELETE FROM ContactGroup WHERE is_group_permanent = :isPermanent ")
    void deleteGroupContactsNotPermanent(boolean isPermanent);


    @Update
    void update(ContactGroup task);

    @Query("UPDATE ContactGroup SET is_group_permanent = :isPermanent WHERE id = :id")
    void updateGroupPermanent(int id, boolean isPermanent);



}
