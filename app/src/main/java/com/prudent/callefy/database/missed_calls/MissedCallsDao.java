package com.prudent.callefy.database.missed_calls;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.prudent.callefy.database.calender_event.CalenderEvent;

import java.util.List;

@Dao
public interface MissedCallsDao {

    @Query("SELECT * FROM MissedCalls ORDER BY id DESC")
    List<MissedCalls> getAll();

    @Query("SELECT * FROM MissedCalls WHERE uploadStatus = 0  ORDER BY id ASC")
    List<MissedCalls> getAllMissedCallReport();

    @Insert
    void insert(MissedCalls item);

    @Update
    void update(MissedCalls item);

    @Query("UPDATE MissedCalls SET uploadStatus=1 WHERE uploadStatus = 0")
    void updateAfterUploadReport();
}
