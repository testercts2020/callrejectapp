package com.prudent.callefy.ui.notification;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.database.CommonTransactionsDb;
import com.prudent.callefy.database.NotificationCallLog;
import com.prudent.callefy.model.CallLogsModel;
import com.prudent.callefy.model.CommonDataResponse;
import com.prudent.callefy.model.NotificationsModel;
import com.prudent.callefy.model.NotificationsResponse;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.ui.profile.ProfileActivity;
import com.prudent.callefy.ui.profile.ProfileViewModel;
import com.prudent.callefy.utils.CommonUtils;
import com.prudent.callefy.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationActivity extends BaseActivity implements View.OnClickListener,
        CommonTransactionsDb.CommonTransNotifLogListDbCallBack {


    @BindView(R.id.rvList)
    RecyclerView rvList;
    @BindView(R.id.clNoNotification)
    ConstraintLayout clNoNotification;
    @BindView(R.id.btViewDashboard)
    MaterialButton btViewDashboard;

    NotificationAdapter notificationAdapter;

    private Observer<NotificationsResponse> notificationsResponseObserver;
    private Observer<NotificationsResponse> notificationsLastTimeResponseObserver;
    private NotificationViewModel notificationViewModel;

    public static Intent startActivity(Context context){
        return new Intent(context, NotificationActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Notification");



        if (notificationViewModel == null){
            notificationViewModel = ViewModelProviders.of(this).get(NotificationViewModel.class);
            notificationViewModel.getIsLoading().observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean isLoading) {
                    if (isLoading) {
                        showProgress();
                    } else {
                        dismissProgress();
                    }
                }
            });

            notificationsResponseObserver = this :: onSuccessGetNotifications;
            notificationsLastTimeResponseObserver = this :: onSuccessGetLastNotifications;
        }

        initView();
    }

    @Override
    public void initView() {

        // Removes blinks
        ((SimpleItemAnimator) rvList.getItemAnimator()).setSupportsChangeAnimations(false);

        rvList.setHasFixedSize(true);
        rvList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        btViewDashboard.setOnClickListener(this::onClick);

        mTransactionsDb = new CommonTransactionsDb(getApplicationContext(), this::getNotifCallLogList);

//        mTransactionsDb.deleteAllNotificationCallLog(); // clear call logs

        mTransactionsDb.getNotificationCallLogs();

    }

    @Override
    public int setLayout() {
        return R.layout.activity_notification;
    }

    @Override
    public boolean setToolbar() {
        return true;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btViewDashboard:
                finish();
                break;

        }
    }

    private String getContactName(Context context, String number) {

        String name = null;

        // define the columns I want the query to return
        String[] projection = new String[] {
                ContactsContract.PhoneLookup.DISPLAY_NAME,
                ContactsContract.PhoneLookup._ID};

        // encode the phone number and build the filter URI
        Uri contactUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        // query time
        Cursor cursor = context.getContentResolver().query(contactUri, projection, null, null, null);

        if(cursor != null) {
            if (cursor.moveToFirst()) {
                name =      cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
                Log.v(TAG, "Started uploadcontactphoto: Contact Found @ " + number);
                Log.v(TAG, "Started uploadcontactphoto: Contact name  = " + name);
            } else {
                Log.v(TAG, "Contact Not Found @ " + number);
            }
            cursor.close();
        }
        return name;
    }

    @Override
    public void getNotifCallLogList(List<NotificationCallLog> logs) {
        if (logs == null || logs.size() < 1){

        if (CommonUtils.isNetworkConnected(getApplicationContext()))
            notificationViewModel.getNotification(SharedPrefs.getUserId() , Constants.TAG_API_GET_NOTIFICATIONS)
                    .observe(this, notificationsResponseObserver);
        else
            openNoInternetScreen();

        }else {

            List<CallLogsModel> mCallLogsList = new ArrayList<>();

            for (NotificationCallLog callLog : logs){
                CallLogsModel callLogsModel = new CallLogsModel();
                callLogsModel.setCallerName(callLog.getCallerName());
                callLogsModel.setDateTime(callLog.getDateTime());
                callLogsModel.setHeader(callLog.getHeader());
//                callLogsModel.setHeader(callLog.getHeader());

                mCallLogsList.add(callLogsModel);

            }

            notificationAdapter = new NotificationAdapter(mCallLogsList,
                    NotificationActivity.this);

            rvList.setAdapter(notificationAdapter);

            // get notification last time

            if (logs.size() > 1)
            checkNotificationMissedCalls(logs.get(1).getDateTime());// last date get from second item
        }
    }

    private void checkNotificationMissedCalls(String lastDate){

        SharedPrefs.setString(SharedPrefs.Key.NOTIFICATION_CALLS_LAST_DATE, lastDate);
//        lastDate = Constants.TEMP_LAST_DATE;
        notificationViewModel.getNotificationLastTime(SharedPrefs.getUserId() , Constants.TAG_API_NOTIFICATION_LAST_TIME, lastDate)
                .observe(this, notificationsLastTimeResponseObserver);

    }

    public void onSuccessGetNotifications(NotificationsResponse response){

        if (response.getStatus() == 1 ){

            if (response.getNotification() == null || response.getNotification().size()<1){
                clNoNotification.setVisibility(View.VISIBLE);
                return;
            }

            List<CallLogsModel> mCallLogsList = new ArrayList<>();

            for (NotificationsModel notificationsModel : response.getNotification()){
                CallLogsModel headerCallLogsModel = new CallLogsModel();
                NotificationCallLog headerNotifLogs = new NotificationCallLog();
                headerCallLogsModel.setHeader(true);
                headerNotifLogs.setHeader(true);

                if (notificationsModel.getDateTime().equals(CommonUtils.getTodayDate()))
                    headerCallLogsModel.setDateTime(getString(R.string.today));
                else
                    headerCallLogsModel.setDateTime(notificationsModel.getDateTime()+" 23:59:50");
                mCallLogsList.add(headerCallLogsModel);

                headerNotifLogs.setDateTime(notificationsModel.getDateTime().trim()+" 23:59:50");
                //// save db
                mTransactionsDb.saveNotificationCallLogs(headerNotifLogs);

                for (CallLogsModel callLogsModel : notificationsModel.getCallLogs()){
                    NotificationCallLog subNotifLogs = new NotificationCallLog();

                    String name = getContactName(this , callLogsModel.getFromNo());
                    if (name == null || name.equals("")){
                        callLogsModel.setCallerName(callLogsModel.getFromNo());
                        subNotifLogs.setCallerName(callLogsModel.getFromNo());
                    }else{
                        callLogsModel.setCallerName(name);
                        subNotifLogs.setCallerName(name);
                    }

                    subNotifLogs.setHeader(false);
                    subNotifLogs.setFromNo(callLogsModel.getFromNo());
                    subNotifLogs.setDateTime(callLogsModel.getDateTime());
                    subNotifLogs.setForwardingNo(callLogsModel.getForwardingNo());

                    mCallLogsList.add(callLogsModel);
                    log(getContactName(this, callLogsModel.getFromNo()));

                    // todo save db

                    mTransactionsDb.saveNotificationCallLogs(
                            subNotifLogs
                    );


                }
            }

            mTransactionsDb.getNotificationCallLogs();
//            notificationAdapter = new NotificationAdapter(mCallLogsList,
//                    NotificationActivity.this);
//
//            rvList.setAdapter(notificationAdapter);

        }else {
            clNoNotification.setVisibility(View.VISIBLE);
        }

    }

    public void onSuccessGetLastNotifications(NotificationsResponse response){
        if (response.getStatus() == 1 ){

            String notLastDate = SharedPrefs.getString(SharedPrefs.Key.NOTIFICATION_CALLS_LAST_DATE, "");
            List<CallLogsModel> mCallLogsList = new ArrayList<>();

            for (NotificationsModel notificationsModel : response.getNotification()){
                CallLogsModel headerCallLogsModel = new CallLogsModel();
                NotificationCallLog headerNotifLogs = new NotificationCallLog();
                headerCallLogsModel.setHeader(true);
                headerNotifLogs.setHeader(true);

                if (notificationsModel.getDateTime().equals(CommonUtils.getTodayDate()))
                    headerCallLogsModel.setDateTime(getString(R.string.today));
                else
                    headerCallLogsModel.setDateTime(notificationsModel.getDateTime()+" 23:59:50");
//                mCallLogsList.add(headerCallLogsModel);

                headerNotifLogs.setDateTime(notificationsModel.getDateTime().trim()+" 23:59:50");
                //// save db
//                mTransactionsDb.saveNotificationCallLogs(headerNotifLogs);

                if (!CommonUtils.checkSameDay(notLastDate, headerNotifLogs.getDateTime())) {
                    mTransactionsDb.saveNotificationCallLogs(headerNotifLogs);
                    notificationAdapter.addItemNotificationAdapter(headerCallLogsModel,true);
                }

                for (CallLogsModel callLogsModel : notificationsModel.getCallLogs()){
                    NotificationCallLog subNotifLogs = new NotificationCallLog();

                    String name = getContactName(this , callLogsModel.getFromNo());
                    if (name == null || name.equals("")){
                        callLogsModel.setCallerName(callLogsModel.getFromNo());
                        subNotifLogs.setCallerName(callLogsModel.getFromNo());
                    }else{
                        callLogsModel.setCallerName(name);
                        subNotifLogs.setCallerName(name);
                    }

                    subNotifLogs.setHeader(false);
                    subNotifLogs.setFromNo(callLogsModel.getFromNo());
                    subNotifLogs.setDateTime(callLogsModel.getDateTime());
                    subNotifLogs.setForwardingNo(callLogsModel.getForwardingNo());

                    mCallLogsList.add(callLogsModel);
                    log(getContactName(this, callLogsModel.getFromNo()));

                    // todo save db

//                    notificationsModel.getDateTime();
//                    if (callLogsModel.isHeader()){
//                        if (!CommonUtils.checkSameDay(notLastDate, callLogsModel.getDateTime())) {
//                            mTransactionsDb.saveNotificationCallLogs(subNotifLogs);
//                            notificationAdapter.addItemNotificationAdapter(callLogsModel);
//                        }
//                    }else {
                        mTransactionsDb.saveNotificationCallLogs(subNotifLogs);
                        notificationAdapter.addItemNotificationAdapter(callLogsModel,false);

//                    }



                }
            }


//            notificationAdapter = new NotificationAdapter(mCallLogsList,
//                    NotificationActivity.this);
//
//            rvList.setAdapter(notificationAdapter);

        }
    }

}