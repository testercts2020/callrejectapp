package com.prudent.callefy.ui.login;

import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import com.prudent.callefy.api.ApiClient;
import com.prudent.callefy.base.BaseViewModel;
import com.prudent.callefy.model.CommonDataResponse;
import com.prudent.callefy.model.LoginResponse;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.utils.Constants;

import java.util.HashMap;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LoginViewModel extends BaseViewModel {
    private MutableLiveData<LoginResponse> loginResponseMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<LoginResponse> otpResponseMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<LoginResponse> login(String mobile_no,
                                                String tag,
                                                String operatorname,
                                                String imei,
                                                String model,
                                                String manufacturer,
                                                String brand,
                                                String sdk,
                                                String device_relese_no) {

        HashMap<String, String> body = new HashMap<>();
        body.put("mobile_no", mobile_no);
        body.put("tag", tag);
        body.put("operatorname", operatorname);
        body.put("imei", imei);
        body.put("model", model);
        body.put("manufacturer", manufacturer);
        body.put("brand", brand);
        body.put("sdk", sdk);
        body.put("device_relese_no", device_relese_no);

        ApiClient.getApiInterface().commonApi(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<LoginResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        setIsLoading(true);
                    }

                    @Override
                    public void onSuccess(LoginResponse videoResponse) {
                        setIsLoading(false);
                        loginResponseMutableLiveData.postValue(videoResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);

                    }
                });
        return loginResponseMutableLiveData;
    }

    public MutableLiveData<LoginResponse> otpVerify(String otp) {

        HashMap<String, String> body = new HashMap<>();
        body.put("user_id", SharedPrefs.getUserId());
        body.put("otp", otp);
        body.put("tag", Constants.TAG_API_OTP_VERIFY);


        ApiClient.getApiInterface().commonApi(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<LoginResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        setIsLoading(true);
                    }

                    @Override
                    public void onSuccess(LoginResponse videoResponse) {
                        setIsLoading(false);
                        otpResponseMutableLiveData.postValue(videoResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);
                    }
                });
        return otpResponseMutableLiveData;
    }

}
