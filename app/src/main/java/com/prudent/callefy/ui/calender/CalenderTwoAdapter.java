package com.prudent.callefy.ui.calender;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.prudent.callefy.R;
import com.prudent.callefy.database.calender_event.CalenderEvent;
import com.prudent.callefy.model.CalenderModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CalenderTwoAdapter extends RecyclerView.Adapter<CalenderTwoAdapter.ViewHolder> {

    private List<CalenderEvent> mCalenderList = new ArrayList<>();
    Context mContext;
    CalenderCallback mCalenderCallback;

    public CalenderTwoAdapter(List<CalenderEvent> mCalenderList, Context mContext, CalenderCallback mCalenderCallback) {
        this.mCalenderList = mCalenderList;
        this.mContext = mContext;
        this.mCalenderCallback = mCalenderCallback;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_calender_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CalenderEvent item = mCalenderList.get(position);
        holder.tvEvent.setText(item.getEvent_name());
        holder.tvStartDate.setText("Start date : "+item.getStart_date());
        holder.tvEndDate.setText("End Date : "+item.getEnd_date());
        holder.tvDesc.setText("Description : "+item.getEvent_desc());
        if (item.isIs_current_event()){
            holder.tvEvent.setBackgroundColor(mContext.getColor(R.color.colorGreen));
        }
        else {
            holder.tvEvent.setBackgroundColor(mContext.getColor(R.color.colorPrimaryDark));
        }

        if (item.isIs_dnt_active()){
            holder.btActive.setVisibility(View.GONE);
            holder.btInActive.setVisibility(View.VISIBLE);
        }else {
            holder.btActive.setVisibility(View.VISIBLE);
            holder.btInActive.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mCalenderList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tvEvent)
        TextView tvEvent;
        @BindView(R.id.tvStartDate)
        TextView tvStartDate;
        @BindView(R.id.tvEndDate)
        TextView tvEndDate;
        @BindView(R.id.tvDesc)
        TextView tvDesc;
        @BindView(R.id.btActive)
        MaterialButton btActive;
        @BindView(R.id.btInActive)
        MaterialButton btInActive;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            btActive.setOnClickListener(this::onClick);
            btInActive.setOnClickListener(this::onClick);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btActive:
//                    mCalenderCallback.active();
                    mCalenderCallback.onCalenderDndActive(mCalenderList.get(getAdapterPosition()));
                    mCalenderList.get(getAdapterPosition()).setIs_dnt_active(true);
                    notifyDataSetChanged();
//                    mCalenderList.set(getAdapterPosition(), )
                    break;
                case R.id.btInActive:
                    mCalenderCallback.onCalenderDndInActive(mCalenderList.get(getAdapterPosition()));
                    mCalenderList.get(getAdapterPosition()).setIs_dnt_active(false);
                    notifyDataSetChanged();
//                    mCalenderCallback.inActive();
                    break;
            }
        }
    }
}
