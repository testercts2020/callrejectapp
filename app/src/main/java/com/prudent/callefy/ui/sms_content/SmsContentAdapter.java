package com.prudent.callefy.ui.sms_content;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.prudent.callefy.R;
import com.prudent.callefy.database.sms_content.SmsContent;
import com.prudent.callefy.model.CalenderModel;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.ui.calender.CalenderCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SmsContentAdapter extends RecyclerView.Adapter<SmsContentAdapter.ViewHolder>{

    private List<SmsContent> mSmsContentsList = new ArrayList<>();
    Context mContext;
    SmsContentCallback mSmsContentCallback;
    boolean isCameFromGroupItemPage;
    String temp_default_sms_content="";
    boolean isRadioButtonClickable= true;

    public SmsContentAdapter(List<SmsContent> mSmsContentsList, Context mContext,
                             SmsContentCallback mSmsContentCallback, boolean isCameFromGroupItemPage) {
        this.mSmsContentsList = mSmsContentsList;
        this.mContext = mContext;
        this.mSmsContentCallback = mSmsContentCallback;
        this.isCameFromGroupItemPage = isCameFromGroupItemPage;
    }

    public void addNewSmsContent(SmsContent smsContent){
        int insertIndex = 0;
        mSmsContentsList.add(insertIndex, smsContent);
        notifyItemInserted(insertIndex);
       notifyDataSetChanged();

    }

    public void radioButtonClick(boolean isClick){
        isRadioButtonClickable = isClick;
        notifyDataSetChanged();
    }

    public void removeItem(int position){

        mSmsContentsList.remove(position);
        notifyItemInserted(position);
        notifyDataSetChanged();

    }

    public void updateItem(int position, String sms){

        mSmsContentsList.get(position).setSmsContent(sms);
        notifyItemInserted(position);
        notifyDataSetChanged();

    }

    public void updateGroupMessage(String groupMessage){

        temp_default_sms_content = groupMessage;

//        mSmsContentsList.get(position).setSmsContent(sms);
//        notifyItemInserted(position);
        notifyDataSetChanged();

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sms_content, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SmsContent item = mSmsContentsList.get(position);
        holder.tvMessage.setText(item.getSmsContent());
        if (temp_default_sms_content.equals(item.getSmsContent()))
            holder.rbSelect.setChecked(true);
        else
            holder.rbSelect.setChecked(false);

        if (isRadioButtonClickable){
            holder.rbSelect.setClickable(true);
        }else {
            holder.rbSelect.setClickable(false);
        }
//        if (isCameFromGroupItemPage){
//            if (temp_default_sms_content.equals(item.getSmsContent()))
//                holder.rbSelect.setChecked(true);
//            else
//                holder.rbSelect.setChecked(false);
//        }else {
//            String defaultMessage = SharedPrefs.getString(SharedPrefs.Key.DEFAULT_SMS_CONTENT,"");
//            if (defaultMessage.equals(item.getSmsContent()))
//                holder.rbSelect.setChecked(true);
//            else
//                holder.rbSelect.setChecked(false);
//        }

    }

    @Override
    public int getItemCount() {
        return mSmsContentsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.rbSelect)
        AppCompatRadioButton rbSelect;
        @BindView(R.id.tvMessage)
        TextView tvMessage;
        @BindView(R.id.btEdit)
        MaterialButton btEdit;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            btEdit.setOnClickListener(this::onClick);
            rbSelect.setOnClickListener(this::onClick);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btEdit:
                    mSmsContentCallback.editSmsContent(mSmsContentsList.get(getAdapterPosition())
                            .getSmsContent(), mSmsContentsList.get(getAdapterPosition())
                            .getId(),getAdapterPosition());
                    break;
//                case R.id.btSetAsDefault:
//                    if (isCameFromGroupItemPage){
//                        temp_default_sms_content = mSmsContentsList
//                                .get(getAdapterPosition()).getSmsContent();
//                        mSmsContentCallback.setGroupItemSmsContent(mSmsContentsList
//                                .get(getAdapterPosition()).getSmsContent());
//                    }else {
//                        SharedPrefs.setString(SharedPrefs.Key.DEFAULT_SMS_CONTENT, mSmsContentsList
//                                .get(getAdapterPosition()).getSmsContent());
//
//                    }
//                    notifyDataSetChanged();
//
//                    break;
                case R.id.rbSelect:
                    temp_default_sms_content = mSmsContentsList.get(getAdapterPosition())
                            .getSmsContent();
                    mSmsContentCallback.setGroupItemSmsContent(mSmsContentsList
                            .get(getAdapterPosition()).getSmsContent());
//                    if (isCameFromGroupItemPage){
//                        temp_default_sms_content = mSmsContentsList
//                                .get(getAdapterPosition()).getSmsContent();
//                        mSmsContentCallback.setGroupItemSmsContent(mSmsContentsList
//                                .get(getAdapterPosition()).getSmsContent());
//                    }else {
//                        SharedPrefs.setString(SharedPrefs.Key.DEFAULT_SMS_CONTENT, mSmsContentsList
//                                .get(getAdapterPosition()).getSmsContent());
//
//                    }
                    notifyDataSetChanged();
                    break;
            }
        }
    }

}
