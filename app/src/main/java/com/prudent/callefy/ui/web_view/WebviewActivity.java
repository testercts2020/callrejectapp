package com.prudent.callefy.ui.web_view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.ui.feedback.FeedbackActivity;

public class WebviewActivity extends BaseActivity {

    WebView webview;

    public static Intent startActivity(Context context) {
        return new Intent(context, WebviewActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_webview);



        webview=(WebView)findViewById(R.id.help_webview);
        webview.setWebViewClient(new MyWebViewClient());
        openURL();

    }

    private void openURL() {
        webview.loadUrl("https://prutech.org/callefyapi/home/help");
        webview.requestFocus();
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public int setLayout() {
        return R.layout.activity_webview;
    }

    @Override
    public void initView() {

    }

    @Override
    public boolean setToolbar() {
        return false;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }
}