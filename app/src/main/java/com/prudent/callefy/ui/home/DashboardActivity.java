package com.prudent.callefy.ui.home;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.database.AppErrorTask;
import com.prudent.callefy.database.CommonTransactionsDb;
import com.prudent.callefy.dialog_fragment.ussd_code_issue.OnUssdCodeErrorDialogClickListener;
import com.prudent.callefy.model.CommonDataResponse;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.ui.calender.CalenderActivity;
import com.prudent.callefy.ui.feedback.FeedbackActivity;
import com.prudent.callefy.ui.home.missed_calls.MissedCallsFragment;
import com.prudent.callefy.ui.home.settings.SettingsFragment;
import com.prudent.callefy.ui.login.LoginActivity;
import com.prudent.callefy.ui.notification.CallLogsActivity;
import com.prudent.callefy.ui.profile.ProfileActivity;
import com.prudent.callefy.ui.settings.SettingsActivity;
import com.prudent.callefy.utils.CommonUtils;
import com.prudent.callefy.utils.Constants;
import com.prudent.callefy.utils.ForegroundService;
import com.prudent.callefy.utils.MyWorker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;



public class DashboardActivity extends BaseActivity implements View.OnClickListener,
        DashboardCallback, OnUssdCodeErrorDialogClickListener,

        CommonTransactionsDb.CommonTransAppErrorListDbCallBack {

//    PopupMenu.OnMenuItemClickListe

    @BindView(R.id.lltRow5)
    RelativeLayout lltRow5;
    @BindView(R.id.lltRowServiceDeactivation)
    RelativeLayout lltRow6;
    @BindView(R.id.lltRow3)
    RelativeLayout lltRow3;
    @BindView(R.id.lltRow4)
    RelativeLayout lltRow4;
    @BindView(R.id.lltRowCalender)
    RelativeLayout lltRowCalender;
    @BindView(R.id.lltRowFeedback)
    RelativeLayout lltRowFeedback;
    @BindView(R.id.lltRowSettings)
    RelativeLayout lltSettings;
    @BindView(R.id.btStartForeground)
    Button btStartForeground;
    @BindView(R.id.btStopForeground)
    Button btStopForeground;
    @BindView(R.id.btCountForeground)
    Button btCountForeground;
    @BindView(R.id.btSaveDataToDb)
    Button btSaveDataToDb;
    @BindView(R.id.btGetDataFromDb)
    Button btGetDataFromDb;
    @BindView(R.id.btCallNumber)
    Button btCallNumber;
    @BindView(R.id.btSendUssdCodeNormalPhone)
    Button btSendUssdCodeNormalPhone;
    //    @BindView(R.id.tvCurrentStatus)
    public static TextView tvCurrentStatus;

    private Observer<CommonDataResponse> dataResponseObserver;
    private Observer<CommonDataResponse> failureCaseResponseObserver;
    private DashboardViewModel dashboardViewModel;

    public CommonTransactionsDb mTransactionsDb;

    public AudioManager audioManager;
    public static boolean isCheckVoipCall = true;
    public static int noOfChecking = 1;
    public static boolean isStartBackground = false;

    private static final int MAKE_CALL_PERMISSION_REQUEST_CODE = 1;


    ViewPager2 viewPager2;
    TabLayout tab_layout;
    ViewpagerAdapter viewpagerAdapter;
    private ArrayList<Fragment> arrayList = new ArrayList<>();

    public static Intent startActivity(Context context) {
        SharedPrefs.setBoolean(SharedPrefs.Key.IS_LOGIN, true);
        return new Intent(context, DashboardActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setTitle("BuziBee");

        if (!checkPermission()) {
            startActivity(LoginActivity.startActivity(getApplicationContext()));

            finish();
            return;
        } else {
            requestPermission();
        }

//        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }

        initView();
        mTransactionsDb = new CommonTransactionsDb(getApplicationContext(), this::getAppErrorTaskList);
        checkBackgroundService();

        SharedPrefs.setInt(SharedPrefs.Key.
                NUMBER_OF_CHECK_USSD_CODE_ACT_DEACT, 0);
        SharedPrefs.setBoolean(SharedPrefs.Key.REMOVE_UNEXPECTED_BACKGROUND_THREAD, false);

        if (!SharedPrefs.getBoolean(SharedPrefs.Key.IS_UPDATE_DEVICE_INFO, false)) {
            getDeviceInfo();
        }
        SharedPrefs.setBoolean(SharedPrefs.Key.IS_START_BACKGROUND_SERVICE, false);
        if (!SharedPrefs.getBoolean(SharedPrefs.Key.IS_START_BACKGROUND_SERVICE, false)) {
            Intent intentStart = new Intent(this, ForegroundService.class);
            intentStart.setAction(ForegroundService.ACTION_START_FOREGROUND_SERVICE);
            startService(intentStart);
        }

        if (dashboardViewModel == null) {
            dashboardViewModel = ViewModelProviders.of(this).get(DashboardViewModel.class);
            dashboardViewModel.getIsLoading().observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean isLoading) {
                    if (isLoading) {
                        showProgress();
                    } else {
                        dismissProgress();
                    }

                }
            });
            dataResponseObserver = this::onSuccessUpdateDeviceInfo;
            failureCaseResponseObserver = this::onSuccessUpdateFailureCase;

        }

        init();
    }

    private void init() {
        // removing toolbar elevation
        getSupportActionBar().setElevation(0);


    }

    @Override
    protected void onResume() {
        super.onResume();
//        tvCurrentStatus.setText(SharedPrefs.getString(SharedPrefs.Key.DASHBOARD_CURRENT_STATUS,
//                getString(R.string.now_you_are_normal_state)));
//        boolean isDNG = SharedPrefs.isGroupsDoNotDisturbActive();

        if (SharedPrefs.isManualDoNotDisturbActive() || SharedPrefs.isGroupsDoNotDisturbActive() ||
                SharedPrefs.isDoNotDisturbActive()) {
            Log.d(TAG, "onReceive aaaaa:  nnnnnnnnnn11111");
            tvCurrentStatus.setText(getString(R.string.in_do_not_disturb_state));
        } else {
            tvCurrentStatus.setText(getString(R.string.now_you_are_normal_state));
            Log.d(TAG, "onReceive aaaaa:  nnnnnnnnnn222222");

        }



        boolean isDNM = SharedPrefs.isManualDoNotDisturbActive();
        boolean isDN = SharedPrefs.isDoNotDisturbActive();
        boolean isDNG = SharedPrefs.isGroupsDoNotDisturbActive();

        if (isDNM){
            Log.d(TAG, "dnm 1111");
        }
        if (isDN){
            Log.d(TAG, "dnm 2222");
        }
        if (isDNG){
            Log.d(TAG, "dnm 3333");
        }



        if (isDNM || isDN|| isDNG){
            Log.d(TAG, "onReceive aaaaa:  nnnnnnnnnn");
            tvCurrentStatus.setText(getString(R.string.in_do_not_disturb_state));

        }

    }

    private boolean checkPermission() {
        String[] permissionList = new String[]{
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.READ_SMS,
                Manifest.permission.RECEIVE_SMS,
                Manifest.permission.SEND_SMS,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.READ_CALL_LOG,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.RECEIVE_BOOT_COMPLETED,
                Manifest.permission.WRITE_CALENDAR,
                Manifest.permission.READ_CALENDAR,


        };

//        Manifest.permission.READ_PHONE_NUMBERS,
//        if (ContextCompat. checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) ==)
//        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

        boolean isPermission = true;
        for (String str : permissionList) {
            int result = ContextCompat.checkSelfPermission(this, str);

            if (result == PackageManager.PERMISSION_DENIED) {
                isPermission = false;
//                return false;
            }
        }
        return isPermission;

    }

    private void requestPermission() {
        if (!checkPermission()) {
            requestPermissions(new String[]{
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.READ_PHONE_NUMBERS,
                    Manifest.permission.READ_SMS,
                    Manifest.permission.RECEIVE_SMS,
                    Manifest.permission.SEND_SMS,
                    Manifest.permission.CALL_PHONE,
                    Manifest.permission.READ_CALL_LOG,
                    Manifest.permission.READ_CONTACTS,
                    Manifest.permission.RECEIVE_BOOT_COMPLETED,
                    Manifest.permission.SYSTEM_ALERT_WINDOW,
                    Manifest.permission.WRITE_CALENDAR,
                    Manifest.permission.READ_CALENDAR,

                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_CALL_LOG,
                    Manifest.permission.MODIFY_PHONE_STATE,
                    Manifest.permission.ANSWER_PHONE_CALLS
            }, 0);
        }

    }

    public void onSuccessUpdateDeviceInfo(CommonDataResponse response) {
        if (response.getStatus() == 1) {
            showToast(response.getMessage());
            SharedPrefs.setBoolean(SharedPrefs.Key.IS_UPDATE_DEVICE_INFO, true);
        } else {
            showToast(response.getMessage());
        }
    }

    private void onSuccessUpdateFailureCase(CommonDataResponse response) {
        if (response.getStatus() == 1) {
            mTransactionsDb.updateStatusAfterSync();

        } else {
            showToast(response.getMessage());
        }
    }

    public void backgroundWorkManager() {
        //creating a data object
        //to pass the data with workRequest
        //we can put as many variables needed
        Data data = new Data.Builder()
                .putString(MyWorker.TASK_DESC, "The task data passed from MainActivity")
                .build();

        //creating constraints
        Constraints constraints = new Constraints.Builder()
                .setRequiresCharging(true) // you can add as many constraints as you want
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .setRequiresDeviceIdle(true)

                .build();

        final OneTimeWorkRequest workRequest =
                new OneTimeWorkRequest.Builder(MyWorker.class)
                        .setInputData(data)
                        .setConstraints(constraints)
                        .build();

        final PeriodicWorkRequest periodicWorkRequest
                = new PeriodicWorkRequest.Builder(MyWorker.class, 10, TimeUnit.SECONDS)
                .build();


        WorkManager.getInstance().enqueue(periodicWorkRequest);
    }

    @Override
    public void initView() {

        tvCurrentStatus = findViewById(R.id.tvCurrentStatus);

        viewPager2 = findViewById(R.id.viewPager);
        tab_layout = findViewById(R.id.tabLayout);

        // add Fragments in your ViewPagerFragmentAdapter class
        arrayList.add(new MissedCallsFragment());
        arrayList.add(new SettingsFragment());

        lltRow3.setOnClickListener(this::onClick);
        lltRow4.setOnClickListener(this::onClick);
        lltRow5.setOnClickListener(this::onClick);
        lltRow6.setOnClickListener(this::onClick);
        lltRowCalender.setOnClickListener(this::onClick);
        lltRowFeedback.setOnClickListener(this::onClick);
        lltSettings.setOnClickListener(this::onClick);

        btStartForeground.setOnClickListener(this::onClick);
        btStopForeground.setOnClickListener(this::onClick);
        btCountForeground.setOnClickListener(this::onClick);
        btSaveDataToDb.setOnClickListener(this::onClick);
        btGetDataFromDb.setOnClickListener(this::onClick);
        btCallNumber.setOnClickListener(this::onClick);
        btSendUssdCodeNormalPhone.setOnClickListener(this::onClick);


        viewpagerAdapter = new ViewpagerAdapter(getSupportFragmentManager(), getLifecycle());
        // set Orientation in your ViewPager2
        viewPager2.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);

        viewPager2.setAdapter(viewpagerAdapter);

//        viewPager2.setPageTransformer(new MarginPageTransformer(1500));

        new TabLayoutMediator(tab_layout, viewPager2, (tab, position) -> {
            switch (position) {
                case 0:
                    tab.setText("Missed calls");
                    break;
                case 1:
                    tab.setText("Settings");
                    break;


            }
        }).attach();


    }

    @Override
    public int setLayout() {
        return R.layout.activity_dashboard;
    }

    @Override
    public boolean setToolbar() {
        return true;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }





    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lltRow5:
                startActivity(ProfileActivity.startActivity(getApplicationContext()));
                break;
            case R.id.lltRowServiceDeactivation:


                break;
            case R.id.lltRowSettings:

                startActivity(SettingsActivity.startActivity(getApplicationContext()));
                break;
            case R.id.lltRow3:
                startActivity(CallLogsActivity.startActivity(getApplicationContext()));

//                if (CommonUtils.isNetworkConnected(getApplicationContext()))
//                    startActivity(NotificationActivity.startActivity(getApplicationContext()));
//                else
//                    openNoInternetScreen();
                break;
            case R.id.lltRow4:
                startActivity(CallLogsActivity.startActivity(getApplicationContext()));
                break;
            case R.id.btStartForeground:
                Intent intentStart = new Intent(this, ForegroundService.class);
                intentStart.setAction(ForegroundService.ACTION_START_FOREGROUND_SERVICE);
                startService(intentStart);
                break;
            case R.id.btStopForeground:
                stopService(new Intent(this, ForegroundService.class));
                break;
            case R.id.btCountForeground:
                log("Count : " + noOfChecking);
                showToast("count  " + noOfChecking);
                break;
            case R.id.lltRowCalender:
                startActivity(CalenderActivity.startActivity(getApplicationContext()));
                break;
            case R.id.lltRowFeedback:
                startActivity(FeedbackActivity.startActivity(getApplicationContext()));
                break;
            case R.id.btSaveDataToDb:

                if (mTransactionsDb == null) {
                    mTransactionsDb = new CommonTransactionsDb(getApplicationContext());
                }
                mTransactionsDb.saveErrorSituation("LoginActivity", "Call Login api", "True");

                break;
            case R.id.btGetDataFromDb:

                if (mTransactionsDb == null) {
                    mTransactionsDb = new CommonTransactionsDb(getApplicationContext());
                }
                mTransactionsDb.getTasks();

                break;
            case R.id.btCallNumber:
                String dial = "tel:" + "8129779445";
                startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
                break;

            case R.id.btSendUssdCodeNormalPhone:
                String cUssd = "9072670614";
//                String cToSend = "tel:" + Uri.encode("*")+Uri.encode("*")+"002"+
//                        Uri.encode("*") + cUssd + Uri.encode("#");
                String cToSend = "tel:" + Uri.encode("*") + "401" +
                        Uri.encode("*") + cUssd + Uri.encode("#");
                startActivityForResult(new Intent("android.intent.action.CALL",
                        Uri.parse(cToSend)), 30);
                break;

        }
    }

    private void checkFirstTimeUssdCheck() {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            placeUssdRequestAct_DeAct_ForCheck();
    }

    public void checkBackgroundService() {

        SharedPrefs.setInt(SharedPrefs.Key.OPERATOR_BACKGROUND_RUNNIG_STATUS, 1);
    }

    /*  Get Device info  start *****/

    public void getDeviceInfo() {
        StringBuffer infoBuffer = new StringBuffer();

        infoBuffer.append("-------------------------------------\n");
        infoBuffer.append("Model :" + Build.MODEL + "\n");//The end-user-visible name for the end product.
        infoBuffer.append("Device: " + Build.DEVICE + "\n");//The name of the industrial design.
        infoBuffer.append("Manufacturer: " + Build.MANUFACTURER + "\n");//The manufacturer of the product/hardware.
        infoBuffer.append("Board: " + Build.BOARD + "\n");//The name of the underlying board, like "goldfish".
        infoBuffer.append("Brand: " + Build.BRAND + "\n");//The consumer-visible brand with which the product/hardware will be associated, if any.
        infoBuffer.append("Serial: " + Build.SERIAL + "\n");
        infoBuffer.append("sdk : " + Build.VERSION.SDK_INT + "\n");
        infoBuffer.append("Relese : " + Build.VERSION.RELEASE + "\n");
        infoBuffer.append("-------------------------------------\n");

        log(infoBuffer.toString());

        SharedPrefs.setString(SharedPrefs.Key.MODEL, Build.MODEL);
        SharedPrefs.setString(SharedPrefs.Key.MANUFACTURER, Build.MANUFACTURER);
        SharedPrefs.setString(SharedPrefs.Key.BRAND, Build.BRAND);
        SharedPrefs.setString(SharedPrefs.Key.SDK, Build.VERSION.SDK_INT + "");
        SharedPrefs.setString(SharedPrefs.Key.DEVICE_RELEASE_NO, Build.VERSION.RELEASE);
        SharedPrefs.setString(SharedPrefs.Key.IMEI, Build.SERIAL);


        getSimInformation();

//        1 0
//        .1.1.0.1..1


    }

    /* ****** Get Device info  END *****/

    /*Get SIM info  start *****/

    public void getSimInformation() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telephonyManager.getSimState();

        switch (simState) {

            case (TelephonyManager.SIM_STATE_ABSENT):
                break;
            case (TelephonyManager.SIM_STATE_NETWORK_LOCKED):
                break;
            case (TelephonyManager.SIM_STATE_PIN_REQUIRED):
                break;
            case (TelephonyManager.SIM_STATE_PUK_REQUIRED):
                break;
            case (TelephonyManager.SIM_STATE_UNKNOWN):
                break;
            case (TelephonyManager.SIM_STATE_READY): {

                // Get the SIM country ISO code
                String simCountry = telephonyManager.getSimCountryIso();
                log(simCountry);

                // Get the operator code of the active SIM (MCC + MNC)
                String simOperatorCode = telephonyManager.getSimOperator();
                log(simOperatorCode);

                // Get the name of the SIM operator
                String simOperatorName = telephonyManager.getSimOperatorName();
                log(simOperatorName);
                SharedPrefs.setString(SharedPrefs.Key.OPERATOR, simOperatorName);

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    //  TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    String simNumber = telephonyManager.getLine1Number();
                    log(simNumber);
                    return;
                }
                String simNumber = telephonyManager.getLine1Number();
                log(simNumber);

                String simNumber2 = telephonyManager.getVoiceMailNumber();
                log(simNumber2);
                String simNumber3 = telephonyManager.getDeviceSoftwareVersion();
                log(simNumber3);


                TelephonyManager mTelephonyMgr;
                mTelephonyMgr = (TelephonyManager)
                        getSystemService(Context.TELEPHONY_SERVICE);
                mTelephonyMgr.getLine1Number();
                log(mTelephonyMgr.getLine1Number());

//                updateDeviceInfo();

//                getSimInfoUsingSubC();
                // Get the SIMâ€™s serial number
//                String simSerial = telephonyManager.getSimSerialNumber();

            }
        }
    }

    /* ****** Get SIM info  END *****/

    /* Start background Check voip call  start *****/


    public void runBackground() {
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        if (audioManager.getMode() == AudioManager.MODE_IN_COMMUNICATION) {
            Log.d("TAG==>  ", "onCreate: MODE_IN_COMMUNICATION");
//            Toast.makeText(getApplicationContext(), "MODE_IN_COMMUNICATION",Toast.LENGTH_SHORT).show();

        } else {
            Log.d("TAG==>  ", "onCreate: NOT MODE_IN_COMMUNICATION");
//            Toast.makeText(getApplicationContext(), "NOT MODE_IN_COMMUNICATION",Toast.LENGTH_SHORT).show();

        }
        runThreadBackground();
    }

    public void runThreadBackground() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isCheckVoipCall) {
                    noOfChecking++;
                    showToast("count  " + noOfChecking + "  " + isStartBackground);
                    runBackground();
                }

            }
        }, 10000);
    }

    /* ******  END background Check voip call  END *****/

    public void placeUssdRequestAct_DeAct_ForCheck() {
        int noOfCountActDeactCheck = SharedPrefs.getInt(SharedPrefs.Key.
                NUMBER_OF_CHECK_USSD_CODE_ACT_DEACT, 0);

        if (!SharedPrefs.getBoolean(SharedPrefs.Key.
                IS_CURRENT_NOW_CHECK_USSD_ACT_DE_ACTIVATION, true)) {
            return;
        }

        boolean checkUssdActivation = SharedPrefs.getBoolean(SharedPrefs.Key.IS_CHECK_USSD_ACTIVATION,
                false);

        boolean checkUssdDeActivation = SharedPrefs.getBoolean(SharedPrefs.Key.IS_CHECK_USSD_DE_ACTIVATION,
                false);

        if (noOfCountActDeactCheck > 3) {
            // todo program for pop up for your act and deact code error
            if (!checkUssdActivation) {

                String message = getString(R.string.ussd_error_message);
                String txtCancel = getString(R.string.cancel);
                String txtAllow = getString(R.string.send_report);

                CommonUtils.showCommonMessageDialog(this, null, "", message, txtCancel, txtAllow);

            }
        }

        if (noOfCountActDeactCheck > 3) {
            return;
        }


    }

    @Override
    public void changeStatus(String status) {

    }

    @Override
    public void onDelete() {
        showToast("Delte");
    }

    @Override
    public void onCancel() {
        showToast("cancel");
    }

    @Override
    public void getAppErrorTaskList(List<AppErrorTask> tasks) {
        if (tasks != null && tasks.size() > 0) {
            if (CommonUtils.isNetworkConnected(getApplicationContext()))
                dashboardViewModel.updateAppFailureCase(SharedPrefs.getUserId(),
                        Constants.TAG_API_APP_FAILED_CASE,
                        tasks)
                        .observe(this, failureCaseResponseObserver);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: called");
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 2 && data != null) {
            log(data.getDataString());
        }
        if (requestCode == 2 && data != null) {
            log(data.getDataString());
        }
    }


}