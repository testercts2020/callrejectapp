package com.prudent.callefy.ui.calender;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.button.MaterialButton;
import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.database.CommonTransactionsDb;
import com.prudent.callefy.database.calender_event.CalenderEvent;
import com.prudent.callefy.model.CalenderModel;
import com.prudent.callefy.utils.CommonUtils;
import com.prudent.callefy.utils.ReedCalender;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CalenderTwoActivity extends BaseActivity implements View.OnClickListener, CalenderCallback,
        CommonTransactionsDb.CommonTransCalenderEventDbCallBack {

    @BindView(R.id.rvList)
    RecyclerView rvList;
    @BindView(R.id.clNoData)
    ConstraintLayout clNoData;
    @BindView(R.id.btViewDashboard)
    MaterialButton btViewDashboard;

    ArrayList<CalenderModel> calenderList = new ArrayList<>();
    ArrayList<CalenderEvent> calenderEventList = new ArrayList<>();
    private CalenderTwoAdapter adapter;

    public CommonTransactionsDb mTransactionsDb;

    public static Intent startActivity(Context context) {
        return new Intent(context, CalenderTwoActivity.class);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_calender_two);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Calender");

        initView();
    }

    @Override
    public int setLayout() {
        return R.layout.activity_calender_two;
    }

    @Override
    public void initView() {

        if (mTransactionsDb == null) {
            mTransactionsDb = new CommonTransactionsDb(getApplicationContext(), this);
        }

        getCalenderEventList();

        // Removes blinks
        ((SimpleItemAnimator) rvList.getItemAnimator()).setSupportsChangeAnimations(false);

        rvList.setHasFixedSize(true);
        rvList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

//        adapter = new CalenderTwoAdapter(calenderEventList,
//                this, this);
//
//        rvList.setAdapter(adapter);
//
//        if (calenderList == null || calenderList.size() < 1){
//            clNoData.setVisibility(View.VISIBLE);
//        }

        btViewDashboard.setOnClickListener(this::onClick);
    }

    @Override
    public boolean setToolbar() {
        return true;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btViewDashboard:
                finish();
                break;

        }
    }

    @Override
    public void active() {


    }

    @Override
    public void inActive() {

    }

    @Override
    public void onCalenderDndActive(CalenderEvent event) {
        event.setIs_dnt_active(true);
        mTransactionsDb.updateEventItem(event);
    }

    @Override
    public void onCalenderDndInActive(CalenderEvent event) {
        event.setIs_dnt_active(false);
        mTransactionsDb.updateEventItem(event);
    }


    private void getCalenderEventList() {
        calenderList = ReedCalender.readWebCalendarEventDateRangeToday(
                getApplicationContext());

//        if (calenderList != null && calenderList.size() >0){
//            mTransactionsDb.getAllCalenderEventList();
//        }

        if (calenderList != null && calenderList.size() >0){
            for (int i = 0 ; i< calenderList.size() ; i++){
                CalenderEvent event = new CalenderEvent();

                event.setEvent_id(calenderList.get(i).getEventId());
                event.setEvent_name(calenderList.get(i).getEvent());
                event.setEvent_desc(calenderList.get(i).getDescription());
                event.setStart_date(calenderList.get(i).getStartDate());
                event.setEnd_date(calenderList.get(i).getEndDate());
//                event.setStart_date_before_5min();
                event.setIs_dnt_active(false);
                event.setIs_current_event(false);
                event.setLast_event_update_date(CommonUtils.getTodayDateWithTime());
                if (i == calenderList.size()-1){
                    mTransactionsDb.addOrUpdateEventItem(event, true); // need response for last item
                }else
                mTransactionsDb.addOrUpdateEventItem(event, false);
//                event.setEvent_desc(calenderList.get(i).getEvent());
            }// evt3 148  evt5 149

//            mTransactionsDb.addOrUpdateEventItem();
        }else

        if (calenderList == null || calenderList.size() < 1){
            clNoData.setVisibility(View.VISIBLE);
        }

    }


    @Override
    public void getCalenderEventDbList(List<CalenderEvent> item) {
        if (item == null ||  item.size() <1){

        }

        adapter = new CalenderTwoAdapter(item,
                this, this);

        rvList.setAdapter(adapter);

        if (item == null || item.size() < 1){
            clNoData.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public void onIsAddOrUpdateDbEvent(boolean isUpdate) {
        if (isUpdate){
            mTransactionsDb.getAllCalenderEventList();
        }
    }

    @Override
    public void onUpdateDbEvent(boolean isUpdate) {
        if (isUpdate)
            showToast("Updated");
        else
            showToast("Error updated");
    }
}