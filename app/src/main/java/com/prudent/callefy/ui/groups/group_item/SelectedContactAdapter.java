package com.prudent.callefy.ui.groups.group_item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.prudent.callefy.R;
import com.prudent.callefy.database.groups.ContactGroup;
import com.prudent.callefy.database.groups.group_item.ContGroupItem;
import com.prudent.callefy.dialog_fragment.dialog_contacts.ContactCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SelectedContactAdapter extends RecyclerView.Adapter<SelectedContactAdapter.ViewHolder> {

    private List<ContGroupItem> mContactList = new ArrayList<>();
    Context mContext;
    SelectContactCallback mContactCallback;

    public SelectedContactAdapter(List<ContGroupItem> mContactList, Context mContext, SelectContactCallback mContactCallback) {
        this.mContactList = mContactList;
        this.mContext = mContext;
        this.mContactCallback = mContactCallback;
    }

    public void addNewContent(ContGroupItem contactGroup){
        int insertIndex = mContactList.size();
        mContactList.add(insertIndex, contactGroup);
        notifyItemInserted(insertIndex+1);
        notifyDataSetChanged();
        mContactCallback.onMoveToLastPosition(insertIndex);

    }

    public void removeItem(int position){

        mContactList.remove(position);
        notifyItemInserted(position);
        notifyDataSetChanged();

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_contacts, parent, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ContGroupItem item = mContactList.get(position);
        holder.tvContactName.setText(item.getContact_name());
        holder.tvContactNumber.setText(item.getContact_no());
        holder.btAddContact.setText("Remove");

    }

    @Override
    public int getItemCount() {
        return mContactList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tvContactName)
        TextView tvContactName;
        @BindView(R.id.tvContactNumber)
        TextView tvContactNumber;
        @BindView(R.id.btAddContact)
        MaterialButton btAddContact;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            btAddContact.setOnClickListener(this::onClick);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btAddContact:

                    mContactCallback.onRemoveContacts(getAdapterPosition(), mContactList.get(getAdapterPosition()));
                    break;
            }

        }
    }
}
