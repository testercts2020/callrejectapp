package com.prudent.callefy.ui.notification;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.prudent.callefy.api.ApiClient;
import com.prudent.callefy.base.BaseViewModel;
import com.prudent.callefy.model.CommonDataResponse;
import com.prudent.callefy.model.NotificationsResponse;
import com.prudent.callefy.model.ProfileResponse;
import com.prudent.callefy.utils.Constants;

import java.util.HashMap;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class NotificationViewModel extends BaseViewModel {
    private MutableLiveData<NotificationsResponse> notificationsResponseMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<NotificationsResponse> notificationsLastTimeResponseMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<NotificationsResponse> getNotification(String userId, String tag) {

        HashMap<String, String> body = new HashMap<>();
        body.put("user_id", userId);
        body.put("tag", tag);

        ApiClient.getApiInterface().getNotifications(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<NotificationsResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        setIsLoading(true);
                    }

                    @Override
                    public void onSuccess(NotificationsResponse response) {
                        setIsLoading(false);
                        notificationsResponseMutableLiveData.postValue(response);

                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);
                        Log.d("TAG", "onError: " + e.getMessage());
                    }
                });

        return notificationsResponseMutableLiveData;

    }


    public MutableLiveData<NotificationsResponse> getNotificationLastTime(String userId, String tag, String lastDate) {

        HashMap<String, String> body = new HashMap<>();
        body.put("user_id", userId);
        body.put("tag", tag);
        body.put("call_starting_time", lastDate);

        ApiClient.getApiInterface().getNotifications(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<NotificationsResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        setIsLoading(true);
                    }

                    @Override
                    public void onSuccess(NotificationsResponse response) {
                        setIsLoading(false);
                        notificationsLastTimeResponseMutableLiveData.postValue(response);

                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);
                        Log.d("TAG", "onError: " + e.getMessage());
                    }
                });

        return notificationsLastTimeResponseMutableLiveData;

    }

}
