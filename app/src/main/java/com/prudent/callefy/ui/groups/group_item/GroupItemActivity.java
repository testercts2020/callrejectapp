package com.prudent.callefy.ui.groups.group_item;

import androidx.annotation.NonNull;
import androidx.loader.content.CursorLoader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.button.MaterialButton;
import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.database.CommonTransactionsDb;
import com.prudent.callefy.database.calender_event.CalenderEvent;
import com.prudent.callefy.database.groups.ContactGroup;
import com.prudent.callefy.database.groups.group_item.ContGroupItem;
import com.prudent.callefy.database.missed_calls.MissedCalls;
import com.prudent.callefy.dialog_fragment.dialog_contacts.ContactModel;
import com.prudent.callefy.dialog_fragment.dialog_contacts.ContactsDialogFragment;
import com.prudent.callefy.dialog_fragment.dialog_contacts.OnContactsDialogClickListener;
import com.prudent.callefy.ui.sms_content.SmsContentActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupItemActivity extends BaseActivity implements View.OnClickListener,
        OnContactsDialogClickListener, CommonTransactionsDb.CommonTransGroupItemDbCallBack,
        CommonTransactionsDb.CommonTransCalenderEventDbCallBack,SelectContactCallback, CommonTransactionsDb.CommonTransMissedCallsDbCallsBack {

    @BindView(R.id.rvList)
    RecyclerView rvList;
    @BindView(R.id.btAddContact)
    MaterialButton btAddContacts;

    private CommonTransactionsDb mTransactionsDb;

    ContactsDialogFragment contactsDialog;


    private SelectedContactAdapter adapter;

    int group_id;
    String group_name;
    String group_sms_content;
    int group_call_status_id;
    String group_call_status_disc;


    ArrayList<ContactModel> contactModelsList = new ArrayList<>();


    public static Intent startActivity(Context context, int groupId, String groupName) {
        Intent intent =  new Intent(context, GroupItemActivity.class);
        intent.putExtra("GROUP_ID" , groupId);
        intent.putExtra("GROUP_NAME" , groupName);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        group_id = getIntent().getExtras().getInt("GROUP_ID");
//        group_name = getIntent().getExtras().getString("GROUP_NAME");
//        setTitle(group_name);
        initView();

    }

    @Override
    public int setLayout() {
        return R.layout.activity_group_item;
    }

    @Override
    public void initView() {
        rvList.setHasFixedSize(true);
        rvList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        if (mTransactionsDb == null) {
            mTransactionsDb = new CommonTransactionsDb(getApplicationContext(), this, this, this);
        }

//        mTransactionsDb.getContactGroupsItemList(group_id);
        mTransactionsDb.getGroupsDetailsForContacts(group_id);

        btAddContacts.setOnClickListener(this);



    }

    @Override
    public boolean setToolbar() {
        return true;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btAddContact:


//                showProgress();

                if (contactModelsList == null || contactModelsList.size() <1){
                    showProgress();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getContactList();

                        }
                    },1000);
                }else {
//                    String message = "Contacts list";
//                    String txtCancel = getString(R.string.cancel);
//                    String txtAllow = getString(R.string.save);
//
//                    contactsDialog =  ContactsDialogFragment.newInstance(message, txtCancel, txtAllow, contactModelsList);
////                    newFragment.setTargetFragment(fragment, 1004);
//
//                    contactsDialog.show(getSupportFragmentManager(), "tag");

                    openContactDialogBox();
                }
                


//                if (etGroupName.getText().toString().trim().equals("")) {
//                    showToast(getString(R.string.group_name_field_is_rqd));
//                }else {
//                    mTransactionsDb.saveContactGroup(new ContactGroup(etGroupName.getText().toString().trim()));
//
////                    adapter.addNewSmsContent(new SmsContent(etMessage.getText().toString().trim()));
////                    etMessage.setText("");
//                }

                break;

        }
    }

    @Override
    public void onAddContacts(ContactModel item) {
//        new ContGroupItem(id+"", "", item.getName(), item.getNumber(),"",
//                "", "", "");
        mTransactionsDb.saveContactGroupItem(new ContGroupItem(group_id, group_name, item.getName(), item.getNumber(),group_sms_content,
                0, group_call_status_disc, group_call_status_id, 2));
//        adapter.addNewContent(new ContGroupItem(group_id, "", item.getName(), item.getNumber(),"",
//                0, "", 0, 2));
    }

    @Override
    public void onShowProgress() {
        showProgress();
    }

    @Override
    public void onDismissProgress() {
        dismissProgress();
    }

    @Override
    public void getGroupItemDbList(List<ContGroupItem> item) {
        if (item ==null || item.size() <1){
            if (contactModelsList == null || contactModelsList.size() <1){
                showProgress();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getContactList();

                    }
                },1000);
            }
        }

        adapter = new SelectedContactAdapter(item,
                getApplicationContext(), this);

        rvList.setAdapter(adapter);
    }

    @Override
    public void getLastAddedGroupItemDbList(List<ContGroupItem> item) {
        if (item.size() >= 1)
        adapter.addNewContent(item.get(0));

        ///bbbbbbbbbb
        mTransactionsDb.updateGroupPermanent(group_id);
    }

    @Override
    public void onGetGroupDbItem(List<ContactGroup> item) {

        if (item != null){
            ContactGroup contactGroup = item.get(0);
              group_name = contactGroup.getGroup_name();
              group_sms_content = contactGroup.getGroup_sms_content();
              group_call_status_id= contactGroup.getCall_status_id();
              group_call_status_disc= contactGroup.getCall_status();
              setTitle(group_name);
            mTransactionsDb.getContactGroupsItemList(group_id);

        }
    }

    @Override
    public void onSaveGroupItemDbList(boolean isSaved) {

        if (isSaved){
            showToast("Saved");
            contactsDialog.dismiss();
//            adapter.addNewContent(new ContGroupItem(group_id, "", item.getName(), item.getNumber(),"",
//                    0, "", 0, 2));
            mTransactionsDb.getLatAddedContactGroupsItemList();
        }else {
            showToast("Already saved");
        }
    }

    @Override
    public void onDeleteGroupItemDbList(boolean isSaved) {
        if (isSaved){
            showToast("Delete successfully.");
////            adapter.addNewContent(new ContGroupItem(group_id, "", item.getName(), item.getNumber(),"",
////                    0, "", 0, 2));
//            mTransactionsDb.getLatAddedContactGroupsItemList();
        }else {
            showToast("Delete error!");
        }
    }

    @Override
    public void onCheckDoNotDistActiveDb(boolean isActive) {

    }

    @Override
    public void onRemoveContacts(int position, ContGroupItem item) {
        mTransactionsDb.deleteContactGroupItem(item);
        adapter.removeItem(position);
    }

    @Override
    public void onMoveToLastPosition(int size) {
        rvList.scrollToPosition(size);
    }


    private void getContactList() {
        String tempPhoneNo="";
        String[] PROJECTION = new String[] {
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER };
//        ArrayList<ContactBean> listContacts = new ArrayList<>();
        CursorLoader cursorLoader = new CursorLoader(getApplicationContext(),
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PROJECTION,
                null, null, "UPPER(" + ContactsContract.Contacts.DISPLAY_NAME
                + ")ASC");

        Cursor c = cursorLoader.loadInBackground();

        if (c.moveToFirst()) {

            int Number = c
                    .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int Name = c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);

            do {

                try {
                    String phoneNo = c.getString(Number);
                    String name = c.getString(Name);
                    phoneNo =  phoneNo.replace(" ", "");
                    if (phoneNo.length()>10)
                        phoneNo= phoneNo.substring(phoneNo.length()-10);

                    if (!phoneNo.equals(tempPhoneNo)){
                        contactModelsList.add(new ContactModel(name, phoneNo));
                        tempPhoneNo = phoneNo;
                    }

//                    contactModelsList.add(new ContactModel(phName, phoneNo));


                } catch (Exception e) {

                }

            } while (c.moveToNext());

            c.close();
        }

        openContactDialogBox();

    }
    private void getContactList2() {

        ContentResolver cr =  getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

        if ((cur != null ? cur.getCount() : 0) > 0) {
            String tempPhoneNo="";
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));

                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
                        phoneNo =  phoneNo.replace(" ", "");
                        if (phoneNo.length()>10)
                            phoneNo= phoneNo.substring(phoneNo.length()-10);
                        Log.i("TAG", "Name: " + name);
                        Log.i("TAG", "Phone Number: " + phoneNo);

                        if (!phoneNo.equals(tempPhoneNo)){
                            contactModelsList.add(new ContactModel(name, phoneNo));
                            tempPhoneNo = phoneNo;
                        }

                    }
                    pCur.close();
                }
            }
        }
        if(cur!=null){
            cur.close();
        }

        dismissProgress();

        openContactDialogBox();

    }

    private void openContactDialogBox(){
        String message = "Contacts list";
        String txtCancel = getString(R.string.cancel);
        String txtAllow = getString(R.string.save);

        contactsDialog =  ContactsDialogFragment.newInstance(message, txtCancel, txtAllow, contactModelsList);
//                    newFragment.setTargetFragment(fragment, 1004);

        contactsDialog.show(getSupportFragmentManager(), "tag");
    }

    @Override
    public void getCalenderEventDbList(List<CalenderEvent> item) {

    }

    @Override
    public void onIsAddOrUpdateDbEvent(boolean isUpdate) {

    }

    @Override
    public void onUpdateDbEvent(boolean isUpdate) {

    }

    @Override
    public void onGetAllMissedCalls(List<MissedCalls> list) {

    }

    @Override
    public void onGetAllMissedCallsNotUploaded(List<MissedCalls> list) {

    }

    @Override
    public void onInsertMissedCalls(boolean isAdded) {

    }
}