package com.prudent.callefy.ui.sms_content;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Switch;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.database.CommonTransactionsDb;
import com.prudent.callefy.database.groups.ContactGroup;
import com.prudent.callefy.database.groups.group_item.ContGroupItem;
import com.prudent.callefy.database.sms_content.SmsContent;
import com.prudent.callefy.dialog_fragment.dialog_edit_group.EditGroupDialogFragment;
import com.prudent.callefy.dialog_fragment.dialog_edit_group.OnEditGroupDialogClickListener;
import com.prudent.callefy.dialog_fragment.dialog_edit_sms_content.EditSmsDialogFragment;
import com.prudent.callefy.dialog_fragment.dialog_edit_sms_content.OnEditSmsDialogClickListener;
import com.prudent.callefy.dialog_fragment.dialog_expired.ExpireDialogFragment;
import com.prudent.callefy.dialog_fragment.dialog_expired.OnExpireDialogClickListener;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.ui.groups.group_item.GroupItemActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SmsContentActivity extends BaseActivity implements SmsContentCallback, View.OnClickListener,
        CommonTransactionsDb.CommonTransSmsContentDbCallBack, OnEditSmsDialogClickListener,
        CommonTransactionsDb.CommonTransGroupItemDbCallBack, CommonTransactionsDb.CommonTransGroupDbCallBack,
        OnEditGroupDialogClickListener, OnExpireDialogClickListener {

    @BindView(R.id.rvList)
    RecyclerView rvList;
    @BindView(R.id.radioGp)
    RadioGroup radioGp;
    @BindView(R.id.radioAcceptCall)
    AppCompatRadioButton radioAcceptCall;
    @BindView(R.id.radioRjCall)
    AppCompatRadioButton radioRjCall;
    @BindView(R.id.radioRjCallWithSms)
    AppCompatRadioButton radioRjCallWithSms;
    @BindView(R.id.btAddMessage)
    MaterialButton btAddMessage;
    @BindView(R.id.btContactList)
    MaterialButton btContactList;
    @BindView(R.id.btSave)
    MaterialButton btSave;
    @BindView(R.id.rlResponseMsg)
    RelativeLayout rlResponseMsg;

//    @BindView(R.id.swtDoNot)
//    Switch swtDoNot;

    String tempMessage;
    int tempMessageId;
    int tempPosition;

    boolean isCameFromGroupItemPage;
    int group_id;
    String group_name;
    String group_item_message="";


    ArrayList<SmsContent> smsContentsList = new ArrayList<>();
    private SmsContentAdapter adapter;

    EditSmsDialogFragment editSmsDialog;
    EditGroupDialogFragment editGroupDialog;
    ExpireDialogFragment expireDialog;

    private CommonTransactionsDb mTransactionsDb;

    ContactGroup mContactGroup;

    public static Intent startActivity(Context context, boolean isCameFromGroupItemPage) {
        Intent intent = new Intent(context, SmsContentActivity.class);
        intent.putExtra("IS_CAME_FROM_GROUP_ITEM_PAGE", isCameFromGroupItemPage);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        isCameFromGroupItemPage = getIntent().getExtras().getBoolean("IS_CAME_FROM_GROUP_ITEM_PAGE");

        if (isCameFromGroupItemPage){
            setTitle("Sms content");
            btContactList.setVisibility(View.VISIBLE);
        }else {
            setTitle(getString(R.string.default_status));
            btContactList.setVisibility(View.GONE);
        }

        mAnimationManager = new ExpandOrCollapse();


        initView();
    }

    @Override
    public int setLayout() {
        return R.layout.activity_sms_content;
    }

    @Override
    public void initView() {

        rvList.setHasFixedSize(true);
        rvList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        if (mTransactionsDb == null) {
            mTransactionsDb = new CommonTransactionsDb(getApplicationContext(), this, this, this);
        }

        mTransactionsDb.getSmsContent();

        if (isCameFromGroupItemPage){
            group_id = getIntent().getExtras().getInt("GROUP_ID");
            getGroupDetails();


        }else {

//            boolean isDoNotDisturbActive = SharedPrefs.isManualDoNotDisturbActive();
//            swtDoNot.setChecked(isDoNotDisturbActive);

            int call_state = SharedPrefs.getInt(SharedPrefs.Key.CALL_REJECT_STATE_ID, 1);

            if (call_state == 1) {
                radioAcceptCall.setChecked(true);
                rvList.setVisibility(View.GONE);
                rlResponseMsg.setVisibility(View.GONE);
            } else if (call_state == 2) {
                radioRjCall.setChecked(true);
                rvList.setVisibility(View.GONE);
                rlResponseMsg.setVisibility(View.GONE);
            } else if (call_state == 3) {
                radioRjCallWithSms.setChecked(true);
            }
        }

        radioAcceptCall.setOnClickListener(this);
        radioRjCall.setOnClickListener(this);
        radioRjCallWithSms.setOnClickListener(this);
        btAddMessage.setOnClickListener(this);
        btSave.setOnClickListener(this);
        btContactList.setOnClickListener(this);
//        swtDoNot.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//                if (isCameFromGroupItemPage){
//                    mTransactionsDb.updateContactGroupItemDoNotDisturb(group_id, isChecked);
//                    progressShowSec();
//
//                }else {
//                    SharedPrefs.setIsManualDoNotDisturbActive(isChecked);
//                }
//
//            }
//        });
    }

    @Override
    public boolean setToolbar() {
        return true;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.right_menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        } else if (item.getItemId() == R.id.action_edit) {
            editGroupName();

//            startActivity(SmsContentActivity.startActivity(getApplicationContext(),true));

//            adapter.editAdapter();
        }
        return true;
    }

    private void editGroupName(){
        String title = getString(R.string.group_name);
        String txtCancel = getString(R.string.delete);
        String txtAllow = getString(R.string.update);

//                    CommonUtils.showExpireMessageDialog(this, null, "", message, txtCancel, txtAllow);

        editGroupDialog = EditGroupDialogFragment.newInstance(title, txtCancel, txtAllow, group_name, false);
//                    newFragment.setTargetFragment(fragment, 1004);

        editGroupDialog.show(getSupportFragmentManager(), "tag");
    }

    private void saveData() {
        int selectedId = radioGp.getCheckedRadioButtonId();
        if (isCameFromGroupItemPage){
            if (selectedId == R.id.radioAcceptCall) {
                mContactGroup.setCall_status_id(1);
                mContactGroup.setCall_status(getString(R.string.accept_the_call));
                mTransactionsDb.updateContactGroup(mContactGroup);

                mTransactionsDb.updateContactGroupItemCallAction(group_id , 1);

//                mTransactionsDb.saveContactGroupItem(new ContGroupItem(group_id, group_name, item.getName(), item.getNumber(),group_sms_content,
//                        0, group_call_status_disc, group_call_status_id, 2));

                progressShowSec();
            } else if (selectedId == R.id.radioRjCall) {
                mContactGroup.setCall_status_id(2);
                mContactGroup.setCall_status(getString(R.string.reject_the_call));
                 mTransactionsDb.updateContactGroup(mContactGroup);
                mTransactionsDb.updateContactGroupItemCallAction(group_id , 2);

                progressShowSec();
            } else if (selectedId == R.id.radioRjCallWithSms) {
//                SharedPrefs.setString(SharedPrefs.Key.DEFAULT_SMS_CONTENT, group_item_message);
//                mTransactionsDb.updateContactGroupItemCallAction(group_id, 3);
                mContactGroup.setCall_status_id(3);
                mContactGroup.setCall_status(getString(R.string.reject_the_call_with_sms));
//                SharedPrefs.setString(SharedPrefs.Key.CALL_REJECT_STATE_DESC, getString(R.string.reject_the_call_with_sms));
                mContactGroup.setGroup_sms_content(group_item_message);
                mTransactionsDb.updateContactGroup(mContactGroup);
                mTransactionsDb.updateContactGroupItemCallAction(group_id , 3);
                mTransactionsDb.updateContactGroupItemCallAction(group_id , group_item_message);

                progressShowSec();

            }

            mTransactionsDb.getContactGroupsItemList(group_id);

        }else  {
            if (selectedId == R.id.radioAcceptCall) {
                SharedPrefs.setInt(SharedPrefs.Key.CALL_REJECT_STATE_ID, 1);
                SharedPrefs.setString(SharedPrefs.Key.CALL_REJECT_STATE_DESC, getString(R.string.accept_the_call));
                progressShowSec();
            } else if (selectedId == R.id.radioRjCall) {
                SharedPrefs.setInt(SharedPrefs.Key.CALL_REJECT_STATE_ID, 2);
                SharedPrefs.setString(SharedPrefs.Key.CALL_REJECT_STATE_DESC, getString(R.string.reject_the_call));
                progressShowSec();
            }else if (selectedId == R.id.radioRjCallWithSms) {
                 SharedPrefs.setString(SharedPrefs.Key.DEFAULT_SMS_CONTENT, group_item_message);
                SharedPrefs.setInt(SharedPrefs.Key.CALL_REJECT_STATE_ID, 3);
                SharedPrefs.setString(SharedPrefs.Key.CALL_REJECT_STATE_DESC, getString(R.string.reject_the_call_with_sms));
                progressShowSec();

            }
        }

    }

    private void progressShowSec() {
        checkGroupsDonotDistActive();
        showProgress();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showToast("Data saved successfully.");
                dismissProgress();
            }
        }, 1000);
    }

    @Override
    public void getSmsContentList(List<SmsContent> logs) {

        adapter = new SmsContentAdapter(logs,
                this, this, isCameFromGroupItemPage);

        rvList.setAdapter(adapter);
        group_item_message = SharedPrefs.getString(SharedPrefs.Key.DEFAULT_SMS_CONTENT, "");
        if (!isCameFromGroupItemPage){
            adapter.updateGroupMessage(group_item_message);
        }

    }

    private boolean isVisible = false;
    private ExpandOrCollapse mAnimationManager;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.radioAcceptCall:
//                adapter.radioButtonClick(false);

            case R.id.radioRjCall:
//                adapter.radioButtonClick(false);
                mAnimationManager.expand(rvList, 500, 0);
                rlResponseMsg.setVisibility(View.GONE);
                isVisible = true;
//                rvList.setVisibility(View.GONE);

                break;
            case R.id.radioRjCallWithSms:
                adapter.radioButtonClick(true);
                rlResponseMsg.setVisibility(View.VISIBLE);
                rvList.setVisibility(View.VISIBLE);
                if (isVisible)
                mAnimationManager.expand(rvList, 1000, -200);
                isVisible = false;
//                mAnimationManager.expand(rvList, 1000, 200);
//                isVisible = true;
//                mAnimationManager.expand(rvList, 1000, -200);
//                isVisible = false;
                break;
            case R.id.btAddMessage:

                String title = getString(R.string.response_message);
                String txtCancel = getString(R.string.cancel);
                String txtAllow = getString(R.string.add);

//                    CommonUtils.showExpireMessageDialog(this, null, "", message, txtCancel, txtAllow);

                editSmsDialog = EditSmsDialogFragment.newInstance(title, txtCancel, txtAllow, "", true);
//                    newFragment.setTargetFragment(fragment, 1004);

                editSmsDialog.show(getSupportFragmentManager(), "tag");

                break;
            case R.id.btContactList:
                startActivity(GroupItemActivity.startActivity(getApplicationContext(), group_id, group_name));
                break;
            case R.id.btSave:
                saveData();
                break;
        }
    }

    @Override
    public void setAsDefaultSms() {

    }

    @Override
    public void editSmsContent(String message, int messageId, int position) {
        tempMessage = message;
        tempMessageId = messageId;
        tempPosition = position;

        String title = getString(R.string.response_message);
        String txtCancel = getString(R.string.delete);
        String txtAllow = getString(R.string.update);

//                    CommonUtils.showExpireMessageDialog(this, null, "", message, txtCancel, txtAllow);

        editSmsDialog = EditSmsDialogFragment.newInstance(title, txtCancel, txtAllow, message, false);
//                    newFragment.setTargetFragment(fragment, 1004);

        editSmsDialog.show(getSupportFragmentManager(), "tag");
    }

    @Override
    public void setGroupItemSmsContent(String message) {
        if (isCameFromGroupItemPage)
        mTransactionsDb.updateContactGroupItemCallAction(group_id, message);

        group_item_message = message;
    }

    @Override
    public void addSmsContent(String message) {

                    mTransactionsDb.saveSmsContent(new SmsContent(message));

                    adapter.addNewSmsContent(new SmsContent(message));

    }

    @Override
    public void updateSmsContent(String message) {
        if (tempMessage.equals(SharedPrefs.getString(SharedPrefs.Key.DEFAULT_SMS_CONTENT, "")))
            SharedPrefs.setString(SharedPrefs.Key.DEFAULT_SMS_CONTENT, message);

        mTransactionsDb.updateSmsContent(tempMessageId, message , tempMessage);
        adapter.updateItem(tempPosition, message);
        editSmsDialog.dismiss();

    }

    @Override
    public void deleteSmsContent() {

        mTransactionsDb.deleteSmsContent(tempMessageId, tempMessage);
        adapter.removeItem(tempPosition);
        editSmsDialog.dismiss();

    }

    public void getGroupDetails(){

        mTransactionsDb.getContactGroupById(group_id);

    }

    public void checkGroupsDonotDistActive(){
        mTransactionsDb.checkContactGroupItemDoNotDisturbActive();
    }

    @Override
    public void getGroupItemDbList(List<ContGroupItem> item) {
        if (item == null || item.size()<1){
            startActivity(GroupItemActivity.startActivity(getApplicationContext(), group_id, group_name));
        }
    }

    @Override
    public void getLastAddedGroupItemDbList(List<ContGroupItem> item) {

    }

    @Override
    public void onSaveGroupItemDbList(boolean isSaved) {

        if (isSaved)
        log("updated*******");
        else
            log("NOT updated*******");
    }

    @Override
    public void onDeleteGroupItemDbList(boolean isSaved) {

    }

    @Override
    public void onCheckDoNotDistActiveDb(boolean isActive) {
        SharedPrefs.setIsGroupsDoNotDisturbActive(isActive);
    }

    @Override
    public void getGroupDbList(List<ContactGroup> item) {

    }

    @Override
    public void onGetGroupDbItem(List<ContactGroup> item) {
//        ContactGroup contactGroup ;
        if (item != null && item.size()>0){
            mContactGroup = item.get(0);
            setTitle(mContactGroup.getGroup_name());

            if (mContactGroup.getCall_status_id() != 0){
                int call_state = mContactGroup.getCall_status_id();

                if (call_state == 1) {
                    radioAcceptCall.setChecked(true);
                    rvList.setVisibility(View.GONE);
                    rlResponseMsg.setVisibility(View.GONE);

                } else if (call_state == 2) {
                    radioRjCall.setChecked(true);
                    rvList.setVisibility(View.GONE);
                    rlResponseMsg.setVisibility(View.GONE);

                } else if (call_state == 3) {
                    radioRjCallWithSms.setChecked(true);
                }
            }

            group_item_message = mContactGroup.getGroup_sms_content();
            group_name = mContactGroup.getGroup_name();
            adapter.updateGroupMessage(group_item_message);

        }


    }

    @Override
    public void onGetGroupDbLastItem(List<ContactGroup> item) {

    }

    @Override
    public void onSavedGroupDb(boolean isSaved) {

    }

    @Override
    public void onUpdateGroupDb(boolean isUpdated) {

    }

    @Override
    public void onAddGroup(String name) {

    }

    @Override
    public void onUpdateGroup(String name) {
        mContactGroup.setGroup_name(name);
        mTransactionsDb.updateContactGroup( mContactGroup);
        editGroupDialog.dismiss();
        setTitle(name);
    }

    @Override
    public void onDeleteGroup() {
        String message = getString(R.string.are_you_sure_want_to_delete);
        String txtCancel = getString(R.string.cancel);
        String txtAllow = getString(R.string.delete);

        expireDialog =  ExpireDialogFragment.newInstance(message, txtCancel, txtAllow);
        expireDialog.show(getSupportFragmentManager(), "tag");
    }

    @Override
    public void onDelete() {
//        adapter.removeItem(tempPosition);
        mTransactionsDb.deleteContactGroup(mContactGroup);
        mTransactionsDb.deleteContactGroupContentList(mContactGroup.getId());
        expireDialog.dismiss();

        finish();
    }

    @Override
    public void onCancel() {

    }


    public class ExpandOrCollapse {

        public   void expand(final View v, int duration, int targetHeight) {
            int prevHeight  = v.getHeight();
            v.setVisibility(View.VISIBLE);
            ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    v.getLayoutParams().height = (int) animation.getAnimatedValue();
                    v.requestLayout();
                }
            });
            valueAnimator.setInterpolator(new DecelerateInterpolator());
            valueAnimator.setDuration(duration);
            valueAnimator.start();
        }

        public  void collapse(final View v, int duration, int targetHeight) {
            int prevHeight  = v.getHeight();
            ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
            valueAnimator.setInterpolator(new DecelerateInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    v.getLayoutParams().height = (int) animation.getAnimatedValue();
                    v.requestLayout();
                }
            });
            valueAnimator.setInterpolator(new DecelerateInterpolator());
            valueAnimator.setDuration(duration);
            valueAnimator.start();
        }
    }

}