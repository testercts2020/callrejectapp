package com.prudent.callefy.ui.login;

import androidx.appcompat.widget.AppCompatRadioButton;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.dialog_fragment.dialog_sms_content.OnSmsContentDialogClickListener;
import com.prudent.callefy.dialog_fragment.dialog_sms_content.SmsContentDialogFragment;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.ui.home.DashboardActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CallRejectOptionActivity extends BaseActivity implements View.OnClickListener,
        OnSmsContentDialogClickListener {

    @BindView(R.id.radioGp)
    RadioGroup radioGroup;
    @BindView(R.id.btSubmit)
    Button btSubmit;
    @BindView(R.id.tvDefaultSms)
    TextView tvDefaultSms;
    @BindView(R.id.radioRjCall)
    AppCompatRadioButton radioRjCall;

    SmsContentDialogFragment smsContentDialog;

    public static Intent startActivity(Context context) {
        return new Intent(context, CallRejectOptionActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        initView();

    }

    @Override
    public int setLayout() {
        return R.layout.activity_call_reject_option;
    }

    @Override
    public void initView() {
        btSubmit.setOnClickListener(this::onClick);
        radioRjCall.setOnClickListener(this::onClick);

    }

    @Override
    public boolean setToolbar() {
        return false;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.radioRjCall:
                tvDefaultSms.setText("");
                break;
            case R.id.btSubmit:

                int selectedId = radioGroup.getCheckedRadioButtonId();
                if (selectedId == -1){
                    showToast(getString(R.string.please_choose_a_item));
                }else {
                    if (selectedId == R.id.radioAcceptCall){
                        SharedPrefs.setInt(SharedPrefs.Key.CALL_REJECT_STATE_ID, 1);
                        SharedPrefs.setString(SharedPrefs.Key.CALL_REJECT_STATE_DESC, getString(R.string.accept_the_call));
                        gotoDashboardActivity();
                    }else if (selectedId == R.id.radioRjCall){
                        SharedPrefs.setInt(SharedPrefs.Key.CALL_REJECT_STATE_ID, 2);
                        SharedPrefs.setString(SharedPrefs.Key.CALL_REJECT_STATE_DESC, getString(R.string.reject_the_call));
                        gotoDashboardActivity();
                    }else {
//                        smsContentDialog =  SmsContentDialogFragment.newInstance(message, txtCancel, txtAllow);
                        SharedPrefs.setInt(SharedPrefs.Key.CALL_REJECT_STATE_ID, 3);
                        SharedPrefs.setString(SharedPrefs.Key.CALL_REJECT_STATE_DESC, getString(R.string.reject_the_call_with_sms));
                        startActivity(LoginSmsContentActivity.startActivity(getApplicationContext()));
                        finish();
                    }
                }
                break;
        }
    }

    private void gotoDashboardActivity(){
        showToast(getString(R.string.your_are_successfully_logged_in));
        startActivity(DashboardActivity.startActivity(getApplicationContext()));
        finish();
    }

    @Override
    public void onDefaultMessage(String message) {
        tvDefaultSms.setText("Your default SMS content: "+message);
    }
}