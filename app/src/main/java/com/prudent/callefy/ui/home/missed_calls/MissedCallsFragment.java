package com.prudent.callefy.ui.home.missed_calls;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.CallLog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.prudent.callefy.R;
import com.prudent.callefy.database.CommonTransactionsDb;
import com.prudent.callefy.database.missed_calls.MissedCalls;
import com.prudent.callefy.model.CallLogsModel;
import com.prudent.callefy.ui.notification.CallLogsActivity;
import com.prudent.callefy.ui.notification.NotificationAdapter;
import com.prudent.callefy.utils.CommonUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MissedCallsFragment extends Fragment implements CommonTransactionsDb.CommonTransMissedCallsDbCallsBack {

    @BindView(R.id.rvList)
    RecyclerView rvList;
    @BindView(R.id.clNoNotification)
    ConstraintLayout clNoNotification;

    NotificationAdapter notificationAdapter;

    List<CallLogsModel> callLogList;

    private CommonTransactionsDb mTransactionsDb;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_missed_calls, container, false);
        ButterKnife.bind(this, view);
        initViews(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        mTransactionsDb.getAllMissedCalls();
    }

    private void initViews(View view) {

        ((SimpleItemAnimator) rvList.getItemAnimator()).setSupportsChangeAnimations(false);

        rvList.setHasFixedSize(true);
        rvList.setLayoutManager(new LinearLayoutManager(getContext()));

//        getCallDetails();

        if (mTransactionsDb == null) {
            mTransactionsDb = new CommonTransactionsDb(getContext(), this);
        }


        uploadRejectReportToServer();

    }

    @Override
    public void onStart() {
        super.onStart();
    }

//    private String getCallDetails() {
//        StringBuffer sb = new StringBuffer();
//        Cursor managedCursor = getContext().getContentResolver().query(CallLog.Calls.CONTENT_URI, null,
//                null, null, CallLog.Calls.DATE + " DESC");
//        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
//        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
//        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
//        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
//        int name = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
//
//        sb.append("Call logs");
//        if (callLogList == null)
//            callLogList = new ArrayList<>();
//
//        String beforeDate = "";
//
//        while (managedCursor.moveToNext()) {
//            String phNumber = managedCursor.getString(number);
//            String callType = managedCursor.getString(type);
//            String callDate = managedCursor.getString(date);
//            Date callDayTime = new Date(Long.valueOf(callDate));
//            String callDuration = managedCursor.getString(duration);
//            String myDateTimeStr = CommonUtils.getDateTimeString(callDayTime);
//            String myDateStr = CommonUtils.getDateString(callDayTime);
//            String nameStr = managedCursor.getString(name);
////                    new SimpleDateFormat("dd-MM-yyyy").format(itemDate);
//            String dir = null;
//            int dircode = Integer.parseInt(callType);
//            switch (dircode) {
//                case CallLog.Calls.OUTGOING_TYPE:
//                    dir = "Outgoing";
//                    break;
//
//                case CallLog.Calls.INCOMING_TYPE:
//                    dir = "Incoming";
//                    break;
//
//                case CallLog.Calls.MISSED_TYPE:
//                    dir = "Missed";
//                    break;
//
//                case CallLog.Calls.REJECTED_TYPE:
//                    dir = "Reject";
//                    break;
//            }
//
//
//            if (!myDateStr.equals(beforeDate)) {
//                beforeDate = myDateStr;
////                CallLogsModel callLogs = new CallLogsModel(phNumber, callType, myDateTimeStr);
////                callLogList.add(callLogs);
//
//                CallLogsModel headerCallLogsModel = new CallLogsModel();
//
//                headerCallLogsModel.setHeader(true);
//                if (beforeDate.equals(CommonUtils.getTodayDate())) {
//                    headerCallLogsModel.setDateTime("Today");
//                    callLogList.add(headerCallLogsModel);
//                }
//                else
//                    headerCallLogsModel.setDateTime(beforeDate);
//
////                callLogList.add(headerCallLogsModel);
//
//            }
//
//            if (myDateStr.equals(CommonUtils.getTodayDate())) {
//                if (dir != null && (dir.equals("Missed") || dir.equals("Reject") )){
//                    CallLogsModel callLogs = new CallLogsModel(phNumber, dir, myDateTimeStr, nameStr);
//                    callLogList.add(callLogs);
//                }
//
//            }
//
//
//
//
//            sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- "
//                    + dir + " \nCall Date:--- " + callDayTime
//                    + " \nCall duration in sec :--- " + callDuration);
//            sb.append("\n----------------------------------");
//
//
//        }
//        managedCursor.close();
//        updateListView();
//        return sb.toString();
//
//    }

    private void updateListView() {

        if (callLogList == null||callLogList.size() <1){
            clNoNotification.setVisibility(View.VISIBLE);
            return;
        }

        clNoNotification.setVisibility(View.GONE);


        notificationAdapter = new NotificationAdapter(callLogList,
                getContext());

        rvList.setAdapter(notificationAdapter);

    }

    @Override
    public void onGetAllMissedCalls(List<MissedCalls> list) {

        if (list !=null && list.size() > 0){
            if (callLogList == null)
                callLogList = new ArrayList<>();

            callLogList.clear();
            for (int i =0 ; i < list.size() ; i++){
                String phNumber = list.get(i).getPh_no();
                String dir = "Missed";
                String myDateTimeStr = list.get(i).getDate_time();
                String nameStr = list.get(i).getName();

                CallLogsModel callLogs = new CallLogsModel(phNumber, dir, myDateTimeStr, nameStr);
                if (i == 0)
                    callLogs.setExpanded(true);
                callLogList.add(callLogs);
            }
            updateListView();
        }
    }

    @Override
    public void onGetAllMissedCallsNotUploaded(List<MissedCalls> list) {

        if (list !=null && list.size() > 0){



        }
    }

    @Override
    public void onInsertMissedCalls(boolean isAdded) {

    }

    private void uploadRejectReportToServer(){
        if (CommonUtils.isNetworkConnected(getContext()))
        mTransactionsDb.getAllMissedCallsForSaveToServer();
    }

}
