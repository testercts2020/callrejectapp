package com.prudent.callefy.ui.calender;

import com.prudent.callefy.database.calender_event.CalenderEvent;

public interface CalenderCallback {

    public void active();
    public void inActive();
    void onCalenderDndActive(CalenderEvent event);
    void onCalenderDndInActive(CalenderEvent event);
}
