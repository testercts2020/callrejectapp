package com.prudent.callefy.ui.sms_content;

public interface SmsContentCallback {
    void setAsDefaultSms();
    void editSmsContent(String message,int messageId,  int position);
    void setGroupItemSmsContent(String message);
}
