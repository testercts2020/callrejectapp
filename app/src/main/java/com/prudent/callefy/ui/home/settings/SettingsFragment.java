package com.prudent.callefy.ui.home.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.prudent.callefy.R;
import com.prudent.callefy.ui.calender.CalenderActivity;
import com.prudent.callefy.ui.calender.CalenderTwoActivity;
import com.prudent.callefy.ui.donot_disturbe.DoNotActivity;
import com.prudent.callefy.ui.feedback.FeedbackActivity;
import com.prudent.callefy.ui.groups.GroupsActivity;
import com.prudent.callefy.ui.profile.ProfileActivity;
import com.prudent.callefy.ui.status.StatusActivity;
import com.prudent.callefy.ui.web_view.WebviewActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.clProfile)
    ConstraintLayout clProfile;
    @BindView(R.id.clStatus)
    ConstraintLayout clStatus;
    @BindView(R.id.clGroups)
    ConstraintLayout clGroups;
    @BindView(R.id.clCalender)
    ConstraintLayout clCalender;
    @BindView(R.id.clFeedback)
    ConstraintLayout clFeedback;
    @BindView(R.id.clHelp)
    ConstraintLayout clHelp;
    @BindView(R.id.clDonotDisturb)
    ConstraintLayout clDonotDisturb;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);
        initViews(view);

        return view;
    }

    private void initViews(View view) {

        clProfile.setOnClickListener(this::onClick);
        clStatus.setOnClickListener(this::onClick);
        clGroups.setOnClickListener(this::onClick);
        clCalender.setOnClickListener(this::onClick);
        clFeedback.setOnClickListener(this::onClick);
        clHelp.setOnClickListener(this::onClick);
        clDonotDisturb.setOnClickListener(this::onClick);
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.clProfile:

                startActivity(ProfileActivity.startActivity(getContext()));
                break;
            case R.id.clStatus:

                startActivity(StatusActivity.startActivity(getContext()));
                break;
            case R.id.clGroups:

                startActivity(GroupsActivity.startActivity(getContext()));
                break;
            case R.id.clCalender:

                startActivity(CalenderTwoActivity.startActivity(getContext()));
                break;
            case R.id.clFeedback:

                startActivity(FeedbackActivity.startActivity(getContext()));
                break;
            case R.id.clHelp:

                startActivity(WebviewActivity.startActivity(getContext()));
                break;
            case R.id.clDonotDisturb:

                startActivity(DoNotActivity.startActivity(getContext()));
                break;
        }

    }
}

