package com.prudent.callefy.ui.home;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.prudent.callefy.ui.home.missed_calls.MissedCallsFragment;
import com.prudent.callefy.ui.home.settings.SettingsFragment;

import java.util.ArrayList;

public class ViewpagerAdapter extends FragmentStateAdapter {
    private ArrayList<Fragment> arrayList = new ArrayList<>();

    public ViewpagerAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return new MissedCallsFragment();
            case 1:
                return new SettingsFragment();


        }
        return null;
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
