package com.prudent.callefy.ui.notification;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.CallLog;

import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.model.CallLogsModel;
import com.prudent.callefy.utils.CommonUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CallLogsActivity extends BaseActivity {

    @BindView(R.id.rvList)
    RecyclerView rvList;

    NotificationAdapter notificationAdapter;

    List<CallLogsModel> callLogList;

    public static Intent startActivity(Context context) {
        return new Intent(context, CallLogsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Call log");

        initView();

    }

    @Override
    public void initView() {

        log("Call details");
        getCallDetails();

        ((SimpleItemAnimator) rvList.getItemAnimator()).setSupportsChangeAnimations(false);

        rvList.setHasFixedSize(true);
        rvList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

    }

    private String getCallDetails() {
        StringBuffer sb = new StringBuffer();
        Cursor managedCursor = getContentResolver().query(CallLog.Calls.CONTENT_URI, null,
                null, null, CallLog.Calls.DATE + " DESC");
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        int name = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);

        sb.append("Call logs");
        if (callLogList == null)
            callLogList = new ArrayList<>();

        String beforeDate = "";

        while (managedCursor.moveToNext()) {
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);
            String myDateTimeStr = CommonUtils.getDateTimeString(callDayTime);
            String myDateStr = CommonUtils.getDateString(callDayTime);
            String nameStr = managedCursor.getString(name);
//                    new SimpleDateFormat("dd-MM-yyyy").format(itemDate);
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "Outgoing";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "Incoming";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "Missed";
                    break;

                case CallLog.Calls.REJECTED_TYPE:
                    dir = "Reject";
                    break;
            }


            if (!myDateStr.equals(beforeDate)) {
                beforeDate = myDateStr;
//                CallLogsModel callLogs = new CallLogsModel(phNumber, callType, myDateTimeStr);
//                callLogList.add(callLogs);

                CallLogsModel headerCallLogsModel = new CallLogsModel();

                headerCallLogsModel.setHeader(true);
                if (beforeDate.equals(CommonUtils.getTodayDate())) {
                    headerCallLogsModel.setDateTime("Today");
                    callLogList.add(headerCallLogsModel);
                }
                else
                    headerCallLogsModel.setDateTime(beforeDate);

//                callLogList.add(headerCallLogsModel);

            }

            if (myDateStr.equals(CommonUtils.getTodayDate())) {
                if (dir != null && (dir.equals("Missed") || dir.equals("Reject") )){
                    CallLogsModel callLogs = new CallLogsModel(phNumber, dir, myDateTimeStr, nameStr);
                    callLogList.add(callLogs);
                }

            }




            sb.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- "
                    + dir + " \nCall Date:--- " + callDayTime
                    + " \nCall duration in sec :--- " + callDuration);
            sb.append("\n----------------------------------");


        }
        managedCursor.close();
        updateListView();
        return sb.toString();

    }

    private void updateListView() {

        if (callLogList == null)
            return;

        notificationAdapter = new NotificationAdapter(callLogList,
                CallLogsActivity.this);

        rvList.setAdapter(notificationAdapter);

    }

    @Override
    public int setLayout() {
        return R.layout.activity_call_logs;
    }

    @Override
    public boolean setToolbar() {
        return true;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }
}