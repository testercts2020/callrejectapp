package com.prudent.callefy.ui.status;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.prudent.callefy.R;
import com.prudent.callefy.database.groups.ContactGroup;
import com.prudent.callefy.ui.groups.GroupsAdapter;
import com.prudent.callefy.ui.groups.GroupsCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StatusGroupAdapter extends RecyclerView.Adapter<StatusGroupAdapter.ViewHolder>{

    private List<ContactGroup> mContactGroupList = new ArrayList<>();
    Context mContext;
    GroupsCallback mGroupsCallback;
    boolean isEdit= false;

    public StatusGroupAdapter(List<ContactGroup> mContactGroupList, Context mContext, GroupsCallback mGroupsCallback) {
        this.mContactGroupList = mContactGroupList;
        this.mContext = mContext;
        this.mGroupsCallback = mGroupsCallback;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_status_group, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ContactGroup item = mContactGroupList.get(position);
        holder.tvGroupName.setText(item.getGroup_name());
        holder.tvStatus.setText(item.getCall_status());
        if (item.getCall_status().equals(mContext.getString(R.string.reject_the_call_with_sms)))
            holder.tvStatus.setText(item.getCall_status()+": "+item.getGroup_sms_content());
//        if (isEdit){
//            holder.btDelete.setVisibility(View.VISIBLE);
//        }else {
//            holder.btDelete.setVisibility(View.GONE);
//        }
    }

    @Override
    public int getItemCount() {

            return mContactGroupList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tvGroupName)
        TextView tvGroupName;
        @BindView(R.id.tvStatus)
        TextView tvStatus;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

//            rlGroup.setOnClickListener(this::onClick);
//            btDelete.setOnClickListener(this::onClick);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.rlGroup:
                    mGroupsCallback.onListItemClick(getAdapterPosition(), mContactGroupList.get(getAdapterPosition()));
                    break;
//                case R.id.btDelete:
//                    mGroupsCallback.onDeleteGroup(getAdapterPosition(), mContactGroupList.get(getAdapterPosition()));
//                    break;
            }

        }
    }
}
