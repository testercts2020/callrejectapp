package com.prudent.callefy.ui.settings;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.ui.calender.CalenderActivity;
import com.prudent.callefy.ui.donot_disturbe.DoNotActivity;
import com.prudent.callefy.ui.feedback.FeedbackActivity;
import com.prudent.callefy.ui.groups.GroupsActivity;
import com.prudent.callefy.ui.home.DashboardActivity;
import com.prudent.callefy.ui.profile.ProfileActivity;
import com.prudent.callefy.ui.sms_content.SmsContentActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.clSmsContent)
    ConstraintLayout clSmsContent;
    @BindView(R.id.clGroups)
    ConstraintLayout clGroups;
    @BindView(R.id.clDoNot)
    ConstraintLayout clDoNot;
    @BindView(R.id.clCalender)
    ConstraintLayout clCalender;
    @BindView(R.id.clFeedback)
    ConstraintLayout clFeedback;
    @BindView(R.id.clProfile)
    ConstraintLayout clProfile;

    public static Intent startActivity(Context context) {
         return new Intent(context, SettingsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Settings");
        initView();

    }

    @Override
    public int setLayout() {
        return R.layout.activity_settings;
    }

    @Override
    public void initView() {
        clSmsContent.setOnClickListener(this::onClick);
        clGroups.setOnClickListener(this::onClick);
        clDoNot.setOnClickListener(this::onClick);
        clCalender.setOnClickListener(this::onClick);
        clFeedback.setOnClickListener(this::onClick);
        clProfile.setOnClickListener(this::onClick);
    }

    @Override
    public boolean setToolbar() {
        return true;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.clSmsContent:

                startActivity(SmsContentActivity.startActivity(getApplicationContext(),false));
                break;
            case R.id.clGroups:

                startActivity(GroupsActivity.startActivity(getApplicationContext()));
                break;
            case R.id.clDoNot:

                startActivity(DoNotActivity.startActivity(getApplicationContext()));
                break;
            case R.id.clCalender:

                startActivity(CalenderActivity.startActivity(getApplicationContext()));
                break;
            case R.id.clFeedback:

                startActivity(FeedbackActivity.startActivity(getApplicationContext()));
                break;
            case R.id.clProfile:

                startActivity(ProfileActivity.startActivity(getApplicationContext()));
                break;
        }
    }
}