package com.prudent.callefy.ui.donot_disturbe;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.dialog_fragment.dialog_expired.ExpireDialogFragment;
import com.prudent.callefy.dialog_fragment.dialog_number_picker.NumberPickerDialogFragment;
import com.prudent.callefy.dialog_fragment.dialog_number_picker.OnNumberPickerDialogClickListener;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.utils.ForegroundService;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DoNotActivity extends BaseActivity implements View.OnClickListener,  NumberPicker.OnValueChangeListener ,
        OnNumberPickerDialogClickListener {

    @BindView(R.id.swtDoNot)
    Switch swtDoNot;
    @BindView(R.id.btAddTime)
    Button btAddTime;
    @BindView(R.id.tvDonotDistTime)
    TextView tvDonotDistTime;

    NumberPickerDialogFragment numberPickerDialog;

    public static Intent startActivity(Context context) {
        Intent intent =  new Intent(context, DoNotActivity.class);
//        intent.putExtra("GROUP_ID" , groupId);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.do_not_disturb));
        initView();


    }

    @Override
    public int setLayout() {
        return R.layout.activity_do_not;
    }

    @Override
    public void initView() {

        boolean isDoNotDisturbActive = SharedPrefs.isManualDoNotDisturbActive();
        swtDoNot.setChecked(isDoNotDisturbActive);
        swtDoNot.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                SharedPrefs.setIsManualDoNotDisturbActive(isChecked);
            }
        });

        btAddTime.setOnClickListener(this);

    }

    @Override
    public boolean setToolbar() {
        return true;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btAddTime:


                String message = getString(R.string.please_select_minutes_for_call_reject);
                String txtCancel = getString(R.string.cancel);
                String txtAllow = getString(R.string.select);

                numberPickerDialog =  NumberPickerDialogFragment.newInstance(message, txtCancel, txtAllow);
                numberPickerDialog.show(getSupportFragmentManager(), "tag");


                break;
        }
    }


//    public void show()
//    {
//
//        final Dialog d = new Dialog(DoNotActivity.this);
//        d.setTitle("NumberPicker");
//        d.setContentView(R.layout.dialog_number_picker);
//        Button b1 = (Button) d.findViewById(R.id.button1);
//        Button b2 = (Button) d.findViewById(R.id.button2);
//        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
//        np.setMaxValue(100);
//        np.setMinValue(0);
//        np.setWrapSelectorWheel(false);
//        np.setOnValueChangedListener(this);
//        b1.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v) {
////                tv.setText(String.valueOf(np.getValue()));
//                d.dismiss();
//            }
//        });
//        b2.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v) {
//                d.dismiss();
//            }
//        });
//        d.show();
//
//
//    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        Log.i("value is",""+newVal);
    }

    @Override
    public void onNumberPickedValues(int minutes) {
        numberPickerDialog.dismiss();
        SharedPrefs.setIsManualDoNotDisturbActive(true);
        if (minutes == 0)
            minutes = 1;
        SharedPrefs.setInt(SharedPrefs.Key.DO_NOT_DISTURB_WITH_TIME, minutes);
        Intent intent = new Intent(this,ForegroundService.class);
        intent.setAction("ACTION_DONOT_DISTURB_WITH_TIME");
        ContextCompat.startForegroundService(getApplicationContext(),intent);



//        tvDonotDistTime.setText("Do not disturb active "+minutes+" minute");
//        SharedPrefs.setIsManualDoNotDisturbActive(true);
//        swtDoNot.setChecked(true);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                SharedPrefs.setIsManualDoNotDisturbActive(false);
//                swtDoNot.setChecked(false);
//            }
//        },60000*minutes);
    }
}