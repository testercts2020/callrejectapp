package com.prudent.callefy.ui.groups;

import com.prudent.callefy.database.groups.ContactGroup;

public interface GroupsCallback {

    void onListItemClick(int position, ContactGroup group);

    void onMoveToLastPosition(int size);

    void onDeleteGroup(int position,ContactGroup group);

    void onEditGroup(int position,ContactGroup group);
}
