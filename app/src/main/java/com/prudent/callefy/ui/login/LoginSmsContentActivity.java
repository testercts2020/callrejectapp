package com.prudent.callefy.ui.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.database.CommonTransactionsDb;
import com.prudent.callefy.database.sms_content.SmsContent;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.ui.home.DashboardActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginSmsContentActivity extends BaseActivity implements View.OnClickListener,
        CommonTransactionsDb.CommonTransSmsContentDbCallBack{

    @BindView(R.id.radiogroup)
    RadioGroup radiogroup;
    @BindView(R.id.etCustomMessage)
    TextInputEditText etCustomMessage;
    @BindView(R.id.btSubmit)
    MaterialButton btSubmit;
    @BindView(R.id.btAddMessage)
    MaterialButton btAddMessage;

    public static Intent startActivity(Context context) {
        return new Intent(context, LoginSmsContentActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        initView();
    }

    @Override
    public int setLayout() {
        return R.layout.activity_login_sms_content;
    }

    @Override
    public void initView() {
        btSubmit.setOnClickListener(this);
        btAddMessage.setOnClickListener(this);
        getSmsContent();
    }
    private void getSmsContent(){
//        if (mTransactionsDb == null){
            mTransactionsDb = new CommonTransactionsDb(getApplicationContext(), this);
//        }

        mTransactionsDb.getSmsContent();
    }

    @Override
    public boolean setToolbar() {
        return false;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btSubmit:

                int selectedId = radiogroup.getCheckedRadioButtonId();

                RadioButton radioButtonMsg = (RadioButton) radiogroup.findViewById(selectedId);
                String value = radioButtonMsg.getText().toString();
//                    final String value =
//                            ((RadioButton)getContext().findViewById(radiogroup.getCheckedRadioButtonId()))
//                                    .getText().toString();
                SharedPrefs.setString(SharedPrefs.Key.DEFAULT_SMS_CONTENT,value);

                showToast(getString(R.string.your_are_successfully_logged_in));
                startActivity(DashboardActivity.startActivity(getApplicationContext()));
                finish();

                break;
            case R.id.btAddMessage:
                if (etCustomMessage.getText().toString().trim().equals("")){
                    showToast("Enter custom message");
                }else {
                    AppCompatRadioButton radioButton = new AppCompatRadioButton(this);
                    radioButton.setId(View.generateViewId());
                    radioButton.setText(etCustomMessage.getText().toString().trim());
                    radioButton.setPadding(0,6, 0,0);
                    ColorStateList colorStateList = new ColorStateList(
                            new int[][]{

                                    new int[]{-android.R.attr.state_enabled}, //disabled
                                    new int[]{android.R.attr.state_enabled} //enabled
                            },
                            new int[] {

                                    getResources().getColor(R.color.colorPrimaryDark) //disabled
                                    ,getResources().getColor(R.color.colorPrimary) //enabled

                            }
                    );
                    radioButton.setButtonTintList(colorStateList);
                    radiogroup.addView(radioButton);


//                    scroll.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            scroll.fullScroll(View.FOCUS_DOWN);
//                        }
//                    });

                    mTransactionsDb.saveSmsContent(new SmsContent(etCustomMessage.getText().toString().trim()));
                    etCustomMessage.setText("");
                }

                break;
        }
    }

    @Override
    public void getSmsContentList(List<SmsContent> logs) {
        radiogroup.setOrientation(LinearLayout.VERTICAL);
        if (logs == null || logs.size() < 1){
            return;
        }
        for (int i = 0; i< logs.size(); i++){
            AppCompatRadioButton radioButton = new AppCompatRadioButton(this);
            radioButton.setId(View.generateViewId());
            radioButton.setText(logs.get(i).getSmsContent());
            radioButton.setGravity(Gravity.TOP);
            radioButton.setPadding(0,6, 0,0);
            ColorStateList colorStateList = new ColorStateList(
                    new int[][]{

                            new int[]{-android.R.attr.state_enabled}, //disabled
                            new int[]{android.R.attr.state_enabled} //enabled
                    },
                    new int[] {

                            getResources().getColor(R.color.colorPrimaryDark) //disabled
                            ,getResources().getColor(R.color.colorPrimary) //enabled

                    }
            );
            radioButton.setButtonTintList(colorStateList);
            if ( i==0){
                radioButton.setChecked(true);
            }
            radiogroup.addView(radioButton);

        }
    }
}