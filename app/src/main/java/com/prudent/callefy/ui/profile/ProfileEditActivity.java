package com.prudent.callefy.ui.profile;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.dialog_fragment.image_picker.ImagePickerDialog;
import com.prudent.callefy.model.CommonDataResponse;
import com.prudent.callefy.model.ProfileResponse;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.utils.CommonUtils;
import com.prudent.callefy.utils.Constants;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileEditActivity extends BaseActivity implements View.OnClickListener, ImagePickerDialog.OnInteractionListener {

    @BindView(R.id.etName)
    TextInputEditText etName;
    @BindView(R.id.etEmail)
    TextInputEditText etEmail;
    @BindView(R.id.etMobileNo)
    TextInputEditText etMobileNo;
//    @BindView(R.id.etMobileOperator)
//    TextInputEditText etMobileOperator;
//    @BindView(R.id.etUssdActivationCode)
//    TextInputEditText etUssdActivationCode;
//    @BindView(R.id.etUssdDeActivationCode)
//    TextInputEditText etUssdDeActivationCode;
    @BindView(R.id.btUpdate)
    MaterialButton btUpdate;
    @BindView(R.id.btProfileAdd)
    ImageView btProfileAdd;
    @BindView(R.id.myprofile_img)
    CircleImageView myprofile_img;


    private static final int REQUEST_CAMERA = 11;
    private static final int REQUEST_GALLERY = 12;
    private static final int REQUEST_CAMERA_PERMISSION = 1;

    private String imageFileName;

    private Observer<ProfileResponse> profileResponseObserver;
    private Observer<CommonDataResponse> updatePrfResponseObserver;
    private ProfileViewModel profileViewModel;

    public static Intent startActivity(Context context) {
        return new Intent(context, ProfileEditActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Edit profile");
        initView();

        if (profileViewModel == null) {
            profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
            profileViewModel.getIsLoading().observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean isLoading) {
                    if (isLoading) {
                        showProgress();
                    } else {
                        dismissProgress();
                    }
                }
            });

//            profileResponseObserver = this::onSuccessGetProfileResponse;
            updatePrfResponseObserver = this::onSuccessUpdateProfile;


        }


    }

    @Override
    public int setLayout() {
        return R.layout.activity_profile_edit;
    }

    @Override
    public void initView() {
        String act = SharedPrefs.getUssdActCode();
        String deAct = SharedPrefs.getUssdDeActCode();
        String callForwardingNo = SharedPrefs.getCallForwardingNo();
        String mobileNo = SharedPrefs.getString(SharedPrefs.Key.MOB_NO, "");
        String operator = SharedPrefs.getString(SharedPrefs.Key.OPERATOR, "");

        etName.setText(SharedPrefs.getString(SharedPrefs.Key.USER_NAME, ""));
        etEmail.setText(SharedPrefs.getString(SharedPrefs.Key.USER_EMAIL , ""));
        etMobileNo.setText(mobileNo);
        String user_profile_image = SharedPrefs.getString(SharedPrefs.Key.USER_PROFILE_IMAGE , "");

        if (user_profile_image  != null && !user_profile_image.equals("")) {
//                SharedPrefs.setString(SharedPrefs.Key.USER_EMAIL, response.getUserInfo().getEmailId());
            Glide.with(this)
                    .load( user_profile_image) // image url
                    .error(R.drawable.ic_loading)
//                    .diskCacheStrategy(DiskCacheStrategy.NONE)
//                    .skipMemoryCache(true)
                    .into(myprofile_img);
        }

//        etMobileOperator.setText(operator);
//        etUssdActivationCode.setText(act);
//        etUssdDeActivationCode.setText(deAct);
//
        btUpdate.setOnClickListener(this::onClick);
        btProfileAdd.setOnClickListener(this::onClick);


    }

    @Override
    public boolean setToolbar() {
        return true;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btProfileAdd:
                showImagePickerDialog();

                break;


            case R.id.btUpdate:
                if (isValid()){
//                    String tempAct = etUssdActivationCode.getText().toString();
//                    String tempDeAct = etUssdDeActivationCode.getText().toString();
                    String tempForwardNo = "";
                    String tempMobileNo = etMobileNo.getText().toString();
//                    String tempOperator = etMobileOperator.getText().toString();
//                    SharedPrefs.setString(SharedPrefs.Key.USSD_ACT_CODE, tempAct);
//                    SharedPrefs.setString(SharedPrefs.Key.USSD_DE_ACT_CODE, tempDeAct);
                    SharedPrefs.setString(SharedPrefs.Key.CALL_FORWARDING_NO, tempForwardNo);
//                    profileViewModel.updateEditProfile(SharedPrefs.getUserId(),
//                            Constants.TAG_API_UPDATE_PROFILE,
//                            etName.getText().toString(),
//                            etEmail.getText().toString(),
//                            tempMobileNo,
//                            "",
//                            "",
//                            "",
//                            "",
//                            "",
//                            "",
//                            "").observe(this, updatePrfResponseObserver);



                    profileViewModel.updateEditProfileWithImage(SharedPrefs.getUserId(),
                            Constants.TAG_API_UPDATE_PROFILE,
                            etName.getText().toString(),
                            etEmail.getText().toString(),
                            tempMobileNo,
                            imageFileName
                            ).observe(this, updatePrfResponseObserver);


                }
                break;

        }
    }

    public boolean isValid(){
        if (etName.getText().toString().trim().equals("")){
            showToast("Name field is required.");
            return false;
        }else if (etEmail.getText().toString().trim().equals("")){
            showToast("Email address is required.");

            return false;
        }else if (!isValidEmail(etEmail.getText().toString().trim())){
            showToast("Invalid email address.");
            return false;
        }

        return true;
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public void onSuccessUpdateProfile(CommonDataResponse response){
        if (response.getStatus() == 1){
            showToast("Profile updated");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            },1000);
        }else {
            showToast(response.getMessage());
        }
    }


    private void showImagePickerDialog() {

        ImagePickerDialog imagePickerDialog = ImagePickerDialog.newInstance(this);
        imagePickerDialog.show(getSupportFragmentManager(), "image_picker_dialog");

    }

    @Override
    public void onSelectFromGallery() {
        pickFromGallery();
    }

    @Override
    public void onTakeNew() {
        requestForCameraPermission();
    }

    private void pickFromGallery() {
        Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(galleryIntent,
                "Select Picture"), REQUEST_GALLERY);
    }

    public void requestForCameraPermission() {
        final String permission = Manifest.permission.CAMERA;
        if (ContextCompat.checkSelfPermission(getApplicationContext(), permission)
                != PackageManager.PERMISSION_GRANTED) {
            requestForPermission(permission);
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
//                showPermissionRationaleDialog("App Need to allow camera permission for the proper functioning.", permission);
//            } else {
//                requestForPermission(permission);
//            }
        } else {
            launchCamera();
        }
    }

    private void showPermissionRationaleDialog(final String message, final String permission) {
        new AlertDialog.Builder(getApplicationContext())
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestForPermission(permission);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }

    private void requestForPermission(final String permission) {
        ActivityCompat.requestPermissions(this, new String[]{permission}, REQUEST_CAMERA_PERMISSION);
    }

    private void launchCamera() {
        Intent startCustomCameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(startCustomCameraIntent, REQUEST_CAMERA);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == REQUEST_CAMERA) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                Uri tempUri = CommonUtils.getImageUri(getApplicationContext(), photo);
                File finalFile = new File(CommonUtils.getRealPathFromURI(getApplicationContext(), tempUri));
//                if (imageType == IMAGE_TYPE_CHARITY_PICS) {
//                    imageList.add(finalFile.toString());
//                } else if (imageType == IMAGE_TYPE_SIGNATURE) {
//                    imageSignatureName = finalFile.toString();
//                } else {
//                    imageLogoName = finalFile.toString();
//                }

                imageFileName = finalFile.toString();
                setImage(finalFile.toString());
            } else if (requestCode == REQUEST_GALLERY) {
                String pathFromResult = CommonUtils.getPath(getApplicationContext(), data.getData());
//                if (imageType == IMAGE_TYPE_CHARITY_PICS) {
//                    imageList.add(pathFromResult);
//                } else if (imageType == IMAGE_TYPE_SIGNATURE) {
//                    imageSignatureName = pathFromResult;
//                } else {
//                    imageLogoName = pathFromResult;
//                }
                imageFileName=pathFromResult;
                setImage( pathFromResult);
            }
        }
    }

    private void setImage( String imageBitmap) {

        Glide.with(getApplicationContext()).load(imageBitmap).override(200, 200).into(myprofile_img);

//        if (imageType == IMAGE_TYPE_SIGNATURE)
//            Glide.with(getContext()).load(imageBitmap).override(200, 200).into(ivSignature);
//        else if (imageType == IMAGE_TYPE_LOGO)
//            Glide.with(getContext()).load(imageBitmap).override(200, 200).into(ivLogo);
//        if (imageType == IMAGE_TYPE_CHARITY_PICS){
//            imageAdapter.notifyList();
//            rvList.getLayoutManager().scrollToPosition(imageList.size() -1);
//        }
    }

}