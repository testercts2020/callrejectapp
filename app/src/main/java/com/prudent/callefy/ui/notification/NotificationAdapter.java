package com.prudent.callefy.ui.notification;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.prudent.callefy.R;
import com.prudent.callefy.model.CallLogsModel;
import com.prudent.callefy.utils.CommonUtils;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    List<CallLogsModel> mCallLogsList;
    Context mContext;

    public NotificationAdapter(List<CallLogsModel> mCallLogsList) {
        this.mCallLogsList = mCallLogsList;

    }

    public NotificationAdapter(List<CallLogsModel> mCallLogsList, Context context) {
        mContext = context;
//        if (isHeader)
        if (this.mCallLogsList == null)
            this.mCallLogsList = new ArrayList<>();
        this.mCallLogsList.addAll(mCallLogsList);

    }

    public void addItemNotificationAdapter(CallLogsModel model, boolean isHeader){
        int insertIndex = 0;
        if (!isHeader)
             insertIndex = 1;

        mCallLogsList.add(insertIndex, model);
        notifyItemInserted(insertIndex);
    }


    @Override
    public int getItemViewType(int position) {
        return mCallLogsList.get(position).isHeader() ? 0 : 1;
//        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemViewHeader = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_notification_header, parent, false);

        View itemViewSub = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_notification_sub, parent, false);

        if (viewType == 0) {
            return new ViewHolderHeader(itemViewHeader);
        } else {
            return new ViewHolderSub(itemViewSub);
        }


//        switch (viewType){
//            case 0: return new ViewHolderHeader(itemViewHeader);
//
//            case : return new ViewHolderSub(itemViewSub);
//
//        }
//        return new ViewHolderHeader(itemViewHeader);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        CallLogsModel item = mCallLogsList.get(position);


        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderHeader viewHolderHeader = (ViewHolderHeader) holder;
                viewHolderHeader.tvDate.setText(item.getDateTime());
                break;
            case 1:
                ViewHolderSub viewHolder2 = (ViewHolderSub) holder;
                viewHolder2.bind(item);
                String fromNo = item.getFromNo();
//                String charOne = fromNo.charAt(0)+"";
                viewHolder2.tvCircle.setText(item.getCallerName().charAt(0)+"");
//                viewHolder2.tvCircle.setText(item.getFromNo().charAt(0));
                viewHolder2.tvCallerName.setText(item.getCallerName());


                if (item.getCallType() != null && !item.getCallType().equals("")){
                    viewHolder2.tvCallerType.setText(item.getCallType());
                    if (item.getCallType().equals("Incoming")){
                        viewHolder2.tvCallerType.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.ic_phone_receiver, 0 ,0, 0);
                    }else if (item.getCallType().equals("Outgoing")){
                        viewHolder2.tvCallerType.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.ic_outgoing_call, 0 ,0, 0);
                    }else {
                        viewHolder2.tvCallerType.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.ic_missed_call, 0 ,0, 0);
                    }


                }
                viewHolder2.tvNumber.setText(item.getFromNo());
                Log.d("TAG", "getTimeFormatDateString: "+item.getCallerName());
                viewHolder2.tvTime.setText(CommonUtils.getTimeFormatDateString(item.getDateTime()));
                viewHolder2.ivDialCall.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:"+item.getFromNo()));
                        ((Activity)mContext).startActivity(intent);
                    }
                });

                viewHolder2.ivMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                        smsIntent.setType("vnd.android-dir/mms-sms");
                        smsIntent.putExtra("address", item.getFromNo());
//                        smsIntent.putExtra("sms_body","Body of Message");
                        ((Activity)mContext).startActivity(smsIntent);
                    }
                });

                viewHolder2.ivWhatsapp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PackageManager packageManager = mContext.getPackageManager();
                        Intent i = new Intent(Intent.ACTION_VIEW);

                        try {
                            String fromNo = item.getFromNo();
                            if (fromNo.trim().length() < 12 )
                                fromNo = "+91"+fromNo;
                            String url = "https://api.whatsapp.com/send?phone="+ fromNo +"&text=" + URLEncoder.encode("", "UTF-8");
                            i.setPackage("com.whatsapp");
                            i.setData(Uri.parse(url));
                            if (i.resolveActivity(packageManager) != null) {
                                mContext.startActivity(i);
                            }
                        } catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                });

                viewHolder2.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean expand = item.isExpanded();
                        item.setExpanded(!item.isExpanded());
                        notifyItemChanged(position);
                    }
                });
                break;
        }
    }


    @Override
    public int getItemCount() {
        return mCallLogsList.size();
    }


    class ViewHolderHeader extends RecyclerView.ViewHolder {

        @BindView(R.id.tvDate)
        TextView tvDate;

        public ViewHolderHeader(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class ViewHolderSub extends RecyclerView.ViewHolder {
        @BindView(R.id.rlCall)
        RelativeLayout rlCall;
        @BindView(R.id.tvCircle)
        TextView tvCircle;
        @BindView(R.id.tvCallerName)
        TextView tvCallerName;
        @BindView(R.id.tvCallerType)
        TextView tvCallerType;
        @BindView(R.id.tvTime)
        TextView tvTime;
        @BindView(R.id.tvNumber)
        TextView tvNumber;
        @BindView(R.id.ivDialCall)
        ImageView ivDialCall;
        @BindView(R.id.ivMessage)
        ImageView ivMessage;
        @BindView(R.id.ivWhatsapp)
        ImageView ivWhatsapp;
        @BindView(R.id.ivExpand)
        ImageView ivExpand;

        public ViewHolderSub(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this , itemView);
        }


        private void bind(CallLogsModel model){
            boolean expanded = model.isExpanded();


            rlCall.setVisibility(expanded ? View.VISIBLE : View.GONE);
            tvNumber.setVisibility(expanded ? View.VISIBLE : View.GONE);
            ivExpand.setImageDrawable(expanded ? mContext.getResources().getDrawable(R.drawable.ic_minus_img, mContext.getTheme()) :
                        mContext.getResources().getDrawable(R.drawable.ic_plus_img, mContext.getTheme()));
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                ivExpand.setImageDrawable(expanded ? mContext.getResources().getDrawable(R.drawable.ic_minus_img, mContext.getTheme()) :
//                        mContext.getResources().getDrawable(R.drawable.ic_plus_img, mContext.getTheme()));
//            } else {
//                ivExpand.setImageDrawable(expanded ? mContext.getResources().getDrawable(R.drawable.ic_minus_img , mContext.getTheme()) :
//                        mContext.getResources().getDrawable(R.drawable.ic_plus_img, mContext.getTheme()));
//            }
//            ivExpand.setBackgroundResource(expanded ? R.drawable.ic_minus_img : R.drawable.ic_plus_img );



        }


    }


}
