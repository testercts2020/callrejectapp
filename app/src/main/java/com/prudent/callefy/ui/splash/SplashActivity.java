package com.prudent.callefy.ui.splash;

import android.os.Bundle;
import android.os.Handler;

import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.database.CommonTransactionsDb;
import com.prudent.callefy.database.sms_content.SmsContent;
import com.prudent.callefy.dialog_fragment.dialog_expired.ExpireDialogFragment;
import com.prudent.callefy.dialog_fragment.dialog_expired.OnExpireDialogClickListener;
import com.prudent.callefy.dialog_fragment.ussd_code_issue.OnUssdCodeErrorDialogClickListener;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.ui.home.DashboardActivity;
import com.prudent.callefy.ui.login.CallRejectOptionActivity;
import com.prudent.callefy.ui.login.LoginActivity;
import com.prudent.callefy.utils.CommonUtils;
import com.prudent.callefy.utils.Constants;

public class SplashActivity extends BaseActivity implements OnExpireDialogClickListener {

    ExpireDialogFragment expireDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String expireDate = Constants.APP_EXPIRED_DATE;
        String currentDate = CommonUtils.getTodayDate();

        long expireDateLong = CommonUtils.getMillisecond(expireDate);
        long currentDateLong = CommonUtils.getMillisecond(currentDate);

        initView();

        if (currentDateLong > expireDateLong) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showToast("Please update your application.");

//                    finish();

                    String message = getString(R.string.your_app_has_expired);
                    String txtCancel = getString(R.string.cancel);
                    String txtAllow = getString(R.string.report);

//                    CommonUtils.showExpireMessageDialog(this, null, "", message, txtCancel, txtAllow);

                      expireDialog =  ExpireDialogFragment.newInstance(message, txtCancel, txtAllow);
//                    newFragment.setTargetFragment(fragment, 1004);

                    expireDialog.show(getSupportFragmentManager(), "tag");

                }
            },2000);
        }else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (SharedPrefs.isLogin()) {
                        startActivity(DashboardActivity.startActivity(getApplicationContext()));
                    }else {
//                        startActivity(CallRejectOptionActivity.startActivity(getApplicationContext()));
                        startActivity(LoginActivity.startActivity(getApplicationContext()));
                    }

                    finish();
                }
            },2000);
        }
    }

    @Override
    public int setLayout() {
        return R.layout.activity_splash;
    }

    @Override
    public void initView() {

        if (mTransactionsDb == null){
            mTransactionsDb = new CommonTransactionsDb(getApplicationContext());
        }

        SmsContent smsContent= new SmsContent();
        smsContent.setSmsContent("I am in an important Conference call, i will call you immediately" +
                " once I am free. Download KOLiFY : bitly//");
        smsContent.setId(1);

        SmsContent smsContent2= new SmsContent();
        smsContent2.setSmsContent("I am in an important Conference call, SMS or WhatsApp if " +
                "important. Download KOLiFY : bitly//");
        smsContent2.setId(2);

        SmsContent smsContent3= new SmsContent();
        smsContent3.setSmsContent("Sorry, I am on another call. Download KOLiFY : bitly//");
        smsContent3.setId(3);
        mTransactionsDb.saveSmsContent(smsContent);
        mTransactionsDb.saveSmsContent(smsContent2);
        mTransactionsDb.saveSmsContent(smsContent3);
    }
    @Override
    public boolean setToolbar() {
        return false;
    }

    @Override
    public boolean hideStatusbar() {
        return true;
    }

    @Override
    public boolean setFullScreen() {
        return true;
    }

    @Override
    public void onDelete() {

        showToast("Will update");
        expireDialog.dismiss();
        finish();
    }

    @Override
    public void onCancel() {
        expireDialog.dismiss();
        finish();
    }
}