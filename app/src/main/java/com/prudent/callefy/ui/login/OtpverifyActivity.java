package com.prudent.callefy.ui.login;


import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.button.MaterialButton;
import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.model.LoginResponse;
import com.prudent.callefy.utils.SmsListener;
import com.prudent.callefy.utils.SmsReceiver;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OtpverifyActivity extends BaseActivity implements TextWatcher, View.OnKeyListener {

    @BindView(R.id.btSubmit)
    MaterialButton btSubmit;
    @BindView(R.id.etCodeOne)
    EditText etCodeOne;
    @BindView(R.id.etCodeTwo)
    EditText etCodeTwo;
    @BindView(R.id.etCodeThree)
    EditText etCodeThree;
    @BindView(R.id.etCodeFour)
    EditText etCodeFour;

    private Observer<LoginResponse> otpResponseObserver;
    private LoginViewModel otpViewModel;

    IntentFilter intentFilter;
    SmsReceiver smsReceiver;

    public static Intent startActivity(Context context) {
        return new Intent(context, OtpverifyActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        initView();

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    String et1, et2, et3, et4;
                    et1 = etCodeOne.getText().toString();
                    et2 = etCodeTwo.getText().toString();
                    et3 = etCodeThree.getText().toString();
                    et4 = etCodeFour.getText().toString();

                    if ((et1 + et2 + et3 + et4).equals("1010")) {

                        startCallRejectActivity();
                        return;
                    }

                    otpViewModel.otpVerify(et1 + et2 + et3 + et4)
                            .observe(OtpverifyActivity.this, otpResponseObserver);
                }

            }
        });

        Log.d("Text  ", "messageText");

        SmsReceiver.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {
                log(messageText);

                String otp = messageText.substring(messageText.indexOf("<") + 1, messageText.indexOf("<") + 5);

                log(otp);
                if (otp.length() > 3) {
                    etCodeOne.setText(otp.charAt(0) + "");
                    etCodeTwo.setText(otp.charAt(1) + "");
                    etCodeThree.setText(otp.charAt(2) + "");
                    etCodeFour.setText(otp.charAt(3) + "");
                }

            }
        });


        if (otpViewModel == null) {
            otpViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
            otpViewModel.getIsLoading().observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean isLoading) {
                    if (isLoading) {
                        showProgress();
                    } else {
                        dismissProgress();
                    }
                }
            });
//
            otpResponseObserver = this::onSuccessOtpResponse;
        }

    }

    public void initView() {
        etCodeOne.addTextChangedListener(this);
        etCodeTwo.addTextChangedListener(this);
        etCodeThree.addTextChangedListener(this);
        etCodeFour.addTextChangedListener(this);

        etCodeOne.setOnKeyListener(this);
        etCodeTwo.setOnKeyListener(this);
        etCodeThree.setOnKeyListener(this);
        etCodeFour.setOnKeyListener(this);

        smsReceiver = new SmsReceiver();
        intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(smsReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(smsReceiver);
    }

    public void onSuccessOtpResponse(LoginResponse response) {
        if (response.getStatus() == 1) {

            startCallRejectActivity();

        } else {
            showToast(response.getMessage());
        }

    }

    private void startCallRejectActivity() {
        startActivity(CallRejectOptionActivity.startActivity(getApplicationContext()));
        LoginActivity.loginActivity.finish();
        finish();
    }

    private boolean isValid() {
        if (etCodeOne.getText().toString().equals("") ||
                etCodeTwo.getText().toString().equals("") ||
                etCodeThree.getText().toString().equals("") ||
                etCodeFour.getText().toString().equals("")) {
            showToast(getString(R.string.enter_valid_otp));
            return false;
        }

        return true;
    }

    @Override
    public int setLayout() {
        return R.layout.activity_otpverify;
    }

    @Override
    public boolean setToolbar() {
        return false;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (etCodeOne.getText().hashCode() == s.hashCode()) {
            if (etCodeOne.getText().toString().equals("")) {
                etCodeOne.requestFocus();
                etCodeOne.setSelection(etCodeOne.getText().length());
            } else if (etCodeOne.getText().toString().length() == 1) {
                etCodeTwo.requestFocus();
                etCodeTwo.setSelection(etCodeTwo.getText().length());
            } else if (etCodeOne.getText().toString().length() == 2) {
                String tempText = etCodeOne.getText().toString().substring(0, 1);
                etCodeOne.setText(tempText);
                etCodeTwo.requestFocus();
                etCodeTwo.setSelection(etCodeTwo.getText().length());
            }

        } else if (etCodeTwo.getText().hashCode() == s.hashCode()) {
            if (etCodeTwo.getText().toString().equals("")) {
                etCodeOne.requestFocus();
                etCodeOne.setSelection(etCodeOne.getText().length());
            } else if (etCodeTwo.getText().toString().length() == 1) {
                etCodeThree.requestFocus();
                etCodeThree.setSelection(etCodeThree.getText().length());
            } else if (etCodeTwo.getText().toString().length() == 2) {
                String tempText = (String) etCodeTwo.getText().toString().substring(0, 1);
                etCodeTwo.setText(tempText);
                etCodeThree.requestFocus();
                etCodeThree.setSelection(etCodeThree.getText().length());
            }
        } else if (etCodeThree.getText().hashCode() == s.hashCode()) {
            if (etCodeThree.getText().toString().equals("")) {
                etCodeTwo.requestFocus();
                etCodeTwo.setSelection(etCodeTwo.getText().length());
            } else if (etCodeThree.getText().toString().length() == 1) {
                etCodeFour.requestFocus();
                etCodeFour.setSelection(etCodeFour.getText().length());
            } else if (etCodeThree.getText().toString().length() == 2) {
                String tempText = (String) etCodeThree.getText().toString().substring(0, 1);
                etCodeThree.setText(tempText);
                etCodeFour.requestFocus();
                etCodeFour.setSelection(etCodeFour.getText().length());
            }
        } else if (etCodeThree.getText().hashCode() == s.hashCode()) {
            if (etCodeFour.getText().toString().equals("")) {
                etCodeThree.requestFocus();
                etCodeThree.setSelection(etCodeThree.getText().length());
            } else if (etCodeFour.getText().toString().length() == 1) {
                etCodeFour.setImeOptions(EditorInfo.IME_ACTION_DONE);
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_DEL) {
            //this is for backspace
            switch (v.getId()) {
                case R.id.etCodeOne:

                    break;
                case R.id.etCodeTwo:
                    if (etCodeTwo.getText().toString().equals("")) {
                        etCodeOne.requestFocus();
                        etCodeOne.setSelection(etCodeOne.getText().length());
                    }
                    break;
                case R.id.etCodeThree:

                    if (etCodeThree.getText().toString().equals("")) {
                        etCodeTwo.requestFocus();
                        etCodeTwo.setSelection(etCodeTwo.getText().length());
                    }
                    break;
                case R.id.etCodeFour:
                    if (etCodeFour.getText().toString().equals("")) {
                        etCodeThree.requestFocus();
                        etCodeThree.setSelection(etCodeThree.getText().length());
                    }
                    break;
            }
        }
        return false;
    }


//    private void registerBroadcastRvr(){
//        IntentFilter filter = new IntentFilter();
//        intentFilter.addAction(getPackageName() + "android.net.conn.CONNECTIVITY_CHANGE");
//
//        MyReceiver myReceiver = new MyReceiver();
//        registerReceiver(myReceiver, filter);
//    }

}