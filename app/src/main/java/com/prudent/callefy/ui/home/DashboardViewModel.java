package com.prudent.callefy.ui.home;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.prudent.callefy.api.ApiClient;
import com.prudent.callefy.base.BaseViewModel;
import com.prudent.callefy.database.AppErrorTask;
import com.prudent.callefy.model.CommonDataResponse;
import com.prudent.callefy.utils.Constants;

import java.util.HashMap;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DashboardViewModel extends BaseViewModel {
    private MutableLiveData<CommonDataResponse> commonDataResponseMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<CommonDataResponse> updateFailureCaseResponseMutableLiveData = new MutableLiveData<>();


    public MutableLiveData<CommonDataResponse> updateDeviceInfo(String userId,
                                                                String tag,
                                                                String mobile_no,
                                                                String operator,
                                                                String imei,
                                                                String model,
                                                                String manufacturer,
                                                                String brand,
                                                                String sdk,
                                                                String device_relese_no){
        HashMap<String, String> body = new HashMap<>();
        body.put("user_id", userId);
        body.put("tag", tag);
        body.put("mobile_no", mobile_no);
        body.put("operator", operator);
        body.put("imei", imei);
        body.put("model", model);
        body.put("manufacturer", manufacturer);
        body.put("brand", brand);
        body.put("sdk", sdk);
        body.put("device_relese_no", device_relese_no);


        ApiClient.getApiInterface().updateDeviceInfo(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CommonDataResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        setIsLoading(true);
                    }

                    @Override
                    public void onSuccess(CommonDataResponse response) {
                        setIsLoading(false);
                        commonDataResponseMutableLiveData.postValue(response);
                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);
                    }
                });
        return commonDataResponseMutableLiveData;

    }

    public MutableLiveData<CommonDataResponse> updateAppFailureCase(String userId,
                                                                    String tag,
                                                                    List<AppErrorTask> tasks) {

        JsonObject error_listObj = new JsonObject();
        JsonArray jsonArray = new JsonArray();
        for (AppErrorTask task : tasks) {


            JsonObject taskObj = new JsonObject();
            taskObj.addProperty("app_failed_title", task.getTask());
            taskObj.addProperty("app_failed_message", task.getDesc());
            taskObj.addProperty("error_date", task.getErrorDate());
            taskObj.addProperty("is_working_successful", task.getIsWorkingSuccessFul());

            jsonArray.add(taskObj);


        }
        error_listObj.add("error_list", jsonArray);
        error_listObj.addProperty("user_id", userId);
        error_listObj.addProperty("tag", tag);


        Log.d("TAG", "updateAppFailureCase: " + error_listObj.toString());

        ApiClient.getApiInterface().updateFailureCase(error_listObj)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CommonDataResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        setIsLoading(true);
                    }

                    @Override
                    public void onSuccess(CommonDataResponse response) {
                        setIsLoading(false);
                        updateFailureCaseResponseMutableLiveData.postValue(response);
                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);
                    }
                });

        return updateFailureCaseResponseMutableLiveData;
    }


}
