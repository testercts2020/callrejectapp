package com.prudent.callefy.ui.profile;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.material.button.MaterialButton;
import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.model.CommonDataResponse;
import com.prudent.callefy.model.ProfileMobileInfoModel;
import com.prudent.callefy.model.ProfileResponse;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.ui.home.DashboardActivity;
import com.prudent.callefy.utils.Constants;

import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends BaseActivity implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener
        {

    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.btUpdate)
    MaterialButton btUpdate;
    @BindView(R.id.etUssdActivationCode)
    EditText etUssdActivationCode;
    @BindView(R.id.etUssdDeActivationCode)
    EditText etUssdDeActivationCode;
    @BindView(R.id.etCallForwardingNo)
    EditText etCallForwardingNo;
    @BindView(R.id.tvMobNoUssdCode)
    TextView tvMobNoUssdCode;
    @BindView(R.id.btManuelActivation)
    Button btManuelActivation;
    @BindView(R.id.btSimInfo)
    Button btSimInfo;
    @BindView(R.id.btMobInfo)
    Button btMobInfo;
    @BindView(R.id.btRefresh)
    Button btRefresh;
    @BindView(R.id.etMobileNoOperator)
    EditText etMobileNoOperator;
    @BindView(R.id.btCallForwarding)
    MaterialButton btCallForwarding;
    @BindView(R.id.myprofile_img)
    CircleImageView myprofile_img;

    private Observer<ProfileResponse> profileResponseObserver;
    private Observer<CommonDataResponse> updatePrfResponseObserver;
    private ProfileViewModel profileViewModel;


    public static Intent startActivity(Context context) {
        return new Intent(context, ProfileActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Profile");
        initView();

        if (profileViewModel == null) {
            profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
            profileViewModel.getIsLoading().observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean isLoading) {
                    if (isLoading) {
                        showProgress();
                    } else {
                        dismissProgress();
                    }
                }
            });

            profileResponseObserver = this::onSuccessGetProfileResponse;
            updatePrfResponseObserver = this::onSuccessUpdateProfile;
        }
    }

    @Override
    public int setLayout() {
        return R.layout.activity_profile;
    }

    public void initView() {

        String act = SharedPrefs.getUssdActCode();
        String deAct = SharedPrefs.getUssdDeActCode();
        String callForwardingNo = SharedPrefs.getCallForwardingNo();
        String mobileNo = SharedPrefs.getString(SharedPrefs.Key.MOB_NO, "");
        String operator = SharedPrefs.getString(SharedPrefs.Key.OPERATOR, "");
        boolean isCallForwardingState = SharedPrefs.isCallForwardingState();


        etUssdActivationCode.setText(act);
        etUssdDeActivationCode.setText(deAct);
        etCallForwardingNo.setText(callForwardingNo);
        etMobileNoOperator.setText(mobileNo + " (" + operator + ")");
        etName.setText(SharedPrefs.getString(SharedPrefs.Key.USER_NAME, ""));
        etEmail.setText(SharedPrefs.getString(SharedPrefs.Key.USER_EMAIL, ""));

//        if (!isCallForwardingState) {
//            btManuelActivation.setText("Active");
////            btManuelActivation.setText("Act/InActive");
//            tvMobNoUssdCode.setText(act);
//        } else {
//            btManuelActivation.setText("InActive");
////            btManuelActivation.setText("Act/InActive");
//            tvMobNoUssdCode.setText(deAct);
//        }

        if (SharedPrefs.isCallForwardingStateManual()) {
            btManuelActivation.setText(getString(R.string.click_to_inactive));
        } else {
            btManuelActivation.setText(getString(R.string.click_to_active));
        }

        btUpdate.setOnClickListener(this::onClick);
        btManuelActivation.setOnClickListener(this::onClick);
        btSimInfo.setOnClickListener(this::onClick);
        btMobInfo.setOnClickListener(this::onClick);
        btRefresh.setOnClickListener(this::onClick);
        btCallForwarding.setOnClickListener(this::onClick);

//                .placeholder(R.drawable.placeholder) // any placeholder to load at start
//                .error(R.drawable.imagenotfound)  // any image in case of error
//                .override(200, 200); // resizing
//        .centerCrop();
//        .into(imageView);
    }

    @Override
    public boolean setToolbar() {
        return true;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        profileViewModel.getProfile(SharedPrefs.getUserId())
                .observe(this, profileResponseObserver);
    }

    //    public static void editText(){
//
//        etName.setFocusable(true);
//    }
//
//    public void edt(){
//        etName.setFocusable(true);
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.right_menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        } else if (item.getItemId() == R.id.action_edit) {
//            etEmail.setFocusable(true);
//            etEmail.setFocusableInTouchMode(true);
//            etName.setFocusable(true);
//            etName.setFocusableInTouchMode(true);
//            etName.requestFocus();
//            btUpdate.setVisibility(View.VISIBLE);

            startActivity(ProfileEditActivity.startActivity(getApplicationContext()));

        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btUpdate:
                btUpdate.setVisibility(View.GONE);
                String tempAct = etUssdActivationCode.getText().toString();
                String tempDeAct = etUssdDeActivationCode.getText().toString();
                String tempForwardNo = etCallForwardingNo.getText().toString();
                SharedPrefs.setString(SharedPrefs.Key.USSD_ACT_CODE, tempAct);
                SharedPrefs.setString(SharedPrefs.Key.USSD_DE_ACT_CODE, tempDeAct);
                SharedPrefs.setString(SharedPrefs.Key.CALL_FORWARDING_NO, tempForwardNo);
                profileViewModel.updateProfile(SharedPrefs.getUserId(),
                        Constants.TAG_API_UPDATE_PROFILE,
                        etName.getText().toString(),
                        etEmail.getText().toString(),
                        SharedPrefs.getString(SharedPrefs.Key.MOB_NO, ""),
                        SharedPrefs.getString(SharedPrefs.Key.OPERATOR, ""),
                        "",
                        "",
                        "",
                        "",
                        "",
                        "").observe(this, updatePrfResponseObserver);
                break;
            case R.id.btManuelActivation:

                break;
            case R.id.btSimInfo:
                getSimInformation();
                break;
            case R.id.btMobInfo:
                getDeviceInfo();
                break;
            case R.id.btRefresh:


//                Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "8129779445"));
//                startActivity(callIntent);

                break;
            case R.id.btCallForwarding:

                break;
        }
    }

    public void onSuccessGetProfileResponse(ProfileResponse response) {

        if (response.getStatus() == 1) {

            ProfileMobileInfoModel mobile = response.getUserInfo().getMobileNoOperatorUssdcode()
                    .get(0);

            etMobileNoOperator.setText(mobile.getMobileNo());
            SharedPrefs.setString(SharedPrefs.Key.USSD_ACT_CODE, mobile.getUssd_code_activation());
            SharedPrefs.setString(SharedPrefs.Key.USSD_DE_ACT_CODE, mobile.getUssd_code_deactivation());
            SharedPrefs.setString(SharedPrefs.Key.MOB_NO, mobile.getMobileNo());
            SharedPrefs.setString(SharedPrefs.Key.OPERATOR, mobile.getOperator());
            SharedPrefs.setInt(SharedPrefs.Key.OPERATOR_BACKGROUND_RUNNIG_STATUS, mobile.getStatusCode());

            if (response.getUserInfo().getUserName() != null) {
                SharedPrefs.setString(SharedPrefs.Key.USER_NAME, response.getUserInfo().getUserName());
                etName.setText(response.getUserInfo().getUserName());
            }
            if (response.getUserInfo().getEmailId() != null) {
                SharedPrefs.setString(SharedPrefs.Key.USER_EMAIL, response.getUserInfo().getEmailId());
                etEmail.setText(response.getUserInfo().getEmailId());
            }
            if (response.getUserInfo().getProfileURL() != null) {
                SharedPrefs.setString(SharedPrefs.Key.USER_PROFILE_IMAGE, response.getUserInfo().getProfileURL());
                String imageUrl =response.getUserInfo().getProfileURL();
                Glide.with(this)
                        .load(imageUrl ) // image url
                        .error(R.drawable.ic_loading)
//                        .diskCacheStrategy(DiskCacheStrategy.NONE)
//                        .skipMemoryCache(true)
                        .into(myprofile_img);
            }


//            if (!SharedPrefs.isCallForwardingState()) {
//                btManuelActivation.setText("Act/InActive");
//                tvMobNoUssdCode.setText(mobile.getUssd_code_activation());
//            } else {
//                btManuelActivation.setText("Act/InActive");
//                tvMobNoUssdCode.setText(mobile.getUssd_code_deactivation());
//            }
        }

    }

    public void onSuccessUpdateProfile(CommonDataResponse response) {
        if (response.getStatus() == 1) {
            showToast(response.getMessage());
        }
    }

    /*Get SIM info  start *****/
    public void getSimInformation() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telephonyManager.getSimState();

        switch (simState) {

            case (TelephonyManager.SIM_STATE_ABSENT):
                break;
            case (TelephonyManager.SIM_STATE_NETWORK_LOCKED):
                break;
            case (TelephonyManager.SIM_STATE_PIN_REQUIRED):
                break;
            case (TelephonyManager.SIM_STATE_PUK_REQUIRED):
                break;
            case (TelephonyManager.SIM_STATE_UNKNOWN):
                break;
            case (TelephonyManager.SIM_STATE_READY): {

                // Get the SIM country ISO code
                String simCountry = telephonyManager.getSimCountryIso();
                log(simCountry);

                // Get the operator code of the active SIM (MCC + MNC)
                String simOperatorCode = telephonyManager.getSimOperator();
                log(simOperatorCode);

                // Get the name of the SIM operator
                String simOperatorName = telephonyManager.getSimOperatorName();
                log(simOperatorName);

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    String simNumber = telephonyManager.getLine1Number();
                    log(simNumber);
                    return;
                }
                String simNumber = telephonyManager.getLine1Number();
                log(simNumber);

                String simNumber2 = telephonyManager.getVoiceMailNumber();
                log(simNumber2);
                String simNumber3 = telephonyManager.getDeviceSoftwareVersion();
                log(simNumber3);


                TelephonyManager mTelephonyMgr;
                mTelephonyMgr = (TelephonyManager)
                        getSystemService(Context.TELEPHONY_SERVICE);
                mTelephonyMgr.getLine1Number();
                log(mTelephonyMgr.getLine1Number());

                getSimInfoUsingSubC();
                // Get the SIMâ€™s serial number
//                String simSerial = telephonyManager.getSimSerialNumber();
            }
        }

    }

    public void getSimInfoUsingSubC() {
        SubscriptionManager subscriptionManager = (SubscriptionManager) getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        List<SubscriptionInfo> subscriptionInfoList = subscriptionManager.getActiveSubscriptionInfoList();

        if (subscriptionInfoList != null && subscriptionInfoList.size() > 0) {
            for (SubscriptionInfo info : subscriptionInfoList) {
                String carrierName = info.getCarrierName().toString();
                String mobileNo = info.getNumber();
                String countyIso = info.getCountryIso();
                String countyIsoe = info.getCountryIso();
                int dataRoaming = info.getDataRoaming();

                log("Mobile No  " + mobileNo);
                log("Mobile c  " + carrierName);
                log("Mobile iso  " + countyIso);
            }
        }

        getSimInfoUsingGoogleApiClient();
    }

    public void getSimInfoUsingGoogleApiClient() {

        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Auth.CREDENTIALS_API)
                .build();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }


        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();

        PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(mGoogleApiClient, hintRequest);
        try {
            startIntentSenderForResult(intent.getIntentSender(), 1008,
                    null, 0, 0, 0, null);
        } catch (IntentSender.SendIntentException e) {
            Log.e("", "Could not start hint picker Intent", e);
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1008:
                if (resultCode == RESULT_OK) {
                    Credential cred = data.getParcelableExtra(Credential.EXTRA_KEY);
//                    cred.getId====: ====+919*******
                    Log.e("cred.getId", cred.getId());


                } else {
                    // Sim Card not found!
                    Log.e("cred.getId", "1008 else");

                    return;
                }


                break;
        }
    }



    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    /* ****** Get SIM info  END *****/

    /*  Get Device info  start *****/

    public void getDeviceInfo() {
        StringBuffer infoBuffer = new StringBuffer();

        infoBuffer.append("-------------------------------------\n");
        infoBuffer.append("Model :" + Build.MODEL + "\n");//The end-user-visible name for the end product.
        infoBuffer.append("Device: " + Build.DEVICE + "\n");//The name of the industrial design.
        infoBuffer.append("Manufacturer: " + Build.MANUFACTURER + "\n");//The manufacturer of the product/hardware.
        infoBuffer.append("Board: " + Build.BOARD + "\n");//The name of the underlying board, like "goldfish".
        infoBuffer.append("Brand: " + Build.BRAND + "\n");//The consumer-visible brand with which the product/hardware will be associated, if any.
        infoBuffer.append("Serial: " + Build.SERIAL + "\n");
        infoBuffer.append("sdk : " + Build.VERSION.SDK_INT + "\n");
        infoBuffer.append("Relese : " + Build.VERSION.RELEASE + "\n");
        infoBuffer.append("-------------------------------------\n");

        log(infoBuffer.toString());

    }

    /* ****** Get Device info  END *****/


}