package com.prudent.callefy.ui.profile;

import com.prudent.callefy.database.AppErrorTask;

import java.util.List;

public class ErrorListJsonCreate {
    String user_id;
    String tag;
    List<AppErrorTask> tasks;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public List<AppErrorTask> getTasks() {
        return tasks;
    }

    public void setTasks(List<AppErrorTask> tasks) {
        this.tasks = tasks;
    }
}
