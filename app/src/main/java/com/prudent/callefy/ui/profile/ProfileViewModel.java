package com.prudent.callefy.ui.profile;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.prudent.callefy.api.ApiClient;
import com.prudent.callefy.base.BaseViewModel;
import com.prudent.callefy.database.AppErrorTask;
import com.prudent.callefy.model.CommonDataResponse;
import com.prudent.callefy.model.LoginResponse;
import com.prudent.callefy.model.ProfileResponse;
import com.prudent.callefy.utils.CommonUtils;
import com.prudent.callefy.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ProfileViewModel extends BaseViewModel {
    private MutableLiveData<ProfileResponse> profileResponseMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<CommonDataResponse> updatePrfResponseMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<CommonDataResponse> updateEditPrfResponseMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<CommonDataResponse> feedbackResponseMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<CommonDataResponse> updateFailureCaseResponseMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<ProfileResponse> getProfile(String userId) {

        HashMap<String, String> body = new HashMap<>();
        body.put("user_id", userId);
        body.put("tag", Constants.TAG_API_GET_PROFILE);
        ApiClient.getApiInterface().getProfile(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<ProfileResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        setIsLoading(true);
                    }

                    @Override
                    public void onSuccess(ProfileResponse response) {
                        setIsLoading(false);
                        profileResponseMutableLiveData.postValue(response);

                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);
                    }
                });
        return profileResponseMutableLiveData;
    }

    public MutableLiveData<CommonDataResponse> updateProfile(String userId,
                                                             String tag,
                                                             String name,
                                                             String email,
                                                             String mobile_no,
                                                             String operatorname,
                                                             String imei,
                                                             String model,
                                                             String manufacturer,
                                                             String brand,
                                                             String sdk,
                                                             String relese_no) {

        HashMap<String, String> body = new HashMap<>();
        body.put("user_id", userId);
        body.put("tag", tag);
        body.put("name", name);
        body.put("email", email);
        body.put("mobile_no", mobile_no);
        body.put("operatorname", operatorname);
        ApiClient.getApiInterface().updateProfile(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CommonDataResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        setIsLoading(true);
                    }

                    @Override
                    public void onSuccess(CommonDataResponse response) {
                        setIsLoading(false);
                        updatePrfResponseMutableLiveData.postValue(response);
                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);
                    }
                });

        return updatePrfResponseMutableLiveData;
    }

    public MutableLiveData<CommonDataResponse> updateEditProfile(String userId,
                                                                 String tag,
                                                                 String name,
                                                                 String email,
                                                                 String mobile_no,
                                                                 String operatorname,
                                                                 String imei,
                                                                 String model,
                                                                 String manufacturer,
                                                                 String brand,
                                                                 String sdk,
                                                                 String relese_no) {

        HashMap<String, String> body = new HashMap<>();
        body.put("user_id", userId);
        body.put("tag", tag);
        body.put("name", name);
        body.put("email", email);
        body.put("mobile_no", mobile_no);
        body.put("operatorname", operatorname);
        ApiClient.getApiInterface().updateProfile(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CommonDataResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        setIsLoading(true);
                    }

                    @Override
                    public void onSuccess(CommonDataResponse response) {
                        setIsLoading(false);
                        updateEditPrfResponseMutableLiveData.postValue(response);
                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);
                    }
                });

        return updateEditPrfResponseMutableLiveData;
    }


    public MutableLiveData<CommonDataResponse> updateEditProfileWithImage(String userId,
                                                                          String tag,
                                                                          String name,
                                                                          String email,
                                                                          String mobile_no,
                                                                          String file_name) {

        MultipartBody.Part body;

        if (file_name == null || file_name.trim().equals("")){
            body = null;
        }else {
            File file = new File(file_name);
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);

            // MultipartBody.Part is used to send also the actual file name
             body =
                    MultipartBody.Part.createFormData("profile_image", file.getName(), requestFile);

        }

        ApiClient.getApiInterface().updateProfileWithImage(getRequestBodyData(userId),
                getRequestBodyData(tag),
                getRequestBodyData(name),
                getRequestBodyData(email),
                getRequestBodyData(mobile_no),
                body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CommonDataResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        setIsLoading(true);
                    }

                    @Override
                    public void onSuccess(CommonDataResponse response) {
                        setIsLoading(false);
                        updateEditPrfResponseMutableLiveData.postValue(response);
                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);
                    }
                });

        return updateEditPrfResponseMutableLiveData;

    }


    private RequestBody getRequestBodyData(String data) {

        RequestBody fullName =
                RequestBody.create(MediaType.parse("multipart/form-data"), data);
        return fullName;

    }

    public MutableLiveData<CommonDataResponse> feedbackUpdate(String userId,
                                                              String tag,
                                                              String subject,
                                                              String message) {

        HashMap<String, String> body = new HashMap<>();
        body.put("user_id", userId);
        body.put("tag", tag);
        body.put("subject", subject);
        body.put("message", message);

        ApiClient.getApiInterface().updateFeedback(body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CommonDataResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        setIsLoading(true);
                    }

                    @Override
                    public void onSuccess(CommonDataResponse response) {
                        setIsLoading(false);
                        updateEditPrfResponseMutableLiveData.postValue(response);
                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);
                    }
                });

        return updateEditPrfResponseMutableLiveData;
    }

    public MutableLiveData<CommonDataResponse> updateAppFailureCase(String userId,
                                                                    String tag,
                                                                    List<AppErrorTask> tasks) {

        JsonObject error_listObj = new JsonObject();
        JsonArray jsonArray = new JsonArray();
        for (AppErrorTask task : tasks) {


            JsonObject taskObj = new JsonObject();
            taskObj.addProperty("app_failed_title", task.getTask());
            taskObj.addProperty("app_failed_message", task.getDesc());
            taskObj.addProperty("error_date", task.getErrorDate());
            taskObj.addProperty("is_working_successful", task.getIsWorkingSuccessFul());

            jsonArray.add(taskObj);


        }
        error_listObj.add("error_list", jsonArray);
        error_listObj.addProperty("user_id", userId);
        error_listObj.addProperty("tag", tag);


        Log.d("TAG", "updateAppFailureCase: " + error_listObj.toString());

        ApiClient.getApiInterface().updateFailureCase(error_listObj)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CommonDataResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        setIsLoading(true);
                    }

                    @Override
                    public void onSuccess(CommonDataResponse response) {
                        setIsLoading(false);
                        updateFailureCaseResponseMutableLiveData.postValue(response);
                    }

                    @Override
                    public void onError(Throwable e) {
                        setIsLoading(false);
                    }
                });

        return updateFailureCaseResponseMutableLiveData;
    }


}
