package com.prudent.callefy.ui.groups;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.database.CommonTransactionsDb;
import com.prudent.callefy.database.groups.ContactGroup;
import com.prudent.callefy.database.groups.group_item.ContGroupItem;
import com.prudent.callefy.dialog_fragment.dialog_edit_group.EditGroupDialogFragment;
import com.prudent.callefy.dialog_fragment.dialog_edit_group.OnEditGroupDialogClickListener;
import com.prudent.callefy.dialog_fragment.dialog_edit_sms_content.EditSmsDialogFragment;
import com.prudent.callefy.dialog_fragment.dialog_expired.ExpireDialogFragment;
import com.prudent.callefy.dialog_fragment.dialog_expired.OnExpireDialogClickListener;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.ui.groups.group_item.GroupItemActivity;
import com.prudent.callefy.ui.sms_content.SmsContentActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupsActivity extends BaseActivity implements View.OnClickListener,
        CommonTransactionsDb.CommonTransGroupDbCallBack, GroupsCallback, OnExpireDialogClickListener,
        OnEditGroupDialogClickListener {
    @BindView(R.id.rvList)
    RecyclerView rvList;
    @BindView(R.id.btAddGroup)
    MaterialButton btAddGroup;
//    @BindView(R.id.etGroupName)
//    TextInputEditText etGroupName;

    int tempPosition ;
    ContactGroup tempGroupItem;


    private GroupsAdapter adapter;


    private CommonTransactionsDb mTransactionsDb;

    ExpireDialogFragment expireDialog;

    EditGroupDialogFragment editGroupDialog;


    public static Intent startActivity(Context context) {
        return new Intent(context, GroupsActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Groups");

        initView();
    }

    @Override
    public int setLayout() {
        return R.layout.activity_groups;
    }

    @Override
    public void initView() {

        rvList.setHasFixedSize(true);
        rvList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        if (mTransactionsDb == null) {
            mTransactionsDb = new CommonTransactionsDb(getApplicationContext(), this);
        }



        btAddGroup.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mTransactionsDb.deleteContactGroupsNotPermanent();
        mTransactionsDb.getContactGroups();
    }

    @Override
    public boolean setToolbar() {
        return true;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btAddGroup:

                String title = getString(R.string.group_name);
                String txtCancel = getString(R.string.cancel);
                String txtAllow = getString(R.string.add);

//                    CommonUtils.showExpireMessageDialog(this, null, "", message, txtCancel, txtAllow);

                editGroupDialog = EditGroupDialogFragment.newInstance(title, txtCancel, txtAllow, "", true);
//                    newFragment.setTargetFragment(fragment, 1004);

                editGroupDialog.show(getSupportFragmentManager(), "tag");

//                if (etGroupName.getText().toString().trim().equals("")) {
//                    showToast(getString(R.string.group_name_field_is_rqd));
//                }else {
//                    mTransactionsDb.saveContactGroup(new ContactGroup(etGroupName.getText().toString().trim(),
//                            SharedPrefs.getInt(SharedPrefs.Key.CALL_REJECT_STATE_ID, 1),
//                            SharedPrefs.getString(SharedPrefs.Key.CALL_REJECT_STATE_DESC, "")
//                            ));
//
////                    adapter.addNewSmsContent(new ContactGroup(etGroupName.getText().toString().trim()));
////                    etGroupName.setText("");
//                }

                break;

        }
    }

    @Override
    public void getGroupDbList(List<ContactGroup> item) {
//        if (adapter == null){
            adapter = new GroupsAdapter(item,
                    this, this);

            rvList.setAdapter(adapter);
//        }else {
//
//            if (item.size()> adapter.getItemCount()){
//                adapter.addNewSmsContent(item.get(item.size()-1));
//            }
//        }


    }

    @Override
    public void onGetGroupDbItem(List<ContactGroup> item) {

    }

    @Override
    public void onGetGroupDbLastItem(List<ContactGroup> item) {
//        startActivity(GroupItemActivity.startActivity(getApplicationContext(), item.get(0).getId(), item.get(0).getGroup_name()));

        Intent intent =SmsContentActivity.startActivity(getApplicationContext(),true);
        intent.putExtra("GROUP_ID", item.get(0).getId());
        startActivity(intent);

    }

    @Override
    public void onSavedGroupDb(boolean isSaved) {
        if (isSaved) {
            editGroupDialog.dismiss();
            mTransactionsDb.getLastAddedContactGroup();
        }else {
            showToast("Group name already saved, Please change new group name!");
        }

    }

    @Override
    public void onUpdateGroupDb(boolean isUpdated) {
        if (isUpdated) {
            adapter.updateItem(tempPosition, tempGroupItem.getGroup_name());
        editGroupDialog.dismiss();
        } else{
            showToast("Group name already saved, Please change group name!");
        }
    }

    @Override
    public void onListItemClick(int position, ContactGroup group) {

//        startActivity(GroupItemActivity.startActivity(getApplicationContext(), group.getId()));
//        startActivity(SmsContentActivity.startActivity(getApplicationContext(), group.getId()));

        Intent intent =SmsContentActivity.startActivity(getApplicationContext(),true);
        intent.putExtra("GROUP_ID", group.getId());
        startActivity(intent);
    }

    @Override
    public void onMoveToLastPosition(int size) {
        rvList.scrollToPosition(size);
    }

    @Override
    public void onDeleteGroup(int position, ContactGroup group) {

         tempPosition = position ;
         tempGroupItem = group;

        String message = getString(R.string.are_you_sure_want_to_delete);
        String txtCancel = getString(R.string.cancel);
        String txtAllow = getString(R.string.delete);

        expireDialog =  ExpireDialogFragment.newInstance(message, txtCancel, txtAllow);
        expireDialog.show(getSupportFragmentManager(), "tag");

    }

    @Override
    public void onEditGroup(int position, ContactGroup group) {

        tempPosition = position ;
        tempGroupItem = group;

        String title = getString(R.string.group_name);
        String txtCancel = getString(R.string.delete);
        String txtAllow = getString(R.string.update);

//                    CommonUtils.showExpireMessageDialog(this, null, "", message, txtCancel, txtAllow);

        editGroupDialog = EditGroupDialogFragment.newInstance(title, txtCancel, txtAllow, group.getGroup_name(), false);
//                    newFragment.setTargetFragment(fragment, 1004);

        editGroupDialog.show(getSupportFragmentManager(), "tag");


//
//        String message = getString(R.string.are_you_sure_want_to_delete);
//        String txtCancel = getString(R.string.cancel);
//        String txtAllow = getString(R.string.delete);
//
//        expireDialog =  ExpireDialogFragment.newInstance(message, txtCancel, txtAllow);
//        expireDialog.show(getSupportFragmentManager(), "tag");




    }

    @Override
    public void onDelete() {
        adapter.removeItem(tempPosition);
        mTransactionsDb.deleteContactGroup(tempGroupItem);
        mTransactionsDb.deleteContactGroupContentList(tempGroupItem.getId());
        expireDialog.dismiss();

    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onAddGroup(String name) {
        mTransactionsDb.saveContactGroup(new ContactGroup(name,
                            SharedPrefs.getInt(SharedPrefs.Key.CALL_REJECT_STATE_ID, 1),
                            SharedPrefs.getString(SharedPrefs.Key.CALL_REJECT_STATE_DESC, ""),
                SharedPrefs.getString(SharedPrefs.Key.DEFAULT_SMS_CONTENT, "")
                            ));
    }

    @Override
    public void onUpdateGroup(String name) {

        tempGroupItem.setGroup_name(name);
        mTransactionsDb.updateContactGroup( tempGroupItem);

    }

    @Override
    public void onDeleteGroup() {
        String message = getString(R.string.are_you_sure_want_to_delete);
        String txtCancel = getString(R.string.cancel);
        String txtAllow = getString(R.string.delete);

        expireDialog =  ExpireDialogFragment.newInstance(message, txtCancel, txtAllow);
        expireDialog.show(getSupportFragmentManager(), "tag");
    }


}