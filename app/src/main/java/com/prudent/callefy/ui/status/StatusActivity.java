package com.prudent.callefy.ui.status;
 
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.database.CommonTransactionsDb;
import com.prudent.callefy.database.groups.ContactGroup;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.ui.groups.GroupsAdapter;
import com.prudent.callefy.ui.groups.GroupsCallback;
import com.prudent.callefy.ui.sms_content.SmsContentActivity;
import com.prudent.callefy.utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StatusActivity extends BaseActivity implements CommonTransactionsDb.CommonTransGroupDbCallBack,
        GroupsCallback, View.OnClickListener {
    @BindView(R.id.tvStatus)
    TextView tvStatus;
    @BindView(R.id.rvList)
    RecyclerView rvList;
    @BindView(R.id.lltDefaultStatus)
    LinearLayout lltDefaultStatus;

    private StatusGroupAdapter adapter;

    public static Intent startActivity(Context context) {
        return new Intent(context, StatusActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Status");
        initView();
    }

    @Override
    public int setLayout() {
        return R.layout.activity_status;
    }

    @Override
    public void initView() {
//        SharedPrefs.setInt(SharedPrefs.Key.CALL_REJECT_STATE, 1);
//        if (SharedPrefs.getInt(SharedPrefs.Key.CALL_REJECT_STATE_ID, 1) == 1){
//            tvStatus.setText(getString(R.string.accept_the_call));
//        }else if (SharedPrefs.getInt(SharedPrefs.Key.CALL_REJECT_STATE_ID, 1) == 2){
//            tvStatus.setText(getString(R.string.reject_the_call));
//        }else {
//            tvStatus.setText(getString(R.string.reject_the_call_with_sms));
//        }


        rvList.setHasFixedSize(true);
        rvList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

//        if (mTransactionsDb == null) {
            mTransactionsDb = new CommonTransactionsDb(getApplicationContext(), this);
//        }



        lltDefaultStatus.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mTransactionsDb.getContactGroups();
        tvStatus.setText(SharedPrefs.getString(SharedPrefs.Key.CALL_REJECT_STATE_DESC, ""));
        if (SharedPrefs.getString(SharedPrefs.Key.CALL_REJECT_STATE_DESC, "").equals(getString(R.string.reject_the_call_with_sms)))
            tvStatus.setText(SharedPrefs.getString(SharedPrefs.Key.CALL_REJECT_STATE_DESC, "")+
                    ": "+SharedPrefs.getString(SharedPrefs.Key.DEFAULT_SMS_CONTENT, ""));

    }

    @Override
    public boolean setToolbar() {
        return true;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }

    @Override
    public void getGroupDbList(List<ContactGroup> item) {
        if (adapter == null){
            adapter = new StatusGroupAdapter(item,
                    this, this);

            rvList.setAdapter(adapter);

        }
    }

    @Override
    public void onGetGroupDbItem(List<ContactGroup> item) {

    }

    @Override
    public void onGetGroupDbLastItem(List<ContactGroup> item) {

    }

    @Override
    public void onSavedGroupDb(boolean isSaved) {

    }

    @Override
    public void onUpdateGroupDb(boolean isUpdated) {

    }

    @Override
    public void onListItemClick(int position, ContactGroup group) {

    }

    @Override
    public void onMoveToLastPosition(int size) {

    }

    @Override
    public void onDeleteGroup(int position, ContactGroup group) {

    }

    @Override
    public void onEditGroup(int position, ContactGroup group) {

    }

    @Override
    public void onClick(View v) {
        startActivity(SmsContentActivity.startActivity(getApplicationContext(),false));

    }
}