package com.prudent.callefy.ui.groups.group_item;

import com.prudent.callefy.database.groups.group_item.ContGroupItem;
import com.prudent.callefy.dialog_fragment.dialog_contacts.ContactModel;

public interface SelectContactCallback {

    void onRemoveContacts(int position,ContGroupItem item);

    void onMoveToLastPosition(int size);

}
