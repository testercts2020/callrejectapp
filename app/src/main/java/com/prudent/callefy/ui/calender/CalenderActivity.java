package com.prudent.callefy.ui.calender;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.database.calender_event.CalenderEvent;
import com.prudent.callefy.model.CalenderModel;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.utils.ReedCalender;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CalenderActivity extends BaseActivity implements CalenderCallback,
       View.OnClickListener {

    @BindView(R.id.rvList)
    RecyclerView rvList;
    @BindView(R.id.clNoData)
    ConstraintLayout clNoData;


    ArrayList<CalenderModel> calenderList = new ArrayList<>();
    private CalenderAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
//    UssdCallService ussdCallService;

    public static Intent startActivity(Context context) {
        return new Intent(context, CalenderActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Calender");

        initView();
    }

    @Override
    public int setLayout() {
        return R.layout.activity_calender;
    }

    @Override
    public void initView() {

        getCalenderEventList();

        // Removes blinks
        ((SimpleItemAnimator) rvList.getItemAnimator()).setSupportsChangeAnimations(false);

        rvList.setHasFixedSize(true);
        rvList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        adapter = new CalenderAdapter(calenderList,
                this, this);

        rvList.setAdapter(adapter);

        if (calenderList == null || calenderList.size() < 1){
            clNoData.setVisibility(View.VISIBLE);
        }

//        btViewDashboard.setOnClickListener(this::onClick);

    }

    private void getCalenderEventList() {
        calenderList = ReedCalender.readCalendarEventDateRangeToday(
                getApplicationContext());

    }

    @Override
    public boolean setToolbar() {
        return true;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }

    @Override
    public void active() {

        SharedPrefs.setIsManualDoNotDisturbActive(true);

    }

    @Override
    public void inActive() {
        SharedPrefs.setIsManualDoNotDisturbActive(false);

    }

    @Override
    public void onCalenderDndActive(CalenderEvent event) {

    }

    @Override
    public void onCalenderDndInActive(CalenderEvent event) {

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){


        }
    }
}