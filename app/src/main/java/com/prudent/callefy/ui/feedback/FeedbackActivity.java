package com.prudent.callefy.ui.feedback;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.database.AppErrorTask;
import com.prudent.callefy.database.CommonTransactionsDb;
import com.prudent.callefy.model.CommonDataResponse;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.ui.profile.ProfileViewModel;
import com.prudent.callefy.utils.Constants;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedbackActivity extends BaseActivity implements View.OnClickListener,
        CommonTransactionsDb.CommonTransAppErrorListDbCallBack {

    @BindView(R.id.btUpdate)
    MaterialButton btUpdate;
    @BindView(R.id.etSubject)
    TextInputEditText etSubject;
    @BindView(R.id.etMessage)
    TextInputEditText etMessage;

    private Observer<CommonDataResponse> feedbackResponseObserver;
    private Observer<CommonDataResponse> failureCaseResponseObserver;
    private ProfileViewModel profileViewModel;

    public static Intent startActivity(Context context) {
        return new Intent(context, FeedbackActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Feedback.");


        initView();

        mTransactionsDb = new CommonTransactionsDb(getApplicationContext() , this::getAppErrorTaskList);

        if (profileViewModel == null) {
            profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
            profileViewModel.getIsLoading().observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean isLoading) {
                    if (isLoading) {
                        showProgress();
                    } else {
                        dismissProgress();
                    }
                }
            });

            feedbackResponseObserver = this::onSuccessUpdateFeedback;
            failureCaseResponseObserver = this::onSuccessUpdateFailureCase;

        }
    }

    @Override
    public void initView() {

        btUpdate.setOnClickListener(this::onClick);
    }

    @Override
    public int setLayout() {
        return R.layout.activity_feedback;
    }

    @Override
    public boolean setToolbar() {
        return true;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btUpdate:
                if (isValid()) {

                    profileViewModel.feedbackUpdate(SharedPrefs.getUserId(),
                            Constants.TAG_API_UPDATE_FEEDBACK,
                            etSubject.getText().toString(),
                            etMessage.getText().toString())
                            .observe(this, feedbackResponseObserver);
                }

                break;
        }
    }

    private boolean isValid() {
        if (etSubject.getText().toString().trim().equals("")) {

            showToast("Please enter subject!");

            return false;

        } else if (etMessage.getText().toString().trim().equals("")) {

            showToast("Please enter message!");

            return false;

        }
        return true;
    }

    public void onSuccessUpdateFeedback(CommonDataResponse response) {

        if (response.getStatus() == 1) {
            showToast("Thank you for your feedback!");
            etSubject.setText("");
            etMessage.setText("");

            mTransactionsDb.getTasks();



        } else
            showToast(response.getMessage());

    }

    private void onSuccessUpdateFailureCase(CommonDataResponse response){
        if (response.getStatus() == 1) {
            mTransactionsDb.updateStatusAfterSync();

        }else {
            showToast(response.getMessage());
        }
    }

    @Override
    public void getAppErrorTaskList(List<AppErrorTask> tasks) {
        if (tasks != null && tasks.size() >0)
        profileViewModel.updateAppFailureCase(SharedPrefs.getUserId(),
                Constants.TAG_API_APP_FAILED_CASE,
                tasks)
                .observe(this, failureCaseResponseObserver);
    }
}