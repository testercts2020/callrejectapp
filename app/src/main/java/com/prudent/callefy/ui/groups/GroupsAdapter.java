package com.prudent.callefy.ui.groups;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.prudent.callefy.R;
import com.prudent.callefy.database.groups.ContactGroup;
import com.prudent.callefy.database.sms_content.SmsContent;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class GroupsAdapter extends RecyclerView.Adapter<GroupsAdapter.ViewHolder>{

    private List<ContactGroup> mContactGroupList = new ArrayList<>();
    Context mContext;
    GroupsCallback mGroupsCallback;
    boolean isEdit= false;

    public GroupsAdapter(List<ContactGroup> mContactGroupList, Context mContext, GroupsCallback mGroupsCallback) {
        this.mContactGroupList = mContactGroupList;
        this.mContext = mContext;
        this.mGroupsCallback = mGroupsCallback;
    }

    public void addNewSmsContent(ContactGroup contactGroup){
        int insertIndex = mContactGroupList.size();
        mContactGroupList.add(insertIndex, contactGroup);
        notifyItemInserted(insertIndex+1);
        notifyDataSetChanged();
        mGroupsCallback.onMoveToLastPosition(insertIndex);

    }

    public void removeItem(int position){

        mContactGroupList.remove(position);
        notifyItemInserted(position);
        notifyDataSetChanged();

    }

    public void updateItem(int position, String name){

        mContactGroupList.get(position).setGroup_name(name);
        notifyItemInserted(position);
        notifyDataSetChanged();

    }

    public void editAdapter(){
        isEdit = true;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_contact_group, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ContactGroup item = mContactGroupList.get(position);
        holder.tvGroupName.setText(item.getGroup_name());
        if (isEdit){
            holder.btEdit.setVisibility(View.VISIBLE);
        }else {
            holder.btEdit.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mContactGroupList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tvGroupName)
        TextView tvGroupName;
        @BindView(R.id.rlGroup)
        RelativeLayout rlGroup;
        @BindView(R.id.btEdit)
        Button btEdit;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            rlGroup.setOnClickListener(this::onClick);
            btEdit.setOnClickListener(this::onClick);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.rlGroup:
                    mGroupsCallback.onListItemClick(getAdapterPosition(), mContactGroupList.get(getAdapterPosition()));
                    break;
                case R.id.btEdit:
                    mGroupsCallback.onEditGroup(getAdapterPosition(), mContactGroupList.get(getAdapterPosition()));
                    break;
            }

        }
    }
}
