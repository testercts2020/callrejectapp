package com.prudent.callefy.ui.login;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Selection;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.material.button.MaterialButton;
import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.database.CommonTransactionsDb;
import com.prudent.callefy.database.sms_content.SmsContent;
import com.prudent.callefy.model.LoginResponse;
import com.prudent.callefy.model.ProfileMobileInfoModel;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.utils.CommonUtils;
import com.prudent.callefy.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.btLogin)
    MaterialButton btLogin;
    @BindView(R.id.etMobileNo)
    EditText etMobileNo;
    @BindView(R.id.tvTermsAndCond)
    TextView tvTermsAndCond;

    GoogleApiClient mGoogleApiClient;

    private Observer<LoginResponse> loginResponseObserver;
    private Observer<Boolean> internetError;
    private LoginViewModel loginViewModel;
    public static Activity loginActivity;
    private CommonTransactionsDb mTransactionsDb;

    public static Intent startActivity(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        initView();
        loginActivity = this;

        if (loginViewModel == null) {
            loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
            loginViewModel.getIsLoading().observe(this, new Observer<Boolean>() {
                @Override
                public void onChanged(Boolean isLoading) {
                    if (isLoading) {
                        showProgress();
                    } else {
                        dismissProgress();
                    }
                }
            });
//
            loginResponseObserver = this::onSuccessLoginResponse;
        }


        customTextView();
    }

    public void onSuccessLoginResponse(LoginResponse response) {
        log("MESSAGE==>" + response.getMessage());

        if (mTransactionsDb == null){
            mTransactionsDb = new CommonTransactionsDb(getApplicationContext());
        }



        if (response.getStatus() == 1) {
            SharedPrefs.setString(SharedPrefs.Key.USER_ID, response.getUserInfoModel().getUserId() + "");
            if (response.getUserInfoModel().getUserName() != null) {
                SharedPrefs.setString(SharedPrefs.Key.USER_EMAIL, response.getUserInfoModel().getEmailId());
            }
            if (response.getUserInfoModel().getEmailId() != null) {
                SharedPrefs.setString(SharedPrefs.Key.USER_NAME, response.getUserInfoModel().getUserName());
            }

            ProfileMobileInfoModel mobile = response.getUserInfoModel()
                    .getMobileNoOperatorUssdcode().get(0);

            SharedPrefs.setString(SharedPrefs.Key.USSD_ACT_CODE, mobile.getUssd_code_activation());
            SharedPrefs.setString(SharedPrefs.Key.USSD_DE_ACT_CODE, mobile.getUssd_code_deactivation());
            SharedPrefs.setString(SharedPrefs.Key.MOB_NO, mobile.getMobileNo());
            SharedPrefs.setString(SharedPrefs.Key.OPERATOR, mobile.getOperator());

            mTransactionsDb.saveErrorSituation("LoginActivity", "Call Login api", "True");

            startActivity(OtpverifyActivity.startActivity(getApplicationContext()));
        }else {
            showToast(response.getMessage());
        }

    }

    @Override
    public int setLayout() {
        return R.layout.activity_login;
    }

    @Override
    public void initView() {
        requestPermissions(new String[]{
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.READ_PHONE_NUMBERS,
                Manifest.permission.READ_SMS,
                Manifest.permission.RECEIVE_SMS,
                Manifest.permission.SEND_SMS,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.READ_CALL_LOG,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.RECEIVE_BOOT_COMPLETED,
                Manifest.permission.SYSTEM_ALERT_WINDOW,
                Manifest.permission.WRITE_CALENDAR,
                Manifest.permission.READ_CALENDAR,

                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_CALL_LOG,
                Manifest.permission.MODIFY_PHONE_STATE,
                Manifest.permission.ANSWER_PHONE_CALLS

        }, 1);

//        getSimInfoUsingGoogleApiClient();
//        getDeviceInfo();

        SharedPrefs.clearAllPreferences();

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mobileNo = etMobileNo.getText().toString().trim();
                mobileNo = mobileNo.replace(" ", "");


                if (mobileNo == null || mobileNo.length() < 12) {
                    showToast("Enter valid mobile number. ");
                    return;
                }

                if (CommonUtils.isNetworkConnected(getApplicationContext())) {
                    loginViewModel.login(mobileNo,
                            Constants.TAG_API_LOGIN,
                            SharedPrefs.getString(SharedPrefs.Key.OPERATOR, ""),
                            SharedPrefs.getString(SharedPrefs.Key.IMEI, ""),
                            SharedPrefs.getString(SharedPrefs.Key.MODEL, ""),
                            SharedPrefs.getString(SharedPrefs.Key.MANUFACTURER, ""),
                            SharedPrefs.getString(SharedPrefs.Key.BRAND, ""),
                            SharedPrefs.getString(SharedPrefs.Key.SDK, ""),
                            SharedPrefs.getString(SharedPrefs.Key.DEVICE_RELEASE_NO, "")
                    ).
                            observe(LoginActivity.this, loginResponseObserver);

                } else
                    openNoInternetScreen();


            }
        });



        etMobileNo.setText("+91");
        Selection.setSelection(etMobileNo.getText(), etMobileNo.getText().length());
        etMobileNo.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("+91")){
                    etMobileNo.setText("+91");
                    Selection.setSelection(etMobileNo.getText(), etMobileNo.getText().length());

                }

            }
        });


        if (mTransactionsDb == null){
            mTransactionsDb = new CommonTransactionsDb(getApplicationContext());
        }

        SmsContent smsContent= new SmsContent();
        smsContent.setSmsContent(" I am in an important Conference call, i will call you immediately" +
                " once I am free. Download KOLiFY : bitly//");
        smsContent.setId(1);

        SmsContent smsContent2= new SmsContent();
        smsContent2.setSmsContent(" I am in an important Conference call, SMS or WhatsApp if " +
                "important. Download KOLiFY : bitly//");
        smsContent2.setId(2);

        SmsContent smsContent3= new SmsContent();
        smsContent3.setSmsContent(" Sorry, I am on another call. Download KOLiFY : bitly//");
        smsContent3.setId(3);
        mTransactionsDb.saveSmsContent(smsContent);
        mTransactionsDb.saveSmsContent(smsContent2);
        mTransactionsDb.saveSmsContent(smsContent3);


//        etMobileNo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//
//            @Override
//            public boolean onEditorAction(TextView v, int keyCode, KeyEvent event) {
//                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
//                    // hide virtual keyboard
//                    InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(etMobileNo.getWindowToken(), 0);
//                    return true;
//                }
//                return false;
//            }
//        });

    }

    @Override
    public boolean setToolbar() {
        return false;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }

    /* Start Get SIM Info  start *****/

    public void getSimInfoUsingGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Auth.CREDENTIALS_API)
                .build();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

        HintRequest hintRequest = new HintRequest.Builder().
                setPhoneNumberIdentifierSupported(true).build();

        PendingIntent pendingIntent = Auth.CredentialsApi.getHintPickerIntent(mGoogleApiClient, hintRequest);

        try {
            startIntentSenderForResult(pendingIntent.getIntentSender(), 1008,
                    null, 0, 0, 0, null);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
            log("Could not start hint picker Intent " + e);
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1008:
                if (resultCode == RESULT_OK) {
                    Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
                    log("credin ni :" + credential.getId());
                    etMobileNo.setText(credential.getId());
                }


                break;
        }

    }

    /* ****** End Get SIM Info  END *****/

    /*  Get Device info  start *****/

    public void getDeviceInfo() {
        StringBuffer infoBuffer = new StringBuffer();

        infoBuffer.append("-------------------------------------\n");
        infoBuffer.append("Model :" + Build.MODEL + "\n");//The end-user-visible name for the end product.
        infoBuffer.append("Device: " + Build.DEVICE + "\n");//The name of the industrial design.
        infoBuffer.append("Manufacturer: " + Build.MANUFACTURER + "\n");//The manufacturer of the product/hardware.
        infoBuffer.append("Board: " + Build.BOARD + "\n");//The name of the underlying board, like "goldfish".
        infoBuffer.append("Brand: " + Build.BRAND + "\n");//The consumer-visible brand with which the product/hardware will be associated, if any.
        infoBuffer.append("Serial: " + Build.SERIAL + "\n");
        infoBuffer.append("sdk : " + Build.VERSION.SDK_INT + "\n");
        infoBuffer.append("Relese : " + Build.VERSION.RELEASE + "\n");
        infoBuffer.append("-------------------------------------\n");

        log(infoBuffer.toString());

        SharedPrefs.setString(SharedPrefs.Key.MODEL, Build.MODEL);
        SharedPrefs.setString(SharedPrefs.Key.MANUFACTURER, Build.MANUFACTURER);
        SharedPrefs.setString(SharedPrefs.Key.BRAND, Build.BRAND);
        SharedPrefs.setString(SharedPrefs.Key.SDK, Build.VERSION.SDK_INT + "");
        SharedPrefs.setString(SharedPrefs.Key.DEVICE_RELEASE_NO, Build.VERSION.RELEASE);
        SharedPrefs.setString(SharedPrefs.Key.IMEI, Build.SERIAL);


        getSimInformation();

    }

    /* ****** Get Device info  END *****/

    /*Get SIM info  start *****/

    public void getSimInformation() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telephonyManager.getSimState();

        switch (simState) {

            case (TelephonyManager.SIM_STATE_ABSENT):
                break;
            case (TelephonyManager.SIM_STATE_NETWORK_LOCKED):
                break;
            case (TelephonyManager.SIM_STATE_PIN_REQUIRED):
                break;
            case (TelephonyManager.SIM_STATE_PUK_REQUIRED):
                break;
            case (TelephonyManager.SIM_STATE_UNKNOWN):
                break;
            case (TelephonyManager.SIM_STATE_READY): {

                // Get the SIM country ISO code
                String simCountry = telephonyManager.getSimCountryIso();
                log(simCountry);

                // Get the operator code of the active SIM (MCC + MNC)
                String simOperatorCode = telephonyManager.getSimOperator();
                log(simOperatorCode);

                // Get the name of the SIM operator
                String simOperatorName = telephonyManager.getSimOperatorName();
                log(simOperatorName);
                SharedPrefs.setString(SharedPrefs.Key.OPERATOR, simOperatorName);

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    String simNumber = telephonyManager.getLine1Number();
                    log(simNumber);
                    return;
                }
                String simNumber = telephonyManager.getLine1Number();
                log(simNumber);

                String simNumber2 = telephonyManager.getVoiceMailNumber();
                log(simNumber2);
                String simNumber3 = telephonyManager.getDeviceSoftwareVersion();
                log(simNumber3);


                TelephonyManager mTelephonyMgr;
                mTelephonyMgr = (TelephonyManager)
                        getSystemService(Context.TELEPHONY_SERVICE);
                mTelephonyMgr.getLine1Number();
                log(mTelephonyMgr.getLine1Number());

                getSimInfoUsingGoogleApiClient();

//                updateDeviceInfo();

//                getSimInfoUsingSubC();
                // Get the SIMâ€™s serial number
//                String simSerial = telephonyManager.getSimSerialNumber();

            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    getDeviceInfo();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
//                    Toast.makeText(Logi.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                    showToast("Permission denied !");
                    finish();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void customTextView() {
        SpannableStringBuilder spanTxt = new SpannableStringBuilder(
                "I agree to the ");
        spanTxt.append("Term of services");
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Toast.makeText(getApplicationContext(), "Terms of services Clicked",
                        Toast.LENGTH_SHORT).show();
            }
        }, spanTxt.length() - "Term of services".length(), spanTxt.length(), 0);
        spanTxt.append(" and");
        spanTxt.setSpan(new ForegroundColorSpan(Color.BLACK), 32, spanTxt.length(), 0);
        spanTxt.append(" Privacy Policy");
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Toast.makeText(getApplicationContext(), "Privacy Policy Clicked",
                        Toast.LENGTH_SHORT).show();
            }
        }, spanTxt.length() - " Privacy Policy".length(), spanTxt.length(), 0);
        tvTermsAndCond.setMovementMethod(LinkMovementMethod.getInstance());
        tvTermsAndCond.setText(spanTxt, TextView.BufferType.SPANNABLE);
    }


}