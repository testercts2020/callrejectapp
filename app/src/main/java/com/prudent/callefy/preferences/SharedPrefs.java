package com.prudent.callefy.preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefs {
    public static SharedPreferences sharedPreferences;
    public static String PREFERENCE_FILE_KEY = "com_prudent_callefy";

    public static void initializeSharedPref(Context context) {
        sharedPreferences = context.getSharedPreferences(PREFERENCE_FILE_KEY, Context.MODE_PRIVATE);
        setPreferences(SharedPrefs.sharedPreferences);

    }

    public static SharedPreferences getPreferences() {
        return sharedPreferences;
    }

    public static void setPreferences(SharedPreferences preferences) {
        SharedPrefs.sharedPreferences = preferences;
    }

    public static void clearAllPreferences() {
        SharedPrefs.sharedPreferences .edit().clear().commit();
    }

    public static void setString(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getString(String key, String def) {
        return sharedPreferences.getString(key, def);
    }

    public static String getUssdActCode() {
        return getString(Key.USSD_ACT_CODE, "*123#");
    }

    public static String getUssdDeActCode() {
        return getString(Key.USSD_DE_ACT_CODE, "*123#");
    }

    public static String getSimOperator(){
        return getString(Key.OPERATOR, "");
    }

    public static String getCallForwardingNo() {
        return getString(Key.CALL_FORWARDING_NO, "123");
    }

    public static void setInt(String key, int value){
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static int getInt(String key, int def){
        return sharedPreferences.getInt(key, def);
    }

    public static void setBoolean(String key, boolean value){
        SharedPreferences.Editor editor= sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static boolean getBoolean(String key, boolean def){
        return sharedPreferences.getBoolean(key, def);
    }

    public static boolean isLogin(){
        return getBoolean(Key.IS_LOGIN , false);
    }

    public static boolean isCallForwardingState() {
        return getBoolean(Key.IS_CALL_FORWARDING_state, false);
    }
    public static boolean isCallForwardingStateManual() {
        return getBoolean(Key.IS_CALL_FORWARDING_STATE_MANUAL, false);
    }

    public static void setIsManualDoNotDisturbActive(boolean value){
        setBoolean(Key.IS_MANUAL_DONOT_DISTURB_ACTIVE, value);

    }

    public static boolean isManualDoNotDisturbActive(){
        return getBoolean(Key.IS_MANUAL_DONOT_DISTURB_ACTIVE, false);
    }

    public static void setIsDoNotDisturbActive(boolean value){
        setBoolean(Key.IS_DONOT_DISTURB_ACTIVE, value);

    }

    public static boolean isDoNotDisturbActive(){
        return getBoolean(Key.IS_DONOT_DISTURB_ACTIVE, false);
    }

    public static void setIsGroupsDoNotDisturbActive(boolean value){
        setBoolean(Key.IS_GROUPS_DONOT_DISTURB_ACTIVE, value);

    }

    public static boolean isGroupsDoNotDisturbActive(){
        return getBoolean(Key.IS_GROUPS_DONOT_DISTURB_ACTIVE, false);
    }

    public static void setIsVoipDoNotDisturbActive(boolean value){
        setBoolean(Key.IS_VOIP_CALL_DONOT_DISTURB_ACTIVE, value);

    }

    public static boolean isVoipDoNotDisturbActive(){
        return getBoolean(Key.IS_VOIP_CALL_DONOT_DISTURB_ACTIVE, false);
    }

    public static String getUserId(){
        return getString(Key.USER_ID , "");
    }

    public static void isSetCallUssdActivation(boolean value){
        // true ==> ussd activation call;
        // false ==> ussd de activation call
        setBoolean(Key.IS_CALL_USSD_ACTIVATION, value);
    }

    public static boolean isCallUssdActivation(){
        return getBoolean(Key.IS_CALL_USSD_ACTIVATION, false);
    }




    public static class Key {
        public static final String IS_LOGIN = "is_login";
        public static final String USER_ID = "user_id";
        public static final String USER_NAME = "user_name";
        public static final String USER_EMAIL = "user_email";
        public static final String USER_PROFILE_IMAGE = "user_profile_image";
        public static final String POSITION = "position";
        public static final String USSD_ACT_CODE = "ussd_act_code";
        public static final String USSD_DE_ACT_CODE = "ussd_de_act_code";
        public static final String CALL_FORWARDING_NO = "call_forwarding_no";
        public static final String IS_CALL_FORWARDING_state = "is_call_forwarding_state";
        public static final String IS_CALL_FORWARDING_STATE_MANUAL = "is_call_forwarding_state_manual";
        public static final String CALL_FORWARDING_STATE_FAILED_COUNT = "call_forwarding_state_failed_count";
        public static final String IS_START_BACKGROUND_SERVICE = "is_start_background_service";


        public static final String DEFAULT_SMS_CONTENT = "default_sms_content";
        public static final String DEFAULT_SMS_CONTENT_DYNAMIC = "default_sms_content_dynamic";
        public static final String IS_REJECT_CALL_WITH_SMS = "is_reject_call_with_sms";
        public static final String CALL_REJECT_STATE_ID = "call_reject_state_id";  // 1==>Accept; 2==>Reject call; 3=>reject and sms
        public static final String CALL_REJECT_STATE_DESC = "call_reject_state_id_desc";  // 1==>Accept; 2==>Reject call; 3=>reject and sms

//        public static final String IS_MANUAL_DONOT_DISTURB_ACTIVE = "is_manual_donot_disturb_active";
        public static final String IS_DONOT_DISTURB_ACTIVE = "is_donot_disturb_active";
        public static final String IS_GROUPS_DONOT_DISTURB_ACTIVE = "IS_GROUPS_DONOT_DISTURB_ACTIVE"; // DO NOT DISTURB CONTACT GROUP WISE
        public static final String DO_NOT_DISTURB_WITH_TIME = "do_not_disturb_with_time";

        // donot disturb active deactive

        public static final String IS_MANUAL_DONOT_DISTURB_ACTIVE = "is_manual_donot_disturb_active";
        public static final String IS_MANUAL_DONOT_DISTURB_ACTIVE2 = "is_manual_donot_disturb_active2";
        public static final String IS_VOIP_CALL_DONOT_DISTURB_ACTIVE = "is_voip_call_donot_disturb_active";
        public static final String IS_CALENDER_EVENT_DONOT_DISTURB_ACTIVE = "is_calender_event_donot_disturb_active";

        public static final String MISSED_CALLS_DURING_DNDB = "missed_calls_during_dndb";


        public static final String IS_CHECK_USSD_ACTIVATION = "is_check_ussd_activation";
        public static final String IS_CHECK_USSD_DE_ACTIVATION = "is_check_ussd_de_activation";
        public static final String IS_CURRENT_NOW_CHECK_USSD_ACT_DE_ACTIVATION = "IS_CURRENT_NOW_CHECK_USSD_ACT_DE_ACTIVATION";
        public static final String NUMBER_OF_CHECK_USSD_CODE_ACT_DEACT = "number_of_check_ussd_code_act_deact";
        public static final String IS_CALL_USSD_ACTIVATION = "is_call_ussd_activation"; // Check current ussd call act or deact
        public static final String DASHBOARD_CURRENT_STATUS = "dashboard_current_status"; // Check current ussd call act or deact

        public static final String NUMBER_OF_BACKGROUND_THREAD_RUNNING_COUNT = "number_of_background_thread_running_count";
        public static final String REMOVE_UNEXPECTED_BACKGROUND_THREAD = "remove_unexpected_background_thread";

        public static final String VOIP_CALL_STARTING_TIME = "voip_call_starting_time";
        public static final String NOTIFICATION_CALLS_LAST_DATE = "notification_calls_last_date";

        // os device info
        public static final String IS_UPDATE_DEVICE_INFO = "is_update_device_info";
        public static final String MOB_NO = "mob_no";
        public static final String OPERATOR = "operator";
        public static final String IMEI = "imei";
        public static final String MODEL = "model";
        public static final String MANUFACTURER = "manufacture";
        public static final String BRAND = "brand";
        public static final String SDK = "sdk";
        public static final String DEVICE_RELEASE_NO = "device_release_no";
        public static final String OPERATOR_BACKGROUND_RUNNIG_STATUS = "operator_background_runnig_status";

        // calender event schedule
        public static final String CALENDER_EVENT_START_DATE = "calender_event_start_date";
        public static final String CALENDER_EVENT_END_DATE = "calender_event_end_date";
        public static final String CALENDER_EVENT_CURRENT_DATE = "calender_event_current_date";
        public static final String CALENDER_EVENT_ID_START_BEFORE_5_MIN = "calender_event_id_start_before_5_min";
        public static final String CALENDER_EVENT_ID = "calender_event_id";
        public static final String IS_CALENDER_EVENT_START = "is_calender_event_start";
        public static final String IS_CALENDER_EVENT_START_ACTIVATION = "is_calender_event_start_activation";// after calender event start
        public static final String IS_CHECK_CALENDER_EVENT_ACTIVATION = "is_check_calender_event_activation";// after calender event start


    }

}
