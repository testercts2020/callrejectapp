package com.prudent.callefy;

import androidx.multidex.MultiDexApplication;

import com.prudent.callefy.preferences.SharedPrefs;

public class App extends MultiDexApplication {
    public static final String TAG = App.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPrefs.initializeSharedPref(this);
    }
}
