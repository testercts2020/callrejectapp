package com.prudent.callefy.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class SmsReceiver extends BroadcastReceiver {
    private static SmsListener mListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle data = intent.getExtras();
        Object[] pdus =(Object[]) data.get("pdus");

        for (int i= 0; i< pdus.length; i++){
            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
            String sender = smsMessage.getDisplayOriginatingAddress();
            // check sender is my

            String smsBody = smsMessage.getMessageBody();

            // pass on text to our listener
            if(sender.contains("VM-BUZTES")){
                if (mListener != null)
                mListener.messageReceived(smsBody +"  "+ sender);
            }

        }

    }

    public static void bindListener(SmsListener smsListener){
        mListener = smsListener;
    }

}
