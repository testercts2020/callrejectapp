package com.prudent.callefy.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.prudent.callefy.model.CalenderModel;
import com.prudent.callefy.preferences.SharedPrefs;

import java.util.ArrayList;
import java.util.Calendar;

public class ReedCalender {
    public static ArrayList<CalenderModel> calenderList = new ArrayList<CalenderModel>();

    public static ArrayList<CalenderModel> readCalendarEventDateRange(Context context) {
        Calendar c_start = Calendar.getInstance();
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int month = Calendar.getInstance().get(Calendar.MONTH);
        int date = Calendar.getInstance().get(Calendar.DATE);
        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int minute = Calendar.getInstance().get(Calendar.MINUTE);
        c_start.set(year, month, date, hour, minute); //Note that months start from 0 (January)
        Calendar c_end = Calendar.getInstance();
        c_end.set(year, month, date + 1, 0, 0); //Note that months start from 0 (January)

        String selection = "((dtstart >= " + c_start.getTimeInMillis() + ") AND (dtend <= " + c_end.getTimeInMillis() + "))";

        Cursor cursor = context.getContentResolver()
                .query(
                        Uri.parse("content://com.android.calendar/events"),
                        new String[]{"calendar_id", "title", "description",
                                "dtstart", "dtend", "eventLocation"}, selection,
                        null, null);

        cursor.moveToFirst();
        // fetching calendars name
        String CNames[] = new String[cursor.getCount()];
        calenderList.clear();

        for (int i = 0; i < CNames.length; i++) {

//            logMessage("Evant "+i+"  "+cursor.getString(1));
            logMessage("startDates " + i + "  " + cursor.getString(3));
//            logMessage("endDates "+i+"  "+cursor.getString(4));
//            logMessage("descriptions "+i+"  "+cursor.getString(2));
//
//            logMessage("+++++++__________++++++++++++");
            String event;
            String description;
            String startDate;
            String endDate;
            String eventId;
            eventId = cursor.getString(0);
            event = cursor.getString(1);
            description = cursor.getString(2);
            startDate = cursor.getString(3);
            startDate = CommonUtils.getDateStringFromDouble(startDate);
            endDate = cursor.getString(4);
            endDate = CommonUtils.getDateStringFromDouble(endDate);

            if (description != null && !description.trim().equals("")) {
                boolean isCurrentEvent = false;
                calenderList.add(new CalenderModel(eventId, event, description, startDate, endDate, isCurrentEvent));
            }

            CNames[i] = cursor.getString(1);
            cursor.moveToNext();
        }
        return calenderList;

    }

    public static ArrayList<CalenderModel> readCalendarEventDateRangeCurrentTime(Context context) {
        Calendar c_start = Calendar.getInstance();

        String currentEventStartDate = SharedPrefs.getString(SharedPrefs.Key.CALENDER_EVENT_START_DATE, "");
        if (currentEventStartDate.equals("")) {
            currentEventStartDate = CommonUtils.getTodayDateWithTime();
            SharedPrefs.setString(SharedPrefs.Key.CALENDER_EVENT_START_DATE, currentEventStartDate);
        }

        int year = CommonUtils.getIntYearFromDate(currentEventStartDate, Calendar.YEAR);
        int month = CommonUtils.getIntYearFromDate(currentEventStartDate, Calendar.MONTH);
        int date = CommonUtils.getIntYearFromDate(currentEventStartDate, Calendar.DATE);
        int hour = CommonUtils.getIntYearFromDate(currentEventStartDate, Calendar.HOUR_OF_DAY);
        int minute = CommonUtils.getIntYearFromDate(currentEventStartDate, Calendar.MINUTE);

        c_start.set(year, month, date, hour, minute); //Note that months start from 0 (January)
        Calendar c_end = Calendar.getInstance();
        c_end.set(year, month, date + 1, 0, 0); //Note that months start from 0 (January)

        String selection = "((dtstart >= " + c_start.getTimeInMillis() + ") AND (dtend <= " + c_end.getTimeInMillis() + "))";

        Cursor cursor = context.getContentResolver()
                .query(
                        Uri.parse("content://com.android.calendar/events"),
                        new String[]{"calendar_id", "title", "description",
                                "dtstart", "dtend", "eventLocation"}, selection,
                        null, null);

        cursor.moveToFirst();
        // fetching calendars name
        String CNames[] = new String[cursor.getCount()];
        calenderList.clear();

        for (int i = 0; i < CNames.length; i++) {
//            logMessage("+++++++__________++++++++++++");
            String event;
            String description;
            String startDate;
            String endDate;
            String eventId;
            eventId = cursor.getString(0);
            event = cursor.getString(1);
            description = cursor.getString(2);
            startDate = cursor.getString(3);
            startDate = CommonUtils.getDateStringFromDouble(startDate);
            endDate = cursor.getString(4);
//            String baseEndDate = CommonUtils.getBaseDateStringFromDouble(endDate);
            endDate = CommonUtils.getDateStringFromDouble(endDate);

            if (i == 0){
                long startDateLong = cursor.getLong(3);
//                CALENDER_EVENT_START_DATE
                String currentDate =  SharedPrefs.getString(SharedPrefs.Key.CALENDER_EVENT_START_DATE, "");

                long currentDateLong= CommonUtils.getMillisecondFromDateTimeSecond(currentDate);

                if (startDateLong < currentDateLong){
                    long endDateLong = cursor.getLong(4);

                    if (endDateLong < currentDateLong){
                        String baseEndDate = CommonUtils.getBaseDateStringFromDouble(endDateLong+"");
                        SharedPrefs.setString(SharedPrefs.Key.CALENDER_EVENT_START_DATE, baseEndDate);
                    }

                }else {
                    long currentDateNowLong = CommonUtils.getCurrentMilliSecondDate();
                    if (currentDateNowLong > currentDateLong){
                        String baseStartDate = CommonUtils.getBaseDateStringFromDouble(startDateLong+"");
                        SharedPrefs.setString(SharedPrefs.Key.CALENDER_EVENT_START_DATE, baseStartDate);
                    }

                }

//                SharedPrefs.setString(SharedPrefs.Key.CALENDER_EVENT_ID , eventId);
//                SharedPrefs.setString(SharedPrefs.Key.CALENDER_EVENT_END_DATE , baseEndDate);

            }


            if (description != null && !description.trim().equals("")) {
                boolean isCurrentEvent = false;
                calenderList.add(new CalenderModel(eventId, event, description, startDate, endDate, isCurrentEvent));
            }

            CNames[i] = cursor.getString(1);
            cursor.moveToNext();
        }
        return calenderList;

    }


    public static ArrayList<CalenderModel> readCalendarEventDateRangeToday(Context context) {
        Calendar c_start = Calendar.getInstance();

//        String currentEventStartDate = SharedPrefs.getString(SharedPrefs.Key.CALENDER_EVENT_START_DATE, "");
//        if (currentEventStartDate.equals("")) {
//            currentEventStartDate = CommonUtils.getTodayDateWithTime();
//            SharedPrefs.setString(SharedPrefs.Key.CALENDER_EVENT_START_DATE, currentEventStartDate);
//        }
        String todayDate = CommonUtils.getTodayDateWithTime();

        int year = CommonUtils.getIntYearFromDate(todayDate, Calendar.YEAR);
        int month = CommonUtils.getIntYearFromDate(todayDate, Calendar.MONTH);
        int date = CommonUtils.getIntYearFromDate(todayDate, Calendar.DATE);
        int hour = CommonUtils.getIntYearFromDate(todayDate, Calendar.HOUR_OF_DAY);
        int minute = CommonUtils.getIntYearFromDate(todayDate, Calendar.MINUTE);

        long currentDateNow = CommonUtils.getCurrentMilliSecondDate();

        c_start.set(year, month, date, 0, 0); //Note that months start from 0 (January)
        Calendar c_end = Calendar.getInstance();
        c_end.set(year, month, date + 1, 0, 0); //Note that months start from 0 (January)

        String selection = "((dtstart >= " + c_start.getTimeInMillis() + ") AND (dtend <= " + c_end.getTimeInMillis() + "))";

        Cursor cursor = context.getContentResolver()
                .query(
                        Uri.parse("content://com.android.calendar/events"),
                        new String[]{"calendar_id", "title", "description",
                                "dtstart", "dtend", "eventLocation"}, selection,
                        null, null);

        cursor.moveToFirst();
        // fetching calendars name
        String CNames[] = new String[cursor.getCount()];
        calenderList.clear();

        for (int i = 0; i < CNames.length; i++) {
//            logMessage("+++++++__________++++++++++++");
            String event;
            String description;
            String startDate;
            String endDate;
            String eventId;
            eventId = cursor.getString(0);
            event = cursor.getString(1);
            description = cursor.getString(2);
            startDate = cursor.getString(3);
            startDate = CommonUtils.getDateStringFromDouble(startDate);
            endDate = cursor.getString(4);
//            String baseEndDate = CommonUtils.getBaseDateStringFromDouble(endDate);
            endDate = CommonUtils.getDateStringFromDouble(endDate);

           long startDateLong = cursor.getLong(3);
           long endDateLong = cursor.getLong(4);

           if (endDateLong > currentDateNow){

               boolean isCurrentEvent = false;
               if (startDateLong < currentDateNow)
                   isCurrentEvent = true;

               if (description != null && !description.trim().equals("")) {
                   calenderList.add(new CalenderModel(eventId, event, description, startDate, endDate, isCurrentEvent));
               }

           }




            CNames[i] = cursor.getString(1);
            cursor.moveToNext();
        }
        return calenderList;

    }

    public static ArrayList<CalenderModel> readWebCalendarEventDateRangeToday(Context context) {
        Calendar c_start = Calendar.getInstance();

//        String currentEventStartDate = SharedPrefs.getString(SharedPrefs.Key.CALENDER_EVENT_START_DATE, "");
//        if (currentEventStartDate.equals("")) {
//            currentEventStartDate = CommonUtils.getTodayDateWithTime();
//            SharedPrefs.setString(SharedPrefs.Key.CALENDER_EVENT_START_DATE, currentEventStartDate);
//        }
        String todayDate = CommonUtils.getTodayDateWithTime();

        int year = CommonUtils.getIntYearFromDate(todayDate, Calendar.YEAR);
        int month = CommonUtils.getIntYearFromDate(todayDate, Calendar.MONTH);
        int date = CommonUtils.getIntYearFromDate(todayDate, Calendar.DATE);
        int hour = CommonUtils.getIntYearFromDate(todayDate, Calendar.HOUR_OF_DAY);
        int minute = CommonUtils.getIntYearFromDate(todayDate, Calendar.MINUTE);

        long currentDateNow = CommonUtils.getCurrentMilliSecondDate();

        c_start.set(year, month, date, 0, 0); //Note that months start from 0 (January)
        Calendar c_end = Calendar.getInstance();
        c_end.set(year, month, date + 5, 0, 0); //Note that months start from 0 (January)

        String selection = "((dtstart >= " + c_start.getTimeInMillis() + ") AND (dtend <= " + c_end.getTimeInMillis() + "))";

        Cursor cursor = context.getContentResolver()
                .query(
                        Uri.parse("content://com.android.calendar/events"),
                        new String[]{"calendar_id", "title", "description",
                                "dtstart", "dtend", "eventLocation" , "_id"}, selection,
                        null, null);

        cursor.moveToFirst();
        // fetching calendars name
        String CNames[] = new String[cursor.getCount()];
        calenderList.clear();

        for (int i = 0; i < CNames.length; i++) {
//            logMessage("+++++++__________++++++++++++");
            String event;
            String description;
            String startDate;
            String endDate;
            String eventId;
            eventId = cursor.getString(6);
            Log.d("TAG", "readWebCalendarEventDateRangeToday: eventId = "+eventId);
            event = cursor.getString(1);
            description = cursor.getString(2);
            startDate = cursor.getString(3);
            startDate = CommonUtils.getDateStringFromDouble(startDate);
            endDate = cursor.getString(4);
//            String baseEndDate = CommonUtils.getBaseDateStringFromDouble(endDate);
            endDate = CommonUtils.getDateStringFromDouble(endDate);

            long startDateLong = cursor.getLong(3);
            long endDateLong = cursor.getLong(4);

            if (endDateLong > currentDateNow){

                boolean isCurrentEvent = false;
//                if (startDateLong < currentDateNow)
//                    isCurrentEvent = true;

                if (description != null && !description.trim().equals("")) {
                    calenderList.add(new CalenderModel(eventId, event, description, startDate, endDate, isCurrentEvent));
                }

            }




            CNames[i] = cursor.getString(1);
            cursor.moveToNext();
        }
        return calenderList;

    }


    public static void logMessage(String message) {
        Log.d("TAG==>", "logd: " + message);
    }
}
