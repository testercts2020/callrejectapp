package com.prudent.callefy.utils;

public interface InternetConnectionListener {
    void onInternetUnavailable();

    void onCacheUnavailable();
}
