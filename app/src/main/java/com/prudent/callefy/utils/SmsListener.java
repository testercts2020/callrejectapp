package com.prudent.callefy.utils;

public interface SmsListener {
    public void messageReceived(String messageText);
}
