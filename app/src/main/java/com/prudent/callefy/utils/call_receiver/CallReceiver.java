package com.prudent.callefy.utils.call_receiver;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telecom.TelecomManager;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;


import androidx.core.app.ActivityCompat;

import com.android.internal.telephony.ITelephony;
import com.prudent.callefy.database.CommonTransactionsDb;
import com.prudent.callefy.database.calender_event.CalenderEvent;
import com.prudent.callefy.database.groups.ContactGroup;
import com.prudent.callefy.database.groups.group_item.ContGroupItem;
import com.prudent.callefy.database.missed_calls.MissedCalls;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.utils.CommonUtils;

import java.lang.reflect.Method;
import java.util.List;

public class CallReceiver extends BroadcastReceiver implements CommonTransactionsDb.CommonTransGroupItemDbCallBack,
        CommonTransactionsDb.CommonTransCalenderEventDbCallBack, CommonTransactionsDb.CommonTransMissedCallsDbCallsBack {

    private static final String TAG = "NoPhoneSpam";

    private static final int NOTIFY_REJECTED = 0;
    private static boolean AlreadyOnCall = false;
    //    Database database;
//    ArrayList<ListData> listData;
    String incomingNumber;

    private CommonTransactionsDb mTransactionsDb;

    Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive: 111");
        incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

        mContext = context;
        if (incomingNumber == null)
            return;

        else {

            incomingNumber = incomingNumber.replace(" ", "");
            if (incomingNumber.length() > 10)
                incomingNumber = incomingNumber.substring(incomingNumber.length() - 10);
            Log.d(TAG, "onReceive: " + incomingNumber);
//            breakCall(mContext);

//            boolean isDoNotAct = false;

//            boolean isDNM = SharedPrefs.isManualDoNotDisturbActive();
//            boolean isDN = SharedPrefs.isDoNotDisturbActive();
//            boolean isDNG = SharedPrefs.isGroupsDoNotDisturbActive();


//            if (isDNM || isDN|| isDNG){
//                Log.d(TAG, "onReceive:  nnnnnnnnnn");
//                isDoNotAct = true;
//            }

//            if (isDoNotAct){
//                String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
//                if(stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)){
//                    if (mTransactionsDb == null) {
//                        mTransactionsDb = new CommonTransactionsDb(context, this);
//                    }
//
//                    mTransactionsDb.getContactGroupsNoByInComing(incomingNumber);
//                }
//            }


            boolean isVoipDND = SharedPrefs.isVoipDoNotDisturbActive();
            boolean isManualDND = SharedPrefs.isManualDoNotDisturbActive();
            boolean isItemEventDNDActive;

            if (isVoipDND || isManualDND) {
                String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
                if (stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    if (mTransactionsDb == null) {
                        mTransactionsDb = new CommonTransactionsDb(context, this, this, this);
                    }

                    mTransactionsDb.getContactGroupsNoByInComing(incomingNumber);
                }
            } else { // check any calender event

                String stateStr = intent.getExtras().getString(TelephonyManager.EXTRA_STATE);
                if (stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    if (mTransactionsDb == null) {
                        mTransactionsDb = new CommonTransactionsDb(context, this, this, this);
                    }

                    mTransactionsDb.getAllActivatedCalenderEventList();

                }


            }

        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private void breakCall(Context context, boolean isSendSms, String smsContent, boolean isFromGp, String gpName) {
        Log.d(TAG, "breakCall: ");
        int messedCall = SharedPrefs.getInt(SharedPrefs.Key.MISSED_CALLS_DURING_DNDB, 0);
        SharedPrefs.setInt(SharedPrefs.Key.MISSED_CALLS_DURING_DNDB, messedCall + 1);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            TelecomManager telecomManager = (TelecomManager) context.getSystemService(Context.TELECOM_SERVICE);

            try {
                Log.d(TAG, "Invoked 'endCall' on TelecomManager");
                if (incomingNumber != null) {
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ANSWER_PHONE_CALLS) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    telecomManager.endCall();
                }
            }catch (Exception e) {
                Log.e(TAG, "Couldn't end call with TelecomManager", e);

            }
        }else {
            TelephonyManager telephony = (TelephonyManager)
                    context.getSystemService(Context.TELEPHONY_SERVICE);

            try {
                Class c = Class.forName(telephony.getClass().getName());
                Method m = c.getDeclaredMethod("getITelephony");
                m.setAccessible(true);
                ITelephony telephonyService = (ITelephony) m.invoke(telephony);
                telephonyService.endCall();
            }catch (Exception e) {
                e.printStackTrace();
            }
        }

        String date_time = CommonUtils.getTodayDateWithTime();
//        incomingNumber;
        String nameOfNumber = CommonUtils.getContactName(incomingNumber, context);
        String icon_text = "#";
        if (nameOfNumber != null && !nameOfNumber.trim().equals("")){
            icon_text = nameOfNumber.substring(0,1);
        }


//        mTransactionsDb.addMissedCallsItem(new MissedCalls(date_time, incomingNumber, nameOfNumber,
//                icon_text, isSendSms, smsContent));

        String callRejState = "";
        if (isFromGp)
            callRejState = "Group";
        else
            callRejState = "Default";

        String callRejStateStatus = "";
        if (isSendSms)
            callRejStateStatus = "Call reject with sms";
        else
            callRejStateStatus = "Call reject";


        MissedCalls missedCalls = new MissedCalls(date_time, incomingNumber, nameOfNumber, icon_text, isSendSms, smsContent,
                callRejState, callRejStateStatus, gpName ,true , 0);
        mTransactionsDb.addMissedCallsItem(missedCalls);

    }

    @Override
    public void getGroupItemDbList(List<ContGroupItem> item) {
        Log.d(TAG, "getGroupItemDbList: 1111111111");

        if (item == null || item.size()  < 1){
            Log.d(TAG, "getGroupItemDbList: 2222222");

            int call_state = SharedPrefs.getInt(SharedPrefs.Key.CALL_REJECT_STATE_ID, 1);

            if (call_state == 1) {
                // call accept
                Log.d(TAG, "getGroupItemDbList: 2222222  1111");
            } else if (call_state == 2) {
                Log.d(TAG, "getGroupItemDbList: 2222222  222222222");
                breakCall(mContext , false , "", false, "");
            } else if (call_state == 3) {
                Log.d(TAG, "getGroupItemDbList: 2222222  33333333");

                String defaultMessage = SharedPrefs.getString(SharedPrefs.Key.DEFAULT_SMS_CONTENT,"");
                breakCall(mContext , true, defaultMessage, false, "");
                sendSmsForResponse(incomingNumber, defaultMessage);
            }



            /*int call_state = SharedPrefs.getInt(SharedPrefs.Key.CALL_REJECT_STATE_ID, 1);

            if (call_state == 1) {
                // normal state
            } else {
                Log.d(TAG, "getGroupItemDbList: 22222 33333++++");

                boolean isDNM = SharedPrefs.isManualDoNotDisturbActive();
                boolean isDN = SharedPrefs.isDoNotDisturbActive();

                if (isDNM || isDN){
                    Log.d(TAG, "getGroupItemDbList: 22222 44444++++");
                    breakCall(mContext);
                    if (call_state == 3) {
                        String defaultMessage = SharedPrefs.getString(SharedPrefs.Key.DEFAULT_SMS_CONTENT,"");

                        sendSmsForResponse(incomingNumber, defaultMessage);
                    }
                }
            }*/

        }else {
            Log.d(TAG, "getGroupItemDbList: 3333333");

            int call_state = item.get(0).getCall_action_id();

            if (call_state == 1) {
                // call accept
                Log.d(TAG, "getGroupItemDbList: 3333333  1111");
            } else if (call_state == 2) {
                Log.d(TAG, "getGroupItemDbList: 3333333  222222222");
                breakCall(mContext , false, "", true, item.get(0).getGroup_name());
            } else if (call_state == 3) {
                Log.d(TAG, "getGroupItemDbList: 3333333  33333333");

                String defaultMessage = item.get(0).getSms_content();
                breakCall(mContext , true, defaultMessage, true, item.get(0).getGroup_name());
                Log.d(TAG, "getGroupItemDbList: 3333333  33333333"+item.get(0).getSms_content());
                sendSmsForResponse(incomingNumber, defaultMessage);
            }




//            Log.d(TAG, "getGroupItemDbList: 3333333"+item.size());
//            Log.d(TAG, "getGroupItemDbList: 3333333"+item.toString());
//            Log.d(TAG, "getGroupItemDbList: 3333333"+item.get(0).getContact_name());
//            Log.d(TAG, "getGroupItemDbList: 3333333");
//            Log.d(TAG, "getGroupItemDbList: 3333333");
            /*if (item.get(0).getCall_action_id()==1){
                // normal state
            }else {


                boolean isDoNotAct = false;

                boolean isDNM = SharedPrefs.isManualDoNotDisturbActive();
                boolean isDN = SharedPrefs.isDoNotDisturbActive();
                boolean isDNG = SharedPrefs.isGroupsDoNotDisturbActive();

                if (isDNM || isDN|| isDNG){
                    isDoNotAct = true;
                }

                if (isDoNotAct){
                    breakCall(mContext);
                    if (item.get(0).getCall_action_id()==3){
                        String message = item.get(0).getSms_content();
                        sendSmsForResponse(item.get(0).getContact_no(), message);

                    }
                }



            }*/
        }
    }

    @Override
    public void getLastAddedGroupItemDbList(List<ContGroupItem> item) {

    }

    @Override
    public void onGetGroupDbItem(List<ContactGroup> item) {

    }

    @Override
    public void onSaveGroupItemDbList(boolean isSaved) {

    }

    @Override
    public void onDeleteGroupItemDbList(boolean isSaved) {

    }

    @Override
    public void onCheckDoNotDistActiveDb(boolean isActive) {

    }

    private void sendSmsForResponse(String phoneNo, String message){
        Log.d(TAG, "sendSmsForResponse: ");
        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, message, null, null);
            Toast.makeText(mContext, "Message Sent",
                    Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            Toast.makeText(mContext,ex.getMessage().toString(),
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }

    @Override
    public void getCalenderEventDbList(List<CalenderEvent> item) {
        if (item != null && item.size()> 0){
            for (int i = 0; i < item.size() ; i++){
                long startDate = CommonUtils.getMillisecondFromDateTime(item.get(i).getStart_date());
                long endDate = CommonUtils.getMillisecondFromDateTime(item.get(i).getEnd_date());
                long currentDate = CommonUtils.getCurrentMilliSecondDate();

                if (startDate < currentDate ){
                    if (currentDate < endDate){
                        boolean isItemEventDNDActive = true;

                        mTransactionsDb.getContactGroupsNoByInComing(incomingNumber);

                    }
                }


            }
        }

    }

    @Override
    public void onIsAddOrUpdateDbEvent(boolean isUpdate) {

    }

    @Override
    public void onUpdateDbEvent(boolean isUpdate) {

    }

    @Override
    public void onGetAllMissedCalls(List<MissedCalls> list) {

    }

    @Override
    public void onGetAllMissedCallsNotUploaded(List<MissedCalls> list) {

    }

    @Override
    public void onInsertMissedCalls(boolean isAdded) {
        Log.d(TAG, "onInsertMissedCalls: "+isAdded);

    }
}
