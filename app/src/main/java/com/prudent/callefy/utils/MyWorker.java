package com.prudent.callefy.utils;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.prudent.callefy.R;

public class MyWorker extends Worker {

    //a public static string that will be used as the key
    //for sending and receiving data
    public static final String TASK_DESC = "task_desc";


    public MyWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        //getting the input data
        String taskDesc = getInputData().getString(TASK_DESC);
        Log.d("TAG==>", "doWork: ");

        //setting output data
        Data data = new Data.Builder()
                .putString(TASK_DESC, "The conclusion of the task")
                .build();

//        setOutputData(data);
        displayNotification("My Worker", taskDesc);
        return Result.success();
    }

    private void displayNotification(String title, String task) {
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("simplifiedcoding",
                    "simplifiedcoding", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        getApplicationContext();


        Intent intentStart = new Intent(getApplicationContext(), ForegroundService.class);
        intentStart.setAction(ForegroundService.ACTION_START_FOREGROUND_SERVICE);
         getApplicationContext().startForegroundService(intentStart);


//        NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext(), "simplifiedcoding")
//                .setContentTitle(title)
//                .setContentText(task)
//                .setPriority(NotificationManager.IMPORTANCE_MIN).
//                setCategory(Notification.CATEGORY_SERVICE)
//                .setSmallIcon(R.mipmap.ic_launcher);
//
//        notificationManager.notify(1, notification.build());
    }

//    @RequiresApi(Build.VERSION_CODES.O)
//    private void createNotificationChannel(String channelId, String channelName) {
////        Intent resultIntent = new Intent(this, MainActivity.class);
//        // create the taskStckBuilder and add the intent which inflate the back stack
////        TaskStackBuilder stackBuilder =
////                TaskStackBuilder.create(this);
////        stackBuilder.addNextIntentWithParentStack(resultIntent);
////        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
////                PendingIntent.FLAG_UPDATE_CURRENT);
////        NotificationChannel chan = new NotificationChannel(channelId, channelName,
////                NotificationManager.IMPORTANCE_DEFAULT);
////        chan.setLightColor(Color.BLUE);
//
////        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
////        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
////        assert manager != null;
////        manager.createNotificationChannel(chan);
//
//        // add play button intent in notification
////        Intent playIntent = new Intent(this, MyWorker.class);
////        playIntent.setAction(ACTION_PLAY);
////        playIntent.putExtra(EXTRA_NOTIFICATION_ID, 0);
////        PendingIntent pendingPlayIntent = PendingIntent.getService(this, 0, playIntent, 0);
//
//        // add count button intent in notification
////        Intent countIntent = new Intent(this, ForegroundService.class);
////        countIntent.setAction(ACTION_COUNT);
////        countIntent.putExtra(EXTRA_NOTIFICATION_ID, 0);
////        PendingIntent pendingCountIntent = PendingIntent.getService(this, 0, countIntent, 0);
////        NotificationCompat.Action countAction = new NotificationCompat.Action(android.R.drawable.ic_menu_more
////                , "count", pendingCountIntent);
//
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
//        Notification notification = notificationBuilder.setOngoing(true).
//                setSmallIcon(R.drawable.ic_launcher_background).
//                setContentTitle("App is running in background").
//                setPriority(NotificationManager.IMPORTANCE_MIN).
//                setCategory(Notification.CATEGORY_SERVICE).
//                setContentIntent(resultPendingIntent).
//                addAction(android.R.drawable.ic_media_pause, "Stop", pendingPlayIntent).
//                addAction(android.R.drawable.ic_menu_more, "count", pendingCountIntent).
//                build();
//        NotificationManagerCompat notificationManagerCompat =
//                NotificationManagerCompat.from(this);
//        notificationManagerCompat.notify(1, notificationBuilder.build());
//        startForeground(1, notification);
//
//
//    }
}
