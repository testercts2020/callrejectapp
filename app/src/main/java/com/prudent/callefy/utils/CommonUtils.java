package com.prudent.callefy.utils;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.prudent.callefy.dialog_fragment.ussd_code_issue.UssdCodeNotWorkingDialogFragment;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public final class CommonUtils {
    private static final String TAG = "CommonUtils";
    private static final boolean DEBUG = false;

    public static SimpleDateFormat dateYearAndTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
    public static SimpleDateFormat dateYearFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

    public static SimpleDateFormat dateYearAndTimeHMAFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm aa", Locale.US);
    public static SimpleDateFormat timeFormatHMA = new SimpleDateFormat("hh:mm aa", Locale.US);


    public static String getTodayDate(){
        Date newDate = Calendar.getInstance().getTime();
        return dateYearFormat.format(newDate);
    }

    public static String getTodayDateWithTime(){
        Date newDate = Calendar.getInstance().getTime();
        return dateYearAndTimeFormat.format(newDate);
    }


    public static long getCurrentMilliSecondDate(){
        Date newDate = Calendar.getInstance().getTime();
        return newDate.getTime();
    }

    public static long getCurrentMilliSecondDateForward5Min(){
//        Date newDate = Calendar.getInstance().getTime();
//        return newDate.getTime();


        Calendar cal = Calendar.getInstance();
// remove next line if you're always using the current time.
//        cal.setTime(currentDate);
        cal.setTime(new Date());
        cal.add(Calendar.MINUTE, +5);
        Date oneHourForward = cal.getTime();

        Log.d(TAG, "getCurrentMilliSecondDateForward5Min: "+dateYearAndTimeHMAFormat.format(oneHourForward));
        
        return oneHourForward.getTime();
        
    }

    public static long getMillisecond(String date){
        // from yyyy-MM-dd
        //return 1355270400000
        if (TextUtils.isEmpty(date))
            return 0;
        Date newDate = null;
        try {
            newDate = dateYearFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newDate.getTime();

    }

    public static long getMillisecondFromDateTime(String date){
        // from 2020-11-13 02:00 PM
        //return 1355270400000
        if (TextUtils.isEmpty(date))
            return 0;
        Date newDate = null;
        try {
            newDate = dateYearAndTimeHMAFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newDate.getTime();

    }

    public static long getMillisecondFromDateTimeSecond(String date){
        // from 2020-11-13 02:00:00
        //return 1355270400000
        if (TextUtils.isEmpty(date))
            return 0;
        Date newDate = null;
        try {
            newDate = dateYearAndTimeFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newDate.getTime();

    }

    public static String getTimeFormatDateString(String date) {
        // from 2019-10-24 11:10:10
        // return 12: 20 AM
        if (TextUtils.isEmpty(date))
            return null;
        Date newDate = null;
        try {
            newDate = dateYearAndTimeFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeFormatHMA.format(newDate);
    }

    public static String getDateTimeString(Date callDayTime) {
        // from 2019-10-24 11:10:10
        // return 2019-10-24 11:10:10
//        if (TextUtils.isEmpty(date))
//            return null;
//        Date newDate = null;
//        try {
//            newDate = dateYearAndTimeFormat.parse(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        return dateYearAndTimeFormat.format(callDayTime);
    }

    public static String getDateString(Date callDayTime) {
        return dateYearFormat.format(callDayTime);
    }

    public static String getDateStringFromDouble(String callDayTime) {

        long myLong = Long.parseLong(callDayTime);

        Date itemDate = new Date(myLong);
        return dateYearAndTimeHMAFormat.format(itemDate);
    }

    public static String getBaseDateStringFromDouble(String callDayTime) {

        long myLong = Long.parseLong(callDayTime);

        Date itemDate = new Date(myLong);
        return dateYearAndTimeFormat.format(itemDate);
    }

    public static int getIntYearFromDate(String dateStr, int intParams){
        int result = -1;

        Date date = null;
        try {
            date = dateYearAndTimeFormat.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null){
            Calendar calender = Calendar.getInstance();
            calender.setTime(date);
            result = calender.get(intParams);

        }

        return result;
    }

    public static boolean checkSameDay(String date1, String date2){
        boolean isSameDay = true;

        Date newDate = null;
        try {
            newDate = dateYearFormat.parse(date1);
            date1 = dateYearFormat.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        try {
            newDate = dateYearFormat.parse(date2);
            date2 = dateYearFormat.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date1.equals(date2))
            return true;
        else
            return false;



    }


    public static void showCommonMessageDialog(FragmentActivity activity, Fragment fragment, String tag, String message, String txtCancel, String txtAllow) {
        UssdCodeNotWorkingDialogFragment newFragment = UssdCodeNotWorkingDialogFragment.newInstance(message,txtCancel, txtAllow);
        newFragment.setTargetFragment(fragment, 1004);
        if (activity == null){
            newFragment.show(fragment.getFragmentManager(), tag);
        }else
            newFragment.show(activity.getSupportFragmentManager(), tag);
    }

    public static void showExpireMessageDialog(FragmentActivity activity, Fragment fragment, String tag, String message, String txtCancel, String txtAllow) {
        UssdCodeNotWorkingDialogFragment newFragment = UssdCodeNotWorkingDialogFragment.newInstance(message,txtCancel, txtAllow);
        newFragment.setTargetFragment(fragment, 1004);
        if (activity == null){
            newFragment.show(fragment.getFragmentManager(), tag);
        }else
            newFragment.show(activity.getSupportFragmentManager(), tag);
    }


    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    ////////  image path  //////////

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     * @author paulburke
     */
    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     * @author paulburke
     */
    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     * @author paulburke
     */
    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    //-------

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    public static String getImageRealPath(Context inContext, Uri uri) {
        String imagePath;
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = inContext.getContentResolver().query(
                uri,
                filePathColumn,
                null,
                null,
                null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        imagePath = cursor.getString(columnIndex);
        cursor.close();
        return imagePath;
    }

    public static String getRealPathFromURI(Context inContext, Uri uri) {
        String path = "";
        if (inContext.getContentResolver() != null) {
            Cursor cursor = inContext.getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }


    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     * @author paulburke
     */
    private static String getDataColumn(Context context, Uri uri, String selection,
                                        String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                if (DEBUG)
                    DatabaseUtils.dumpCursor(cursor);

                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static String getExtension(String uri) {
        if (uri == null) {
            return null;
        }

        int dot = uri.lastIndexOf(".");
        if (dot >= 0) {
            return uri.substring(dot);
        } else {
            // No extension.
            return "";
        }
    }



    public static String getPath(final Context context, final Uri uri) {

        if (DEBUG)
            Log.d(TAG + " File -",
                    "Authority: " + uri.getAuthority() +
                            ", Fragment: " + uri.getFragment() +
                            ", Port: " + uri.getPort() +
                            ", Query: " + uri.getQuery() +
                            ", Scheme: " + uri.getScheme() +
                            ", Host: " + uri.getHost() +
                            ", Segments: " + uri.getPathSegments().toString()
            );

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        Log.e(TAG, "pathUri: " + uri);
        if (uri == null)
            return null;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    /////   call logs

    public static String getContactName(final String phoneNumber, Context context)
    {
        Uri uri=Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,Uri.encode(phoneNumber));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};

        String contactName="";
        Cursor cursor=context.getContentResolver().query(uri,projection,null,null,null);

        if (cursor != null) {
            if(cursor.moveToFirst()) {
                contactName=cursor.getString(0);
            }
            cursor.close();
        }

        return contactName;
    }




}
