package com.prudent.callefy.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.prudent.callefy.ui.home.DashboardActivity;

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("TAG+++++>>>>", "onReceive: ");

//        Intent intentStart = new Intent(context, ForegroundService.class);
//        intentStart.setAction(ForegroundService.ACTION_START_FOREGROUND_SERVICE);
//        context.startForegroundService(intentStart);

        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {


            Log.d("TAG+++++>>>>", "onReceive: ========");
            Toast.makeText(context, "br", Toast.LENGTH_SHORT).show();
            Intent pushIntent = new Intent(context, DashboardActivity.class);
            pushIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(pushIntent);
        }
    }

}