package com.prudent.callefy.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.prudent.callefy.R;
import com.prudent.callefy.api.ApiClient;
import com.prudent.callefy.database.AppErrorTask;
import com.prudent.callefy.database.CommonTransactionsDb;
import com.prudent.callefy.database.missed_calls.MissedCalls;
import com.prudent.callefy.model.CalenderModel;
import com.prudent.callefy.model.CommonDataResponse;
import com.prudent.callefy.preferences.SharedPrefs;
import com.prudent.callefy.ui.home.DashboardActivity;
import com.prudent.callefy.ui.notification.CallLogsActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Notification.EXTRA_NOTIFICATION_ID;

public class ForegroundService  extends Service implements CommonTransactionsDb.CommonTransForegroundServiceDbCallsBack {
    private static final String TAG_FOREGROUND_SERVICE = "TAG_FOREGROUND_SERVICE";
    public static final String ACTION_START_FOREGROUND_SERVICE = "ACTION_START_FOREGROUND_SERVICE";
    public static final String ACTION_STOP_FOREGROUND_SERVICE = "ACTION_STOP_FOREGROUND_SERVICE";
    public static final String ACTION_DONOT_DISTURB_ACTIVATION = "ACTION_DONOT_DISTURB_ACTIVATION";

    public static boolean isCheckVoipCall = false;
    public static int noOfChecking = 1;

    ArrayList<CalenderModel> calenderList;

    public CommonTransactionsDb mTransactionsDb;

    public AudioManager audioManager;


    public ForegroundService() {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("not yer implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG_FOREGROUND_SERVICE, "onCreate: service ");
        if (mTransactionsDb == null) {
            mTransactionsDb = new CommonTransactionsDb(getApplicationContext(), this);
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null) {
            String action = intent.getAction();
            if (action != null) {
                switch (action) {
                    case ACTION_START_FOREGROUND_SERVICE:
                        isCheckVoipCall = true;
                        noOfChecking = 1;
                        startForegroundService();
                        break;
                    case ACTION_STOP_FOREGROUND_SERVICE:
                        isCheckVoipCall = false;
                        stopForegroundService();
                        break;
                    case ACTION_DONOT_DISTURB_ACTIVATION:

                        Log.d("TAG", "onStartCommand: ACTION_DONOT_DISTURB_ACTIVATION");
                        SharedPrefs.setIsManualDoNotDisturbActive(true);
                        break;

                }
            }
        }


        return super.onStartCommand(intent, flags, startId);
//        return START_STICKY;
    }

    private void startForegroundService() {
        Log.d(TAG_FOREGROUND_SERVICE, "startForegroundService: ");
        SharedPrefs.setBoolean(SharedPrefs.Key.IS_START_BACKGROUND_SERVICE, true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel("My service", "My Background service");
        }else {
           createNotification();
        }
    }

    private void stopForegroundService() {

        SharedPrefs.setBoolean(SharedPrefs.Key.IS_START_BACKGROUND_SERVICE, false);
        Log.d(TAG_FOREGROUND_SERVICE, "stopForegroundService: ");

        // stop foreground and remove notification
        stopForeground(true);

        // stop foreground
        stopSelf();
    }

    /* ******* 1. Start foreground service notification  ********* */
    private void createNotification(){
        Intent intent = new Intent();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        // create notification builder
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        // make notification big text style
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(getString(R.string.app_name));
        bigTextStyle.bigText("App is running in background"); //our application stop your GSM call until  stop your Voip call

        // set big text style
        builder.setStyle(bigTextStyle);

        builder.setWhen(System.currentTimeMillis());
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle(getString(R.string.app_name));
        builder.setContentText("App is running in background");
        Bitmap largeBitmapIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher_background);
        builder.setLargeIcon(largeBitmapIcon);

//            // make notification max priority
//            builder.setPriority(Notification.PRIORITY_MAX);
//
//            // make head up notification
//            builder.setFullScreenIntent(pendingIntent, true);

        // Build the notification
        Notification notification = builder.build();

        // start foreground
        startForeground(1, notification);

        runBackground();
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void createNotificationChannel(String channelId, String channelName) {
        Intent resultIntent = new Intent(this, DashboardActivity.class);
        // create the taskStckBuilder and add the intent which inflate the back stack
        TaskStackBuilder stackBuilder =
                TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationChannel chan = new NotificationChannel(channelId, channelName,
                NotificationManager.IMPORTANCE_DEFAULT);
        chan.setLightColor(Color.BLUE);

        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
        Notification notification = notificationBuilder.setOngoing(true).
                setSmallIcon(R.drawable.ic_launcher_background).
                setContentTitle("App is running in background").
                setPriority(NotificationManager.IMPORTANCE_MIN).
                setCategory(Notification.CATEGORY_SERVICE).
                setContentIntent(resultPendingIntent).
//                addAction(android.R.drawable.ic_media_pause, "Stop", pendingPlayIntent).
                build();

        NotificationManagerCompat notificationManagerCompat =
                NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(1, notificationBuilder.build());
        startForeground(1, notification);

        runBackground();
    }

    /* ******* 1. End foreground service notification  ********* */
    /* ******* 2. Start Event notification  ********* */
    public void createNotificationForEventNew(){
        int notifyID = 0;
        String CHANNEL_ID = "my_channel_0";// The id of the channel.
        CharSequence name = "Channelname";// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;
//        NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);

        // add count button intent in notification
        Intent countIntent = new Intent(this, ForegroundService.class);
        countIntent.setAction(ACTION_DONOT_DISTURB_ACTIVATION);
        countIntent.putExtra(EXTRA_NOTIFICATION_ID, 0);
        PendingIntent pendingCountIntent = PendingIntent.getService(this, 0, countIntent, 0);
//        NotificationCompat.Action activationAction = new NotificationCompat.Action(android.R.drawable.ic_menu_more
//                , "ACTIVATION", pendingCountIntent);


        Notification notification = new Notification.Builder(this)
                .setContentTitle("Schedule meeting.")
                .setContentText("You have a meeting after few minutes later.")
                .setSmallIcon(R.drawable.ic_launcher_background)
//                .setChannelId(CHANNEL_ID)
                .addAction(android.R.drawable.ic_menu_more, "Click to activate", pendingCountIntent)
                .build();

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        mNotificationManager.createNotificationChannel(mChannel);

// Issue the notification.
        mNotificationManager.notify(notifyID, notification);

    }

    /* ******* 2. End Event notification  ********* */

    /* ******* 3. Start missed call notification  ********* */
    public void createNotificationForMissedCalls(String missedCallCount) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            Intent resultIntent = new Intent(this, CallLogsActivity.class);
            Intent resultIntent = new Intent(this, DashboardActivity.class);
            // create the taskStckBuilder and add the intent which inflate the back stack
            TaskStackBuilder stackBuilder =
                    TaskStackBuilder.create(this);
            stackBuilder.addNextIntentWithParentStack(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(1,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationChannel chan = new NotificationChannel("channelId", "channelName",
                    NotificationManager.IMPORTANCE_DEFAULT);
            chan.setLightColor(Color.BLUE);

            chan.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            assert manager != null;
            manager.createNotificationChannel(chan);

//        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "channelId");
            Notification notification = notificationBuilder.setOngoing(false).
                    setSmallIcon(R.drawable.ic_launcher_background).
                    setContentTitle("You have " + missedCallCount + " missed calls").
//                setCategory(Notification.CATEGORY_MESSAGE).
        setContentIntent(resultPendingIntent).
                            setPriority(NotificationManager.IMPORTANCE_MIN).
                            setCategory(Notification.CATEGORY_SERVICE).
                            setAutoCancel(true).
//                setSound(alarmSound).
//                addAction(android.R.drawable.ic_media_pause, "Stop", pendingPlayIntent).
//                addAction(android.R.drawable.ic_menu_more, "count", pendingCountIntent).
        build();


            NotificationManagerCompat notificationManagerCompat =
                    NotificationManagerCompat.from(this);
            notificationManagerCompat.notify(2, notificationBuilder.build());

//        startForeground(2, notification);
        }else {
            int notifyID = 0;
            String CHANNEL_ID = "my_channel_0";// The id of the channel.
            CharSequence name = "Channelname";// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
//        NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);

            /////

            Intent resultIntent = new Intent(this, DashboardActivity.class);
            // create the taskStckBuilder and add the intent which inflate the back stack
            TaskStackBuilder stackBuilder =
                    TaskStackBuilder.create(this);
            stackBuilder.addNextIntentWithParentStack(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(1,
                    PendingIntent.FLAG_UPDATE_CURRENT);
//            NotificationChannel chan = new NotificationChannel("channelId", "channelName",
//                    NotificationManager.IMPORTANCE_DEFAULT);
//            chan.setLightColor(Color.BLUE);
//
//            chan.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            /////////

            Intent notificationIntent = new Intent(this, CallLogsActivity.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

//            sendNotification(event_notification, notificationIntent, mContext.getString(R.string.event_notif_title),
//                    body, Utils.PA_NOTIFICATIONS_ID);

            // add count button intent in notification
            Intent countIntent = new Intent(this, ForegroundService.class);
            countIntent.setAction(ACTION_DONOT_DISTURB_ACTIVATION);
            countIntent.putExtra(EXTRA_NOTIFICATION_ID, 0);
            PendingIntent pendingCountIntent = PendingIntent.getService(this, 0, countIntent, 0);
//        NotificationCompat.Action activationAction = new NotificationCompat.Action(android.R.drawable.ic_menu_more
//                , "ACTIVATION", pendingCountIntent);


            Notification notification = new Notification.Builder(this)

                    .setContentTitle("You have " + missedCallCount + " missed calls")
//                    .setContentText("You have a meeting after few minutes later.")
                    .setSmallIcon(R.drawable.ic_launcher_background)
//                .setChannelId(CHANNEL_ID)
                    .setContentIntent(resultPendingIntent)

//                    .addAction(android.R.drawable.ic_menu_more, "Click to activate", pendingCountIntent)
                    .build();

            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        mNotificationManager.createNotificationChannel(mChannel);

// Issue the notification.
            mNotificationManager.notify(notifyID, notification);
        }

    }
    /* ******* 3. End missed call notification  ********* */

    public void runBackground() {
        if (audioManager == null)
            audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (mTransactionsDb == null)
            mTransactionsDb = new CommonTransactionsDb(getApplicationContext(),this);

        if (audioManager.getMode() == AudioManager.MODE_IN_COMMUNICATION) {
            Log.d("TAG==>  ", "onCreate: MODE_IN_COMMUNICATION");

//            SharedPrefs.setIsDoNotDisturbActive(true);
            SharedPrefs.setIsVoipDoNotDisturbActive(true);
        }else if (audioManager.getMode() == AudioManager.MODE_RINGTONE) {
            Log.d("TAG==>  ", "onCreate: MODE_RINGTONE");

        }else {
            Log.d("TAG==>  ", "onCreate: NOT MODE_IN_COMMUNICATION");
//            SharedPrefs.setIsDoNotDisturbActive(false);
            SharedPrefs.setIsVoipDoNotDisturbActive(false);

            boolean isVoipDND = SharedPrefs.isVoipDoNotDisturbActive();
            boolean isManualDND = SharedPrefs.isManualDoNotDisturbActive();
            boolean isItemEventDNDActive;

            if (isVoipDND || isManualDND) {

            } else {
                int messedCalls = SharedPrefs.getInt(SharedPrefs.Key.MISSED_CALLS_DURING_DNDB,0);
                if (messedCalls>=1){
                    SharedPrefs.setInt(SharedPrefs.Key.MISSED_CALLS_DURING_DNDB,0);
                    createNotificationForMissedCalls(messedCalls+"");
                }


            }

        }

        // its working for stop functionalitty
        String expireDate = Constants.APP_EXPIRED_DATE;
        String currentDate = CommonUtils.getTodayDate();
        long expireDateLong = CommonUtils.getMillisecond(expireDate);
        long currentDateLong = CommonUtils.getMillisecond(currentDate);

        if (currentDateLong > expireDateLong) {
            SharedPrefs.setInt(SharedPrefs.Key.OPERATOR_BACKGROUND_RUNNIG_STATUS, 0);
        }

        if (SharedPrefs.getInt(SharedPrefs.Key.OPERATOR_BACKGROUND_RUNNIG_STATUS, 1) == 1) {
            int backgroundRunningCount = SharedPrefs.getInt(SharedPrefs.Key.NUMBER_OF_BACKGROUND_THREAD_RUNNING_COUNT, 1);

            if (backgroundRunningCount % 12 == 0) {
                runThreadBackgroundRemoveUnExpectedRunning();
            }
            runThreadBackground();
        }


    }

    public void runThreadBackground() {
        if (SharedPrefs.getBoolean(SharedPrefs.Key.REMOVE_UNEXPECTED_BACKGROUND_THREAD, false)) {
            return;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isCheckVoipCall) {
                    noOfChecking++;
                    int backgroundRunningCount = SharedPrefs.getInt(SharedPrefs.Key.NUMBER_OF_BACKGROUND_THREAD_RUNNING_COUNT, 1);

                    SharedPrefs.setInt(SharedPrefs.Key.NUMBER_OF_BACKGROUND_THREAD_RUNNING_COUNT, backgroundRunningCount + 1);

                    runBackground();

//                    ShowYesNoDialog();
//                    showAlert();
                }

            }
        }, 6000);
    }

    public void runThreadBackgroundRemoveUnExpectedRunning() {

        SharedPrefs.setBoolean(SharedPrefs.Key.REMOVE_UNEXPECTED_BACKGROUND_THREAD, true);


//        createNotificationForEvents();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPrefs.setBoolean(SharedPrefs.Key.REMOVE_UNEXPECTED_BACKGROUND_THREAD, false);

                if (isCheckVoipCall) {
                    noOfChecking++;
                    int backgroundRunningCount = SharedPrefs.getInt(SharedPrefs.Key.NUMBER_OF_BACKGROUND_THREAD_RUNNING_COUNT, 1);

                    SharedPrefs.setInt(SharedPrefs.Key.NUMBER_OF_BACKGROUND_THREAD_RUNNING_COUNT, backgroundRunningCount + 1);


//                    calenderList = ReedCalender.readCalendarEventDateRange(getApplicationContext());


                    runBackground();

//                    createNotificationForEvents();

                    calenderList = ReedCalender.readCalendarEventDateRange(getApplicationContext());


                    if (calenderList == null || calenderList.size() < 1) {

                    } else {
                        CalenderModel item = calenderList.get(0);
                        log("runThreadBackground ==>" + item.getStartDate());
                        long startDate = CommonUtils.getMillisecondFromDateTime(item.getStartDate());

                        long currentDate = CommonUtils.getCurrentMilliSecondDateForward5Min();
//
                        if (startDate < currentDate) {
                            log("start Event");
                            SharedPrefs.setString(SharedPrefs.Key.CALENDER_EVENT_ID_START_BEFORE_5_MIN, item.getEventId());
                            createNotificationForEventNew();
                        }

                    }
                }
            }
        }, 14000);


        if (CommonUtils.isNetworkConnected(getApplicationContext()))
            mTransactionsDb.getAllMissedCallsForSaveToServerForeground();
    }


    public void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void log(String message) {
        Log.d("TAG==> Foreground ", "log: " + message);
    }

    @Override
    public void onGetAllCallRejReports(List<MissedCalls> list) {
        if (list !=null && list.size() > 0){



            JsonObject call_listObj = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            for (MissedCalls missedCalls : list) {

                JsonObject taskObj = new JsonObject();
                taskObj.addProperty("call_rejected_case", missedCalls.getCall_rejected_case());
                taskObj.addProperty("call_rejected_case_status", missedCalls.getCall_rejected_case_status());
                taskObj.addProperty("call_rejected_group_name", missedCalls.getCall_rejected_group_name());
                taskObj.addProperty("call_rejected_response_sms", missedCalls.getSms_content());
                taskObj.addProperty("call_rejected_number", missedCalls.getPh_no());
                taskObj.addProperty("call_rejected_date_time", missedCalls.getDate_time());
                taskObj.addProperty("is_working_successfully", missedCalls.isIs_working_successfully());

                jsonArray.add(taskObj);
            }

            call_listObj.add("call_rejected_reports", jsonArray);
            call_listObj.addProperty("user_id", SharedPrefs.getUserId());
            call_listObj.addProperty("tag", Constants.TAG_API_APP_UPDATE_CALL_REJECTED_REPORTS);

            Log.d("TAG", "updateAppFailureCase: " + call_listObj.toString());

            ApiClient.getApiInterface().updateCallRejectReport(call_listObj)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<CommonDataResponse>() {
                                   @Override
                                   public void onSubscribe(Disposable d) {

                                   }

                                   @Override
                                   public void onSuccess(CommonDataResponse commonDataResponse) {

                                       mTransactionsDb.updateMissedCallsItemAfterUploadServer();
                                   }

                                   @Override
                                   public void onError(Throwable e) {

                                   }
                               }

                    );



        }

    }
}
