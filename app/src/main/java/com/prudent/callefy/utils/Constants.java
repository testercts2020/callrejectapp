package com.prudent.callefy.utils;

public class Constants {


//    **002*9072670614#
//    #21#  --->  *21*9072670614#

    public static final String TAG_API_LOGIN= "login";
    public static final String TAG_API_OTP_VERIFY= "otp_verification";
    public static final String TAG_API_GET_PROFILE= "get_profile";
    public static final String TAG_API_GET_NOTIFICATIONS= "notification_list";
    public static final String TAG_API_UPDATE_DEVICE_INFO= "update_device_info";
    public static final String TAG_API_UPDATE_PROFILE= "update_profile";
    public static final String TAG_API_UPDATE_FEEDBACK= "feedback";
    public static final String TAG_API_APP_FAILED_CASE= "app_failed_case";
    public static final String TAG_API_NOTIFICATION_LAST_TIME= "notification_info_last_time";
    public static final String TAG_API_APP_UPDATE_CALL_REJECTED_REPORTS= "app_update_call_rejected_reports";

    public static final String DB_CODE_TRUE= "true";
    public static final String DB_CODE_FALSE= "false";

    public static final String APP_EXPIRED_DATE= "2021-02-28";
    public static final String OPERATOR_JIO= "jio";

    //// temp data///

//    public static final String TEMP_LAST_DATE= "2020-11-29 18:30:56";


    public static final String CALL_REJECT_STATE = "call_reject_state";  // 1==>Normal; 2==>Reject call; 3=>reject and sms





}
