package com.prudent.callefy.base;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import com.prudent.callefy.R;
import com.prudent.callefy.database.CommonTransactionsDb;
import com.prudent.callefy.dialog_fragment.ProgressDialogFragment;
import com.prudent.callefy.dialog_fragment.dialog_internet_error.InternetErrorActivity;
import com.prudent.callefy.ui.profile.ProfileActivity;

public abstract class BaseActivity  extends AppCompatActivity {
    public static final String TAG = BaseActivity.class.getSimpleName();

    private DialogFragment progressDialog;


    public CommonTransactionsDb mTransactionsDb;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (hideStatusbar()){
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        if (setFullScreen()){
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(setLayout());

        if (setToolbar()){
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
        }

        mTransactionsDb = new CommonTransactionsDb(getApplicationContext());
    }

    public void showProgress(){
        progressDialog = ProgressDialogFragment.newInstance();
        progressDialog.show(getSupportFragmentManager(), "");

    }

    public void dismissProgress(){
        try{
            if (progressDialog != null){
                progressDialog.dismiss();
            }
        }catch (Exception e){

        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu ) {
        return super.onCreateOptionsMenu(menu);
    }

    //    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        onCreateOptionsMenu(menu, inflater);
//        menu.clear();
//        inflater.inflate(R.menu.right_menu_edit, menu);
//    }

//    public abstract void onCreateOptionsMenu(Menu menu, MenuInflater inflater);

    public abstract int setLayout();

    public abstract void initView();

    public abstract boolean setToolbar();

    public abstract boolean hideStatusbar();

    public abstract boolean setFullScreen();

    public void openNoInternetScreen(){
        startActivity(InternetErrorActivity.startActivity(getApplicationContext()));
    }

    public void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void log(String message){
        Log.d(TAG, "log: "+message);
    }

}
