package com.prudent.callefy.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserInfoModel {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("email_id")
    @Expose
    private String emailId;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("mobile_no_operator_ussdcode")
    @Expose
    private List<ProfileMobileInfoModel> mobileNoOperatorUssdcode = null;

    public Integer getUserId() {
        return userId;
    }

    public String getEmailId() {
        return emailId;
    }

    public String getUserName() {
        return userName;
    }

    public List<ProfileMobileInfoModel> getMobileNoOperatorUssdcode() {
        return mobileNoOperatorUssdcode;
    }
}
