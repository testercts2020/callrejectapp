package com.prudent.callefy.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CallLogsModel {
    @SerializedName("to_no")
    @Expose
    private String toNo;
    @SerializedName("from_no")
    @Expose
    private String fromNo;
    @SerializedName("date_time")
    @Expose
    private String dateTime;
    @SerializedName("forwarding_no")
    @Expose
    private String forwardingNo;
    @SerializedName("no_of_calls_same_user")
    @Expose
    private Integer noOfCallsSameUser;

    private String callType;
    private String callerName;

    private boolean isHeader = false;

    private boolean expanded = false;


    public CallLogsModel(String number, String type, String date, String name){
        fromNo = number;
        callType = type;
        dateTime = date;
        if (name == null || name.equals("")){
            callerName = number;
        }else
        callerName = name;
    }

    public CallLogsModel(){

    }


    public String getToNo() {
        return toNo;
    }

    public String getFromNo() {
        return fromNo;
    }

    public String getDateTime() {
        return dateTime;
    }

    public String getForwardingNo() {
        return forwardingNo;
    }

    public Integer getNoOfCallsSameUser() {
        return noOfCallsSameUser;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public String getCallType() {
        return callType;
    }

    public void setFromNo(String fromNo) {
        this.fromNo = fromNo;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getCallerName() {
        return callerName;
    }

    public void setCallerName(String callerName) {
        this.callerName = callerName;
    }
}
