package com.prudent.callefy.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user_info")
    @Expose
    private UserInfoModel userInfoModel;

    public Integer getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public UserInfoModel getUserInfoModel() {
        return userInfoModel;
    }
}
