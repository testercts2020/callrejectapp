package com.prudent.callefy.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationsResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("notification")
    @Expose
    private List<NotificationsModel> notification = null;
    @SerializedName("notification_count")
    @Expose
    private Integer notificationCount;

    public Integer getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<NotificationsModel> getNotification() {
        return notification;
    }

    public Integer getNotificationCount() {
        return notificationCount;
    }
}
