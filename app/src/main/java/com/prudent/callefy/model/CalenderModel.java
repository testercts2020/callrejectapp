package com.prudent.callefy.model;

public class CalenderModel {
    private String eventId;
    private String event;
    private String description;
    private String startDate;
    private String endDate;
    private boolean isCurrentEvent;

    public CalenderModel(String eventId,String event, String description, String startDate, String endDate, boolean isCurrentEvent)  {
        this.eventId = eventId;
        this.event = event;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.isCurrentEvent = isCurrentEvent;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isCurrentEvent() {
        return isCurrentEvent;
    }

    public void setCurrentEvent(boolean currentEvent) {
        isCurrentEvent = currentEvent;
    }
}
