package com.prudent.callefy.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationsModel {
    @SerializedName("call_logs")
    @Expose
    private List<CallLogsModel> callLogs = null;
    @SerializedName("date_time")
    @Expose
    private String dateTime;

    public List<CallLogsModel> getCallLogs() {
        return callLogs;
    }

    public String getDateTime() {
        return dateTime;
    }
}
