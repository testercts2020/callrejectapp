package com.prudent.callefy.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileMobileInfoModel {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("operator")
    @Expose
    private String operator;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("ussd_code_activation")
    @Expose
    private String ussd_code_activation;
    @SerializedName("ussd_code_deactivation")
    @Expose
    private String ussd_code_deactivation;
    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("current_status")
    @Expose
    private String currentStatus;
    @SerializedName("current_status_code")
    @Expose
    private Integer currentStatusCode;

    public String getStatus() {
        return status;
    }

    public String getOperator() {
        return operator;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public String getUssd_code_activation() {
        return ussd_code_activation;
    }

    public String getUssd_code_deactivation() {
        return ussd_code_deactivation;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public Integer getCurrentStatusCode() {
        return currentStatusCode;
    }
}
