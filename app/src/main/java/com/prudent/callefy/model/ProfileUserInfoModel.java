package com.prudent.callefy.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileUserInfoModel {
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("email_id")
    @Expose
    private String emailId;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("profileURL")
    @Expose
    private String profileURL;
    @SerializedName("mobile_no_operator_ussdcode")
    @Expose
    private List<ProfileMobileInfoModel> mobileNoOperatorUssdcode = null;

    public Integer getUserId() {
        return userId;
    }

    public String getEmailId() {
        return emailId;
    }

    public String getUserName() {
        return userName;
    }

    public String getProfileURL() {
        return profileURL;
    }

    public List<ProfileMobileInfoModel> getMobileNoOperatorUssdcode() {
        return mobileNoOperatorUssdcode;
    }
}
