package com.prudent.callefy.api;

import com.google.gson.JsonObject;
import com.prudent.callefy.model.CommonDataResponse;
import com.prudent.callefy.model.LoginResponse;
import com.prudent.callefy.model.NotificationsResponse;
import com.prudent.callefy.model.ProfileResponse;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    // Sign up
    @GET("api/v1/activity/?page=1")
    Single<CommonDataResponse> getVideosList();


    @POST("api/Values/InstantSMS/json/apikey=CMSSD6GI")
    Single<CommonDataResponse> getOtp();

    @POST("Values/callefyapp")
    Single<LoginResponse> commonApi(@Body HashMap<String, String> body);

    @POST("Values/callefyapp")
    Single<ProfileResponse> getProfile(@Body HashMap<String, String> body);

    @POST("Values/callefyapp")
    Single<CommonDataResponse> updateProfile(@Body HashMap<String, String> body);

    @POST("Values/callefyapp")
    Single<NotificationsResponse> getNotifications(@Body HashMap<String, String> body);


    @POST("Values/callefyapp")
    Call<JsonObject> getNotificationsTwo(@Body HashMap<String, String> body);

    @POST("Values/callefyapp")
    Single<CommonDataResponse> updateDeviceInfo(@Body HashMap<String, String> body);

 @POST("Values/callefyapp")
    Single<CommonDataResponse> updateFeedback(@Body HashMap<String, String> body);


    @Headers("Content-Type: application/json")
    @POST("Values/callefyapp")
    Single<CommonDataResponse> updateFailureCase(
            @Body JsonObject body);

    @Multipart
    @POST("Values/fileupload")
    Single<CommonDataResponse> updateProfileWithImage(@Part("user_id") RequestBody id,
                                           @Part("tag") RequestBody tag,
                                           @Part("name") RequestBody name,
                                           @Part("email") RequestBody email,
                                           @Part("mobile_no") RequestBody mobile_no,
                                           @Part MultipartBody.Part image
    );


    @Headers("Content-Type: application/json")
    @POST("Values/callefyapp")
    Single<CommonDataResponse> updateCallRejectReport(
            @Body JsonObject body);


}
