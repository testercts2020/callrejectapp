package com.prudent.callefy.dialog_fragment.dialog_edit_sms_content;

public interface OnEditSmsDialogClickListener {
    void addSmsContent(String message);
    void updateSmsContent(String message);
    void deleteSmsContent();
}
