package com.prudent.callefy.dialog_fragment.dialog_contacts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.prudent.callefy.R;
import com.prudent.callefy.database.groups.ContactGroup;
import com.prudent.callefy.ui.groups.GroupsCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> implements Filterable {

    private List<ContactModel> mContactList = new ArrayList<>();
    private List<ContactModel> mContactListAll = new ArrayList<>();
    Context mContext;
    ContactCallback mContactCallback;

    public ContactAdapter(List<ContactModel> mContactList, Context mContext, ContactCallback mContactCallback) {
        this.mContactList = mContactList;
        this.mContactListAll = mContactList;
        this.mContext = mContext;
        this.mContactCallback = mContactCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_contacts, parent, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ContactModel item = mContactList.get(position);
        holder.tvContactName.setText(item.getName());
        holder.tvContactNumber.setText(item.getNumber());

    }

    @Override
    public int getItemCount() {
        return mContactList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mContactList = mContactListAll;
                }else {
                    List<ContactModel> filteredList = new ArrayList<>();
                    for (ContactModel row : mContactListAll) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) ) {
                            filteredList.add(row);
                        }
                    }

                    mContactList = filteredList;

                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mContactList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mContactList = (ArrayList<ContactModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tvContactName)
        TextView tvContactName;
        @BindView(R.id.tvContactNumber)
        TextView tvContactNumber;
        @BindView(R.id.btAddContact)
        MaterialButton btAddContact;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            btAddContact.setOnClickListener(this::onClick);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btAddContact:

                    mContactCallback.onAddContacts(mContactList.get(getAdapterPosition()));
                    break;
            }

        }
    }
}
