package com.prudent.callefy.dialog_fragment.dialog_contacts;

public interface OnContactsDialogClickListener {
    void onAddContacts(ContactModel item);


    void onShowProgress();
    void onDismissProgress();


}
