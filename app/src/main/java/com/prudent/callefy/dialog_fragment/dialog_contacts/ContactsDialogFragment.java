package com.prudent.callefy.dialog_fragment.dialog_contacts;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import com.prudent.callefy.R;
import com.prudent.callefy.dialog_fragment.dialog_expired.ExpireDialogFragment;
import com.prudent.callefy.dialog_fragment.dialog_expired.OnExpireDialogClickListener;
import com.prudent.callefy.ui.notification.CallLogsActivity;
import com.prudent.callefy.ui.notification.NotificationAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactsDialogFragment extends DialogFragment implements View.OnClickListener,ContactCallback {

    private OnContactsDialogClickListener clickListener;

    @BindView(R.id.tvMessage)
    TextView tvMessage;
//    private Button btAllow, btCancel;
    @BindView(R.id.rvList)
    RecyclerView rvList;
    @BindView(R.id.ibClose)
    ImageButton ibClose;
    @BindView(R.id.etSearch)
    EditText etSearch;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private String mParam1;
    private String mParam2;

    private String message = "";
    private String strCancel = "";
    private String strAllow = "";

    private ContactAdapter adapter;

    public static ArrayList<ContactModel> contactModelsList = new ArrayList<>();


    public static ContactsDialogFragment newInstance(String msg, String strCancel, String strAllow , ArrayList<ContactModel> tcontactModelsList) {
//        return new SelectVehicleDialogFragment();
        ContactsDialogFragment fragment = new ContactsDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, msg);
        args.putString(ARG_PARAM2, strCancel);
        args.putString(ARG_PARAM3, strAllow);
         contactModelsList = tcontactModelsList;
//        args.putString(Constants.DIALOG_VEHICLE, vehicle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            message = getArguments().getString(ARG_PARAM1, "");
            strCancel = getArguments().getString(ARG_PARAM2, "");
            strAllow = getArguments().getString(ARG_PARAM3, "");

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

//        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        View view = inflater.inflate(R.layout.dialog_contact_list, container, false);
        ButterKnife.bind(this,view);
        initViews(view);
        return view;

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), getTheme());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.show();
//        dialog.getWindow().setAttributes(lp);

        return dialog;

    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//            dialog.getWindow().setGravity();
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    private void initViews(View view) {

//        tvMessage = view.findViewById(R.id.tvMessage);
//        btCancel = view.findViewById(R.id.btCancel);
//        btAllow = view.findViewById(R.id.btAllow);

        tvMessage.setText(message);
//        btCancel.setText(strCancel);
//        btAllow.setText(strAllow);

        ((SimpleItemAnimator) rvList.getItemAnimator()).setSupportsChangeAnimations(false);

        rvList.setHasFixedSize(true);
        rvList.setLayoutManager(new LinearLayoutManager(getContext()));

//        getContactList();

        ibClose.setOnClickListener(this);
//        btCancel.setOnClickListener(this);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

//                adapter.filter(s.toString());
                adapter.getFilter().filter(s.toString());
            }
        });

        updateListView();

    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            clickListener = (OnContactsDialogClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "attaching d fragment failed!");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        clickListener = null;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ibClose:
                dismiss();
                break;
        }

    }

    private void getContactList() {

        ContentResolver cr = getContext().getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

        if ((cur != null ? cur.getCount() : 0) > 0) {
            String tempPhoneNo="";
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                        ContactsContract.Contacts.DISPLAY_NAME));

                if (cur.getInt(cur.getColumnIndex(
                        ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));
                        phoneNo =  phoneNo.replace(" ", "");
                        if (phoneNo.length()>10)
                        phoneNo= phoneNo.substring(phoneNo.length()-10);
                        Log.i("TAG", "Name: " + name);
                        Log.i("TAG", "Phone Number: " + phoneNo);

                        if (!phoneNo.equals(tempPhoneNo)){
                            contactModelsList.add(new ContactModel(name, phoneNo));
                            tempPhoneNo = phoneNo;
                        }

                    }
                    pCur.close();
                }
            }
        }
        if(cur!=null){
            cur.close();
        }

        updateListView();

    }

    private void updateListView() {
        clickListener.onDismissProgress();

        if (contactModelsList == null)
            return;

        adapter = new ContactAdapter(contactModelsList,
                getContext(), this);

        rvList.setAdapter(adapter);

//        clickListener.onDismissProgress();

    }

    @Override
    public void onAddContacts(ContactModel item) {
        clickListener.onAddContacts(item);
    }


}
