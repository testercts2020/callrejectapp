package com.prudent.callefy.dialog_fragment.dialog_expired;

public interface OnExpireDialogClickListener {
    void onDelete();
    void onCancel();
}
