package com.prudent.callefy.dialog_fragment.dialog_sms_content;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.prudent.callefy.R;
import com.prudent.callefy.database.CommonTransactionsDb;
import com.prudent.callefy.database.sms_content.SmsContent;
import com.prudent.callefy.dialog_fragment.ussd_code_issue.OnUssdCodeErrorDialogClickListener;
import com.prudent.callefy.dialog_fragment.ussd_code_issue.UssdCodeNotWorkingDialogFragment;
import com.prudent.callefy.preferences.SharedPrefs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SmsContentDialogFragment extends DialogFragment implements View.OnClickListener,
        CommonTransactionsDb.CommonTransSmsContentDbCallBack {

    private OnSmsContentDialogClickListener clickListener;

    private TextView tvMessage;
    private Button btAllow, btCancel;
    @BindView(R.id.radiogroup)
    RadioGroup radiogroup;
    @BindView(R.id.etCustomMessage)
    TextInputEditText etCustomMessage;
    @BindView(R.id.btAddMessage)
    MaterialButton btAddMessage;
    @BindView(R.id.scroll)
    ScrollView scroll;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private String mParam1;
    private String mParam2;

    private String message = "";
    private String strCancel = "";
    private String strAllow = "";


    private CommonTransactionsDb mTransactionsDb;

    public static SmsContentDialogFragment newInstance(String msg, String strCancel, String strAllow) {
//        return new SelectVehicleDialogFragment();
        SmsContentDialogFragment fragment = new SmsContentDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, msg);
        args.putString(ARG_PARAM2, strCancel);
        args.putString(ARG_PARAM3, strAllow);
//        args.putString(Constants.DIALOG_VEHICLE, vehicle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            message = getArguments().getString(ARG_PARAM1, "");
            strCancel = getArguments().getString(ARG_PARAM2, "");
            strAllow = getArguments().getString(ARG_PARAM3, "");

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

//        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        View view = inflater.inflate(R.layout.dialog_sms_content, container, false);
        ButterKnife.bind(this,view);
        initViews(view);
        return view;

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), getTheme());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.show();
//        dialog.getWindow().setAttributes(lp);

        return dialog;

    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//            dialog.getWindow().setGravity();
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    private void initViews(View view) {

        tvMessage = view.findViewById(R.id.tvMessage);
        btCancel = view.findViewById(R.id.btCancel);
        btAllow = view.findViewById(R.id.btAllow);

        tvMessage.setText(message);
        btCancel.setText(strCancel);
        btAllow.setText(strAllow);

        btAllow.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        btAddMessage.setOnClickListener(this);

        getSmsContent();

    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            clickListener = (OnSmsContentDialogClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "attaching d fragment failed!");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        clickListener = null;
    }


    private void getSmsContent(){
        if (mTransactionsDb == null){
            mTransactionsDb = new CommonTransactionsDb(getContext(), this);
        }

        mTransactionsDb.getSmsContent();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btAddMessage:
                if (etCustomMessage.getText().toString().trim().equals("")){
                    Toast.makeText(getContext(), "Enter custom message", Toast.LENGTH_SHORT).show();
                }else {
                    RadioButton radioButton = new RadioButton(getContext());
                    radioButton.setId(View.generateViewId());
                    radioButton.setText(etCustomMessage.getText().toString().trim());
                    radiogroup.addView(radioButton);

                    scroll.post(new Runnable() {
                        @Override
                        public void run() {
                            scroll.fullScroll(View.FOCUS_DOWN);
                        }
                    });

                    mTransactionsDb.saveSmsContent(new SmsContent(etCustomMessage.getText().toString().trim()));
                    etCustomMessage.setText("");
                }


                break;

            case R.id.btAllow:
                int selectedId = radiogroup.getCheckedRadioButtonId();
                if (selectedId == -1){
                    showToast(getString(R.string.please_choose_a_message));
                }else {


                    RadioButton radioButton = (RadioButton) radiogroup.findViewById(selectedId);
                    String value = radioButton.getText().toString();
//                    final String value =
//                            ((RadioButton)getContext().findViewById(radiogroup.getCheckedRadioButtonId()))
//                                    .getText().toString();
                    SharedPrefs.setString(SharedPrefs.Key.DEFAULT_SMS_CONTENT,value);
                    clickListener.onDefaultMessage(value);
                    dismiss();
                }

                break;
            case R.id.btCancel:
                dismiss();
                break;
        }

    }

    @Override
    public void getSmsContentList(List<SmsContent> logs) {
        radiogroup.setOrientation(LinearLayout.VERTICAL);
        if (logs == null || logs.size() < 1){
            return;
        }
        for (int i = 0; i< logs.size(); i++){
            RadioButton radioButton = new RadioButton(getActivity());
            radioButton.setId(View.generateViewId());
            radioButton.setText(logs.get(i).getSmsContent());
            radiogroup.addView(radioButton);

        }
    }



    public void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void log(String message){
        Log.d("TAG", "log: "+message);
    }
}
