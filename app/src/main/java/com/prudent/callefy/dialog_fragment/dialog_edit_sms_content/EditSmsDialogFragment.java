package com.prudent.callefy.dialog_fragment.dialog_edit_sms_content;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.textfield.TextInputEditText;
import com.prudent.callefy.R;
import com.prudent.callefy.dialog_fragment.dialog_expired.ExpireDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditSmsDialogFragment extends DialogFragment implements View.OnClickListener  {

    OnEditSmsDialogClickListener clickListener;


    private TextView tvMessage;
    private Button btAllow, btCancel;
    @BindView(R.id.etMessage)
    TextInputEditText etMessage;
    @BindView(R.id.ibClose)
    ImageButton ibClose;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";
    private String mParam1;
    private String mParam2;

    private String title = "";
    private String message = "";
    private String strCancel = "";
    private String strAllow = "";
    private boolean isAddMessage;

    public static EditSmsDialogFragment newInstance(String title, String strCancel, String strAllow,String message, boolean isAddMessage) {
//        return new SelectVehicleDialogFragment();
        EditSmsDialogFragment fragment = new EditSmsDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, title);
        args.putString(ARG_PARAM2, strCancel);
        args.putString(ARG_PARAM3, strAllow);
        args.putString(ARG_PARAM4, message);
        args.putBoolean(ARG_PARAM5, isAddMessage);
//        args.putString(Constants.DIALOG_VEHICLE, vehicle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString(ARG_PARAM1, "");
            strCancel = getArguments().getString(ARG_PARAM2, "");
            strAllow = getArguments().getString(ARG_PARAM3, "");
            message = getArguments().getString(ARG_PARAM4, "");
            isAddMessage =getArguments().getBoolean(ARG_PARAM5);

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

//        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        View view = inflater.inflate(R.layout.dialog_edit_sms, container, false);
        ButterKnife.bind(this,view);
        initViews(view);
        return view;

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), getTheme());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.show();
//        dialog.getWindow().setAttributes(lp);

        return dialog;

    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//            dialog.getWindow().setGravity();
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    private void initViews(View view) {

        tvMessage = view.findViewById(R.id.tvMessage);
        btCancel = view.findViewById(R.id.btCancel);
        btAllow = view.findViewById(R.id.btAllow);

        tvMessage.setText(title);
        btCancel.setText(strCancel);
        btAllow.setText(strAllow);
        etMessage.setText(message);

        btAllow.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        ibClose.setOnClickListener(this);

    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            clickListener = (OnEditSmsDialogClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "attaching d fragment failed!");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        clickListener = null;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ibClose:
                dismiss();
                break;
            case R.id.btAllow:
                if (isAddMessage){

                    if (etMessage.getText().toString().trim().equals("")){
                        Toast.makeText(getContext(), getString(R.string.message_field_is_rqd), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    clickListener.addSmsContent(etMessage.getText().toString());
                    dismiss();
                }
                else
                clickListener.updateSmsContent(etMessage.getText().toString());
                break;
            case R.id.btCancel:
                if (isAddMessage)
                    dismiss();
                else
                clickListener.deleteSmsContent();
                break;
        }
    }
}
