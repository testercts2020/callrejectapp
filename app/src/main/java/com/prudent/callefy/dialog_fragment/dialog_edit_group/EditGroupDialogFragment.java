package com.prudent.callefy.dialog_fragment.dialog_edit_group;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.base.BaseDialogFragment;
import com.prudent.callefy.dialog_fragment.dialog_edit_sms_content.EditSmsDialogFragment;
import com.prudent.callefy.dialog_fragment.dialog_edit_sms_content.OnEditSmsDialogClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditGroupDialogFragment extends BaseDialogFragment implements View.OnClickListener {

    private TextView tvMessage;
    private Button btAllow, btCancel;
    @BindView(R.id.etMessage)
    TextInputEditText etMessage;
    @BindView(R.id.ibClose)
    ImageButton ibClose;
    @BindView(R.id.message_text_input)
    TextInputLayout message_text_input;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";
    private String mParam1;
    private String mParam2;

    private String title = "";
    private String message = "";
    private String strCancel = "";
    private String strAllow = "";
    private boolean isAddGroup;

    OnEditGroupDialogClickListener clickListener;


    public static EditGroupDialogFragment newInstance(String title, String strCancel, String strAllow, String message, boolean isAddGroup) {
//        return new SelectVehicleDialogFragment();
        EditGroupDialogFragment fragment = new EditGroupDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, title);
        args.putString(ARG_PARAM2, strCancel);
        args.putString(ARG_PARAM3, strAllow);
        args.putString(ARG_PARAM4, message);
        args.putBoolean(ARG_PARAM5, isAddGroup);
//        args.putString(Constants.DIALOG_VEHICLE, vehicle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString(ARG_PARAM1, "");
            strCancel = getArguments().getString(ARG_PARAM2, "");
            strAllow = getArguments().getString(ARG_PARAM3, "");
            message = getArguments().getString(ARG_PARAM4, "");
            isAddGroup = getArguments().getBoolean(ARG_PARAM5);

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().setCanceledOnTouchOutside(false);
        View view = inflater.inflate(setLayout(), container, false);
        ButterKnife.bind(this, view);
        initView(view);
        return view;
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            clickListener = (OnEditGroupDialogClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "attaching d fragment failed!");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        clickListener = null;
    }

    @Override
    public int setLayout() {
        return R.layout.dialog_edit_sms;
    }

    @Override
    public void initView(View view) {
        tvMessage = view.findViewById(R.id.tvMessage);
        btCancel = view.findViewById(R.id.btCancel);
        btAllow = view.findViewById(R.id.btAllow);

        tvMessage.setText(title);
        btCancel.setText(strCancel);
        btAllow.setText(strAllow);
        etMessage.setText(message);
        message_text_input.setHint(title);

        btAllow.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        ibClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibClose:
                dismiss();
                break;
            case R.id.btAllow:
                if (etMessage.getText().toString().trim().equals("")) {
                    Toast.makeText(getContext(), getString(R.string.group_name_field_is_rqd), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (isAddGroup)
                    clickListener.onAddGroup(etMessage.getText().toString());
                else
                    clickListener.onUpdateGroup(etMessage.getText().toString());

                break;
            case R.id.btCancel:
                if (!isAddGroup)
                    clickListener.onDeleteGroup();

                dismiss();


                break;
        }
    }
}
