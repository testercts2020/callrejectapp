package com.prudent.callefy.dialog_fragment.dialog_number_picker;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.prudent.callefy.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NumberPickerDialogFragment extends DialogFragment implements View.OnClickListener, NumberPicker.OnValueChangeListener  {

    OnNumberPickerDialogClickListener clickListener;

    private TextView tvMessage;
    private Button btAllow, btCancel;
    @BindView(R.id.numberPicker1)
    NumberPicker numberPicker1;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private String mParam1;
    private String mParam2;

    private String message = "";
    private String strCancel = "";
    private String strAllow = "";

    public static NumberPickerDialogFragment newInstance(String msg, String strCancel, String strAllow) {
//        return new SelectVehicleDialogFragment();
        NumberPickerDialogFragment fragment = new NumberPickerDialogFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, msg);
        args.putString(ARG_PARAM2, strCancel);
        args.putString(ARG_PARAM3, strAllow);
//        args.putString(Constants.DIALOG_VEHICLE, vehicle);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            message = getArguments().getString(ARG_PARAM1, "");
            strCancel = getArguments().getString(ARG_PARAM2, "");
            strAllow = getArguments().getString(ARG_PARAM3, "");

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

//        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        View view = inflater.inflate(R.layout.dialog_number_picker, container, false);
        ButterKnife.bind(this,view);
        initViews(view);
        return view;

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), getTheme());
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.show();
//        dialog.getWindow().setAttributes(lp);

        return dialog;

    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//            dialog.getWindow().setGravity();
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    private void initViews(View view) {

        tvMessage = view.findViewById(R.id.tvMessage);
        btCancel = view.findViewById(R.id.btCancel);
        btAllow = view.findViewById(R.id.btAllow);

        tvMessage.setText(message);
        btCancel.setText(strCancel);
        btAllow.setText(strAllow);

        btAllow.setOnClickListener(this);
        btCancel.setOnClickListener(this);

        numberPicker1.setMaxValue(10000);
        numberPicker1.setMinValue(0);
        numberPicker1.setWrapSelectorWheel(false);
        numberPicker1.setOnValueChangedListener(this);

    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            clickListener = (OnNumberPickerDialogClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + "attaching d fragment failed!");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        clickListener = null;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btAllow:
                int minutes = numberPicker1.getValue();
                clickListener.onNumberPickedValues(minutes);
//                dismiss();
                break;
            case R.id.btCancel:

                dismiss();
                break;
        }
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        Log.i("value is",""+newVal);
    }
}
