package com.prudent.callefy.dialog_fragment.dialog_edit_group;

public interface OnEditGroupDialogClickListener {
    void onAddGroup(String name);
    void onUpdateGroup(String name);
    void onDeleteGroup();
}
