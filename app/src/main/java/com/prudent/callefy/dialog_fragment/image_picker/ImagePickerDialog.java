package com.prudent.callefy.dialog_fragment.image_picker;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.prudent.callefy.R;

public class ImagePickerDialog  extends BottomSheetDialogFragment implements View.OnClickListener{
    private OnInteractionListener m_listener;
    private LinearLayout llSelectFromGallery;
    private LinearLayout llTakeNew;

    public static ImagePickerDialog newInstance(OnInteractionListener listener) {
        Bundle args = new Bundle();
        ImagePickerDialog fragment = new ImagePickerDialog();
        fragment.setArguments(args);
        fragment.setListener(listener);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.bottom_sheet_image_picker, null, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        llSelectFromGallery = view.findViewById(R.id.llSelectFromGallery);
        llTakeNew = view.findViewById(R.id.llTakeNew);
        llTakeNew.setOnClickListener(this);
        llSelectFromGallery.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llSelectFromGallery: {
                try {
                    m_listener.onSelectFromGallery();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                getDialog().dismiss();
                break;
            }
            case R.id.llTakeNew: {
                try {
                    m_listener.onTakeNew();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                getDialog().dismiss();
                break;
            }
        }
    }

    protected void setListener(OnInteractionListener m_listener) {
        this.m_listener = m_listener;
    }

    public interface OnInteractionListener {
        void onSelectFromGallery();

        void onTakeNew();
    }


}
