package com.prudent.callefy.dialog_fragment.dialog_number_picker;

public interface OnNumberPickerDialogClickListener {

    void onNumberPickedValues(int minutes);
}
