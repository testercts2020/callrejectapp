package com.prudent.callefy.dialog_fragment.ussd_code_issue;

public interface OnUssdCodeErrorDialogClickListener {
    void onDelete();
    void onCancel();
}
