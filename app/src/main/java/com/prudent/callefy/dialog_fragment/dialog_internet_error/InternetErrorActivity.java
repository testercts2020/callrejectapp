package com.prudent.callefy.dialog_fragment.dialog_internet_error;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.button.MaterialButton;
import com.prudent.callefy.R;
import com.prudent.callefy.base.BaseActivity;
import com.prudent.callefy.ui.profile.ProfileActivity;
import com.prudent.callefy.utils.CommonUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InternetErrorActivity extends BaseActivity implements View.OnClickListener{

    @BindView(R.id.btRetry)
    MaterialButton btRetry;

    public static Intent startActivity(Context context) {
        return new Intent(context, InternetErrorActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        initView();
    }

    @Override
    public int setLayout() {
        return R.layout.activity_internet_error;
    }

    @Override
    public void initView() {
        btRetry.setOnClickListener(this::onClick);
    }


    @Override
    public boolean setToolbar() {
        return false;
    }

    @Override
    public boolean hideStatusbar() {
        return false;
    }

    @Override
    public boolean setFullScreen() {
        return false;
    }

    @Override
    public void onClick(View v) {
        if (CommonUtils.isNetworkConnected(getApplicationContext())) {
            finish();

        }
    }
}